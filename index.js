/**
 * @format
 */

import {AppRegistry} from 'react-native';
import { Sentry } from 'react-native-sentry';
import App from './src/Main';
import {name as appName} from './app.json';

Sentry.config('https://660efb7ccf564a389d14f6e6e304d054@sentry.io/1463094').install();
AppRegistry.registerComponent(appName, () => App);
