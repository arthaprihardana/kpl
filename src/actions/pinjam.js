/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 20:43:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 20:43:56
 */
import {GET_STATIC_FORM_PINJAM, GET_STATIC_FORM_PINJAM_SUCCESS, GET_STATIC_FORM_PINJAM_FAILURE, POST_ORDER_PINJAM, POST_ORDER_PINJAM_SUCCESS, POST_ORDER_PINJAM_FAILURE} from './types';

export const getContentFormPinjam = () => ({
    type: GET_STATIC_FORM_PINJAM
});

export const getContentFormPinjamSuccess = response => ({
    type: GET_STATIC_FORM_PINJAM_SUCCESS,
    payload: response
});

export const getContentFormPinjamFailure = error => ({
    type: GET_STATIC_FORM_PINJAM_FAILURE,
    payload: error
});

export const postOrderPinjam = FormData => ({
    type: POST_ORDER_PINJAM,
    payload: FormData
});

export const postOrderPinjamSuccess = response => ({
    type: POST_ORDER_PINJAM_SUCCESS,
    payload: response
});

export const postOrderPinjamFailure = error => ({
    type: POST_ORDER_PINJAM_FAILURE,
    payload: error
});