/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 06:17:43 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-18 23:06:12
 */
import { POST_LOGIN, POST_LOGIN_SUCCESS, POST_LOGIN_FAILURE, CLEAR_PREV_LOGIN , REFRESH_NEW_LOGIN} from "./types";

export const postLogin = params => ({
    type: POST_LOGIN,
    payload: params
});

export const postLoginSuccess = response => ({
    type: POST_LOGIN_SUCCESS,
    payload: response
});

export const postLoginFailure = error => ({
    type: POST_LOGIN_FAILURE,
    payload: error
});

export const clearPrevLogin = () => ({
    type: CLEAR_PREV_LOGIN
});

export const refreshNewLogin = response => ({
    type: REFRESH_NEW_LOGIN,
    payload: response
})