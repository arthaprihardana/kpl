/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:26:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-09 22:26:57
 */
import { GET_AGAMA, GET_AGAMA_SUCCESS, GET_AGAMA_FAILURE } from "./types";

export const getAgama = () => ({
    type: GET_AGAMA
});

export const getAgamaSuccess = response => ({
    type: GET_AGAMA_SUCCESS,
    payload: response
});

export const getAgamaFailure = error => ({
    type: GET_AGAMA_FAILURE,
    payload: error
});