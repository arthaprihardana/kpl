/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-20 08:17:22 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-05-20 08:17:22 
 */
import { NETWORK_STATUS } from "./types";

export const networkStatus = params => ({
    type: NETWORK_STATUS,
    payload: params
});