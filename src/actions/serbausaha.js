/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-19 22:22:55 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-06-19 22:22:55 
 */
import {GET_LIST_SERBA_USAHA, GET_LIST_SERBA_USAHA_SUCCESS, GET_LIST_SERBA_USAHA_FAILURE, GET_DETAIL_SERBA_USAHA, GET_DETAIL_SERBA_USAHA_SUCCESS, GET_DETAIL_SERBA_USAHA_FAILURE, POST_SERBA_USAHA, POST_SERBA_USAHA_SUCCESS, POST_SERBA_USAHA_FAILURE} from './types';

export const getListSerbaUsaha = () => ({
    type: GET_LIST_SERBA_USAHA
});

export const getListSerbaUsahaSuccess = response => ({
    type: GET_LIST_SERBA_USAHA_SUCCESS,
    payload: response
});

export const getListSerbaUsahaFailure = error => ({
    type: GET_LIST_SERBA_USAHA_FAILURE,
    payload: error
});

export const getDetailSerbaUsaha = params => ({
    type: GET_DETAIL_SERBA_USAHA,
    payload: params
});

export const getDetailSerbaUsahaSuccess = response => ({
    type: GET_DETAIL_SERBA_USAHA_SUCCESS,
    payload: response
});

export const getDetailSerbaUsahaFailure = error => ({
    type: GET_DETAIL_SERBA_USAHA_FAILURE,
    payload: error
});

export const postOrderSerbaUsaha = FormData => ({
    type: POST_SERBA_USAHA,
    payload: FormData
});

export const postOrderSerbaUsahaSuccess = response => ({
    type: POST_SERBA_USAHA_SUCCESS,
    payload: response
});

export const postOrderSerbaUsahaFailure = error => ({
    type: POST_SERBA_USAHA_FAILURE,
    payload: error
});