/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-15 19:29:02 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-05-15 19:29:02 
 */
import { GET_BANNER_PROMO, GET_BANNER_PROMO_SUCCESS, GET_BANNER_PROMO_FAILURE } from "./types";

export const getBannerPromo = () => ({
    type: GET_BANNER_PROMO
});

export const getBannerPromoSuccess = response => ({
    type: GET_BANNER_PROMO_SUCCESS,
    payload: response
});

export const getBannerPromoFailure = error => ({
    type: GET_BANNER_PROMO_FAILURE,
    payload: error
});