/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:57:58 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-05-09 22:57:58 
 */
import { GET_ALL_LAYANAN, GET_ALL_LAYANAN_SUCCESS, GET_ALL_LAYANAN_FAILURE } from "./types";

export const getLayanan = () => ({
    type: GET_ALL_LAYANAN
});

export const getLayananSuccess = response => ({
    type: GET_ALL_LAYANAN_SUCCESS,
    payload: response
});

export const getLayananFailure = error => ({
    type: GET_ALL_LAYANAN_FAILURE,
    payload: error
});