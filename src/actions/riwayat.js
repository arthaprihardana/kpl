/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 13:13:25 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-06-21 13:13:25 
 */
import {GET_RIWAYAT, GET_RIWAYAT_SUCCESS, GET_RIWAYAT_FAILURE, GET_DETAIL_RIWAYAT, GET_DETAIL_RIWAYAT_SUCCESS, GET_DETAIL_RIWAYAT_FAILURE} from './types';

export const getRiwayat = params => ({
    type: GET_RIWAYAT,
    payload: params
});

export const getRiwayatSuccess = response => ({
    type: GET_RIWAYAT_SUCCESS,
    payload: response
});

export const getRiwayatFailure = error => ({
    type: GET_RIWAYAT_FAILURE,
    payload: error
});

export const getDetailRiwayat = params => ({
    type: GET_DETAIL_RIWAYAT,
    payload: params
});

export const getDetailRiwayatSuccess = response => ({
    type: GET_DETAIL_RIWAYAT_SUCCESS,
    payload: response
});

export const getDetailRiwayatFailure = error => ({
    type: GET_DETAIL_RIWAYAT_FAILURE,
    payload: error
});