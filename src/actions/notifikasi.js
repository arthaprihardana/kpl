/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-31 20:57:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-31 20:58:01
 */
import { GET_NOTIFIKASI, GET_NOTIFIKASI_SUCCESS, GET_NOTIFIKASI_FAILURE } from "./types";

export const getNotifikasi = params => ({
    type: GET_NOTIFIKASI,
    payload: params
});

export const getNotifikasiSuccess = response => ({
    type: GET_NOTIFIKASI_SUCCESS,
    payload: response
});

export const getNotifikasiFailure = error => ({
    type: GET_NOTIFIKASI_FAILURE,
    payload: error
});