/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:26:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-18 10:40:58
 */
import { GET_MASTER_STASIUN, GET_MASTER_STASIUN_FAILURE, GET_MASTER_STASIUN_SUCCESS } from "./types";

export const getStasiun = () => ({
    type: GET_MASTER_STASIUN
});

export const getStasiunSuccess = response => ({
    type: GET_MASTER_STASIUN_SUCCESS,
    payload: response
});

export const getStasiunFailure = error => ({
    type: GET_MASTER_STASIUN_FAILURE,
    payload: error
});