/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:26:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 20:23:55
 */
import { GET_MASTER_KOTA, GET_MASTER_KOTA_SUCCESS, GET_MASTER_KOTA_FAILURE } from "./types";

export const getKota = () => ({
    type: GET_MASTER_KOTA
});

export const getKotaSuccess = response => ({
    type: GET_MASTER_KOTA_SUCCESS,
    payload: response
});

export const getKotaFailure = error => ({
    type: GET_MASTER_KOTA_FAILURE,
    payload: error
});