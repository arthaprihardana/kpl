/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:26:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-28 01:49:34
 */
import { GET_TOKO, GET_TOKO_SUCCESS, GET_TOKO_FAILURE, GET_DETAIL_PRODUK, GET_DETAIL_PRODUK_SUCCESS, GET_RELATED_PRODUK, GET_RELATED_PRODUK_SUCCESS, GET_RELATED_PRODUK_FAILURE, ADD_TO_CART, CLEAN_CART, DELETE_FROM_CART, POST_ORDER_TOKO, POST_ORDER_TOKO_FAILURE, POST_ORDER_TOKO_SUCCESS, GET_SEARCH_TOKO_SUCCESS, GET_SEARCH_TOKO_FAILURE, GET_SEARCH_TOKO, GET_MASTER_KATEGORI_PRODUK, GET_MASTER_KATEGORI_PRODUK_SUCCESS, GET_MASTER_KATEGORI_PRODUK_FAILURE, GET_PRODUK_BY_KATEGORI, GET_PRODUK_BY_KATEGORI_SUCCESS, GET_PRODUK_BY_KATEGORI_FAILURE } from "./types";

export const getToko = params => ({
    type: GET_TOKO,
    payload: params
});

export const getTokoSuccess = response => ({
    type: GET_TOKO_SUCCESS,
    payload: response
});

export const getTokoFailure = error => ({
    type: GET_TOKO_FAILURE,
    payload: error
});

export const getDetailProduk = params => ({
    type: GET_DETAIL_PRODUK,
    payload: params
});

export const getDetailProdukSuccess = response => ({
    type: GET_DETAIL_PRODUK_SUCCESS,
    payload: response
});

export const getDetailProdukFailure = error => ({
    type: GET_DETAIL_PRODUK_FAILURE,
    payload: error
});

export const getRelatedProduk = params => ({
    type: GET_RELATED_PRODUK,
    payload: params
});

export const getRelatedProdukSuccess = response => ({
    type: GET_RELATED_PRODUK_SUCCESS,
    payload: response
});

export const getRelatedProdukFailure = error => ({
    type: GET_RELATED_PRODUK_FAILURE,
    payload: error
});

export const addToCart = data => ({
    type: ADD_TO_CART,
    payload: data
});

export const deleteFromCart = id => ({
    type: DELETE_FROM_CART,
    payload: id
});

export const cleanCart = () => ({
    type: CLEAN_CART
});

export const postOrderToko = params => ({
    type: POST_ORDER_TOKO,
    payload: params
});

export const postOrderTokoSuccess = response => ({
    type: POST_ORDER_TOKO_SUCCESS,
    payload: response
});

export const postOrderTokoFailure = error => ({
    type: POST_ORDER_TOKO_FAILURE,
    payload: error
});

export const getSearchToko = params => ({
    type: GET_SEARCH_TOKO,
    payload: params
});

export const getSearchTokoSuccess = response => ({
    type: GET_SEARCH_TOKO_SUCCESS,
    payload: response
});

export const getSearchTokoFailure = error => ({
    type: GET_SEARCH_TOKO_FAILURE,
    payload: error
});

export const getMasterKategoriProduk = params => ({
    type: GET_MASTER_KATEGORI_PRODUK,
    payload: params
});

export const getMasterKategoriProdukSuccess = response => ({
    type: GET_MASTER_KATEGORI_PRODUK_SUCCESS,
    payload: response
});

export const getMasterKategoriProdukFailure = error => ({
    type: GET_MASTER_KATEGORI_PRODUK_FAILURE,
    payload: error
});

export const getProductByCategory = params => ({
    type: GET_PRODUK_BY_KATEGORI,
    payload: params
});

export const getProductByCategorySuccess = response => ({
    type: GET_PRODUK_BY_KATEGORI_SUCCESS,
    payload: response
});

export const getProductByCategoryFailure = error => ({
    type: GET_PRODUK_BY_KATEGORI_FAILURE,
    payload: error
});