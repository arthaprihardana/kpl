/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-07 23:23:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-18 22:18:16
 */
import {GET_GEDUNG_LIST, GET_GEDUNG_LIST_SUCCESS, GET_GEDUNG_LIST_FAILURE, GET_GEDUNG_DETAIL, GET_GEDUNG_DETAIL_SUCCESS, GET_GEDUNG_DETAIL_FAILURE, POST_ORDER_GEDUNG, POST_ORDER_GEDUNG_SUCCESS, POST_ORDER_GEDUNG_FAILURE, GET_BOOKING_DATE, GET_BOOKING_DATE_SUCCESS, GET_BOOKING_DATE_FAILURE, GET_REKANAN, GET_REKANAN_SUCCESS, GET_REKANAN_FAILURE} from './types';

export const getGedungList = () => ({
    type: GET_GEDUNG_LIST
});

export const getGedungListSuccess = response => ({
    type: GET_GEDUNG_LIST_SUCCESS,
    payload: response
});

export const getGedungListFailure = error => ({
    type: GET_GEDUNG_LIST_FAILURE,
    payload: error
});

export const getGedungDetail = params => ({
    type: GET_GEDUNG_DETAIL,
    payload: params
});

export const getGedungDetailSuccess = response => ({
    type: GET_GEDUNG_DETAIL_SUCCESS,
    payload: response
});

export const getGedungDetailFailure = error => ({
    type: GET_GEDUNG_DETAIL_FAILURE,
    payload: error
});

export const postOrderGedung = params => ({
    type: POST_ORDER_GEDUNG,
    payload: params
});

export const postOrderGedungSuccess = response => ({
    type: POST_ORDER_GEDUNG_SUCCESS,
    payload: response
});

export const postOrderGedungFailure = error => ({
    type: POST_ORDER_GEDUNG_FAILURE,
    payload: error
});

export const getBookingDate = params => ({
    type: GET_BOOKING_DATE,
    payload: params
});

export const getBookingDateSuccess = response => ({
    type: GET_BOOKING_DATE_SUCCESS,
    payload: response
});

export const getBookingDateFailure = error => ({
    type: GET_BOOKING_DATE_FAILURE,
    payload: error
});

export const getRekanan = params => ({
    type: GET_REKANAN,
    payload: params
});

export const getRekananSuccess = response => ({
    type: GET_REKANAN_SUCCESS,
    payload: response
});

export const getRekananFailure = error => ({
    type: GET_REKANAN_FAILURE,
    payload: error
});
