/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:26:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 19:59:02
 */
import { GET_MASTER_BANDARA, GET_MASTER_BANDARA_FAILURE, GET_MASTER_BANDARA_SUCCESS } from "./types";

export const getBandara = () => ({
    type: GET_MASTER_BANDARA
});

export const getBandaraSuccess = response => ({
    type: GET_MASTER_BANDARA_SUCCESS,
    payload: response
});

export const getBandaraFailure = error => ({
    type: GET_MASTER_BANDARA_FAILURE,
    payload: error
});