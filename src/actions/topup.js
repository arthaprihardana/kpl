/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-25 22:11:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 21:03:54
 */
import { POST_ORDER_PULSA, POST_ORDER_PULSA_SUCCESS, POST_ORDER_PULSA_FAILURE, POST_ORDER_PAKET_DATA, POST_ORDER_PAKET_DATA_SUCCESS, POST_ORDER_PAKET_DATA_FAILURE , POST_ORDER_LISTRIK_TOKEN, POST_ORDER_LISTRIK_TOKEN_SUCCESS, POST_ORDER_LISTRIK_TOKEN_FAILURE, POST_ORDER_LISTRIK_TAGIHAN, POST_ORDER_LISTRIK_TAGIHAN_SUCCESS, POST_ORDER_LISTRIK_TAGIHAN_FAILURE} from "./types";

export const postOrderPulsa = params => ({
    type: POST_ORDER_PULSA,
    payload: params
});

export const postOrderPulsaSuccess = response => ({
    type: POST_ORDER_PULSA_SUCCESS,
    payload: response
});

export const postOrderPulsaFailure = error => ({
    type: POST_ORDER_PULSA_FAILURE,
    payload: error
});

export const postOrderPaketData = params => ({
    type: POST_ORDER_PAKET_DATA,
    payload: params
});

export const postOrderPaketDataSuccess = response => ({
    type: POST_ORDER_PAKET_DATA_SUCCESS,
    payload: response
});

export const postOrderPaketDataFailure = error => ({
    type: POST_ORDER_PAKET_DATA_FAILURE,
    payload: error
});

export const postOrderListrikToken = params => ({
    type: POST_ORDER_LISTRIK_TOKEN,
    payload: params
});

export const postOrderListrikTokenSuccess = response => ({
    type: POST_ORDER_LISTRIK_TOKEN_SUCCESS,
    payload: response
});

export const postOrderListrikTokenFailure = error => ({
    type: POST_ORDER_LISTRIK_TOKEN_FAILURE,
    payload: error
});

export const postOrderListrikTagihan = params => ({
    type: POST_ORDER_LISTRIK_TAGIHAN,
    payload: params
});

export const postOrderListrikTagihanSuccess = response => ({
    type: POST_ORDER_LISTRIK_TAGIHAN_SUCCESS,
    payload: response
});

export const postOrderListrikTagihanFailure = error => ({
    type: POST_ORDER_LISTRIK_TAGIHAN_FAILURE,
    payload: error
});