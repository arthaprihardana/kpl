/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 20:37:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 20:39:50
 */
import {GET_STATIC_FORM_SIMPAN, GET_STATIC_FORM_SIMPAN_SUCCESS, GET_STATIC_FORM_SIMPAN_FAILURE, POST_ORDER_SIMPAN, POST_ORDER_SIMPAN_SUCCESS, POST_ORDER_SIMPAN_FAILURE} from './types';

export const getContentFormSimpan = () => ({
    type: GET_STATIC_FORM_SIMPAN
});

export const getContentFormSimpanSuccess = response => ({
    type: GET_STATIC_FORM_SIMPAN_SUCCESS,
    payload: response
});

export const getContentFormSimpanFailure = error => ({
    type: GET_STATIC_FORM_SIMPAN_FAILURE,
    payload: error
});

export const postOrderSimpan = FormData => ({
    type: POST_ORDER_SIMPAN,
    payload: FormData
});

export const postOrderSimpanSuccess = response => ({
    type: POST_ORDER_SIMPAN_SUCCESS,
    payload: response
});

export const postOrderSimpanFailure = error => ({
    type: POST_ORDER_SIMPAN_FAILURE,
    payload: error
});