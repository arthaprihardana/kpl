/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-17 09:40:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 09:50:48
 */
import { POST_ORDER_PESAWAT, POST_ORDER_PESAWAT_SUCCESS, POST_ORDER_PESAWAT_FAILURE, POST_ORDER_HOTEL, POST_ORDER_HOTEL_SUCCESS, POST_ORDER_HOTEL_FAILURE, POST_ORDER_KERETA, POST_ORDER_KERETA_SUCCESS, POST_ORDER_KERETA_FAILURE, POST_ORDER_BUS, POST_ORDER_BUS_SUCCESS, POST_ORDER_BUS_FAILURE, POST_ORDER_SHUTTLE, POST_ORDER_SHUTTLE_SUCCESS, POST_ORDER_SHUTTLE_FAILURE, CLEAR_PREV_TIKET } from "./types";

// PESAWAT
export const postOrderPesawat = form => ({
    type: POST_ORDER_PESAWAT,
    payload: form
});

export const postOrderPesawatSuccess = response => ({
    type: POST_ORDER_PESAWAT_SUCCESS,
    payload: response
});

export const postOrderPesawatFailure = error => ({
    type: POST_ORDER_PESAWAT_FAILURE,
    payload: error
});

// HOTEL
export const postOrderHotel = form => ({
    type: POST_ORDER_HOTEL,
    payload: form
});

export const postOrderHotelSuccess = response => ({
    type: POST_ORDER_HOTEL_SUCCESS,
    payload: response
});

export const postOrderHotelFailure = error => ({
    type: POST_ORDER_HOTEL_FAILURE,
    payload: error
});

// KERETA
export const postOrderKereta = form => ({
    type: POST_ORDER_KERETA,
    payload: form
});

export const postOrderKeretaSuccess = response => ({
    type: POST_ORDER_KERETA_SUCCESS,
    payload: response
});

export const postOrderKeretaFailure = error => ({
    type: POST_ORDER_KERETA_FAILURE,
    payload: error
});

// BUS
export const postOrderBus = form => ({
    type: POST_ORDER_BUS,
    payload: form
});

export const postOrderBusSuccess = response => ({
    type: POST_ORDER_BUS_SUCCESS,
    payload: response
});

export const postOrderBusFailure = error => ({
    type: POST_ORDER_BUS_FAILURE,
    payload: error
});

// SHUTTLE
export const postOrderShuttle = form => ({
    type: POST_ORDER_SHUTTLE,
    payload: form
});

export const postOrderShuttleSuccess = response => ({
    type: POST_ORDER_SHUTTLE_SUCCESS,
    payload: response
});

export const postOrderShuttleFailure = error => ({
    type: POST_ORDER_SHUTTLE_FAILURE,
    payload: error
});

export const clearPrevTiket = () => ({
    type: CLEAR_PREV_TIKET
});