/**
 * @Author: Artha Prihardana
 * @Date: 2019-10-03 18:12:13
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-03 19:01:53
 */
import { GET_FORCE_UPDATE, GET_FORCE_UPDATE_SUCCESS, GET_FORCE_UPDATE_FAILURE } from "./types";

export const getForceUpdate = () => ({
    type: GET_FORCE_UPDATE
});

export const getForceUpdateSuccess = response => ({
    type: GET_FORCE_UPDATE_SUCCESS,
    payload: response
});

export const getForceUpdateFailure = error => ({
    type: GET_FORCE_UPDATE_FAILURE,
    payload: error
});