/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-30 22:58:49 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-31 23:35:13
 */
import { TAKE_PICTURE , POST_FOTO_PROFIL, POST_FOTO_PROFIL_SUCCESS, POST_FOTO_PROFIL_FAILURE} from "./types";

export const onTakePicture = picture => ({
    type: TAKE_PICTURE,
    payload: picture
});

export const postFotoProfil = form => ({
    type: POST_FOTO_PROFIL,
    payload: form
});

export const postFotoProfilSuccess = response => ({
    type: POST_FOTO_PROFIL_SUCCESS,
    payload: response
});

export const postFotoProfilFailure = error => ({
    type: POST_FOTO_PROFIL_FAILURE,
    payload: error
});