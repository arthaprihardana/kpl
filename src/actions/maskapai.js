/**
 * @Author: Artha Prihardana
 * @Date: 2019-10-10 23:41:39
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-10 23:42:01
 */
import { GET_MASTER_MASKAPAI, GET_MASTER_MASKAPAI_SUCCESS, GET_MASTER_MASKAPAI_FAILURE } from "./types";

export const getMaskapai = () => ({
  type: GET_MASTER_MASKAPAI
});

export const getMaskapaiSuccess = response => ({
  type: GET_MASTER_MASKAPAI_SUCCESS,
  payload: response
});

export const getMaskapaiFailure = error => ({
  type: GET_MASTER_MASKAPAI_FAILURE,
  payload: error
});