/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:26:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-25 01:27:40
 */
import { GET_BERITA, GET_BERITA_SUCCESS, GET_BERITA_FAILURE, GET_DETAIL_BERITA, GET_DETAIL_BERITA_SUCCESS, GET_DETAIL_BERITA_FAILURE } from "./types";

export const getBerita = params => ({
    type: GET_BERITA,
    payload: params
});

export const getBeritaSuccess = response => ({
    type: GET_BERITA_SUCCESS,
    payload: response
});

export const getBeritaFailure = error => ({
    type: GET_BERITA_FAILURE,
    payload: error
});

export const getDetailBerita = params => ({
    type: GET_DETAIL_BERITA,
    payload: params
});

export const getDetailBeritaSuccess = response => ({
    type: GET_DETAIL_BERITA_SUCCESS,
    payload: response
});

export const getDetailBeritaFailure = error => ({
    type: GET_DETAIL_BERITA_FAILURE,
    payload: error
});