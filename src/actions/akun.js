/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-26 19:49:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-26 19:51:27
 */
import {PUT_AKUN, PUT_AKUN_SUCCESS, PUT_AKUN_FAILURE, GET_CHECK_USERNAME, GET_CHECK_USERNAME_SUCCESS, GET_CHECK_USERNAME_FAILURE, GET_SALDO, GET_SALDO_SUCCESS, GET_SALDO_FAILURE} from './types';

export const getCheckUsername = params => ({
    type: GET_CHECK_USERNAME,
    payload: params
});

export const getCheckUsernameSuccess = response => ({
    type: GET_CHECK_USERNAME_SUCCESS,
    payload: response
});

export const getCheckUsernameFailure = error => ({
    type: GET_CHECK_USERNAME_FAILURE,
    payload: error
})

export const updateUser = FormData => ({
    type: PUT_AKUN,
    payload: FormData
});

export const updateUserSuccess = response => ({
    type: PUT_AKUN_SUCCESS,
    payload: response
});

export const updateUserFailure = error => ({
    type: PUT_AKUN_FAILURE,
    payload: error
});

export const getSaldo = FormData => ({
    type: GET_SALDO,
    payload: FormData
});

export const getSaldoSuccess = response => ({
    type: GET_SALDO_SUCCESS,
    payload: response
});

export const getSaldoFailure = error => ({
    type: GET_SALDO_FAILURE,
    payload: error
});