/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 06:17:43 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-14 23:18:05
 */
import { POST_REGISTER, POST_REGISTER_SUCCESS, POST_REGISTER_FAILURE, CLEAR_PREV_REGISTER } from "./types";

export const postRegister = params => ({
    type: POST_REGISTER,
    payload: params
});

export const postRegisterSuccess = response => ({
    type: POST_REGISTER_SUCCESS,
    payload: response
});

export const postRegisterFailure = error => ({
    type: POST_REGISTER_FAILURE,
    payload: error
});

export const clearPrevRegister = () => ({
    type: CLEAR_PREV_REGISTER
});