/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 05:44:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-10 23:46:09
 */
import Config from 'react-native-config';
import {objectToQueryString} from './helpers/index';
const BASE_URL =  `${Config.BASE_URL}/public`;

export const CONFIG_API = "B";  // A = Axios ; B = Fetch
export const PROVIDER = [{
    name: "Telkomsel",
    pattern: /^(\+62|\+0|0|62)8(1[123]|2[12]|51|52|53|21|22|23)[0-9]{5,9}$/g,
    paket: [{"id":1,"nama":"Data 25.000"},{"id":2,"nama":"Data GameMAX 25.000"},{"id":3,"nama":"Data 50.000"},{"id":4,"nama":"Data 12GB"},{"id":5,"nama":"Data 50GB"},{"id":6,"nama":"VideoMax 50.000"},{"id":7,"nama":"Paket Bicara Rp 25.000"}]
}, {
    name: "Indosat Ooredoo",
    pattern: /^(\+62|\+0|0|62)8(1[456]|5[5678]|14|15|16|55|56|57|58)[0-9]{5,9}$/g,
    paket: [{"id":1,"nama":"[PRODUK EKSKLUSIF] Freedom 14GB, 30 Hari"},{"id":2,"nama":"[PRODUK EKSKLUSIF] Freedom 20GB, 30 Hari"},{"id":3,"nama":"*PROMO Internet Unlimited +15GB"},{"id":4,"nama":"*PROMO Internet Unlimited +3GB (Berlangganan 6 Bulan)"},{"id":5,"nama":"*PROMO Internet Unlimited +3GB (Berlangganan 12 Bulan)"},{"id":6,"nama":"Freedom Combo M 12GB"},{"id":7,"nama":"Freedom Combo L 26GB"},{"id":8,"nama":"Freedom Combo XL 41GB"},{"id":9,"nama":"Freedom Combo XXL 65GB"},{"id":10,"nama":"Internet Unlimited + 1GB"},{"id":11,"nama":"Internet Unlimited + 2GB"},{"id":12,"nama":"Internet Unlimited + 3GB"},{"id":13,"nama":"Internet Unlimited + 5GB"},{"id":14,"nama":"Internet Unlimited + 7GB"},{"id":15,"nama":"Internet Unlimited + 10GB"},{"id":16,"nama":"Internet Unlimited + 15GB"},{"id":17,"nama":"Internet Unlimited Jumbo"},{"id":18,"nama":"Internet Unlimited +3GB (Berlangganan 6 Bulan)"},{"id":19,"nama":"Internet Unlimited +3GB (Berlangganan 12 Bulan)"},{"id":20,"nama":"Paket Extra 2GB"},{"id":21,"nama":"Paket Extra 4GB"},{"id":22,"nama":"Paket Extra 6GB"},{"id":23,"nama":"Freedom Internet Plus 1GB"},{"id":24,"nama":"Freedom Internet Plus 2GB"},{"id":25,"nama":"Freedom Internet Plus 4GB"},{"id":26,"nama":"Freedom Internet Plus 5GB"},{"id":27,"nama":"Paket Unlimited Nelpon Sesama + 250min Semua Operator"}]
}, {
    name: "XL Axiata",
    pattern: /^(\+62|\+0|0|62)8(1[789]|5[9]|7[78]|17|18|19|59|77|78)[0-9]{5,9}$/g,
    paket: [{"id":1,"nama":"XTRA Combo VIP 5GB+5GB"},{"id":2,"nama":"XTRA Combo VIP 10GB+10GB"},{"id":3,"nama":"XTRA Combo VIP 15GB+15GB"},{"id":4,"nama":"XTRA Combo VIP 20GB+20GB"},{"id":5,"nama":"XTRA Combo VIP 35GB+35GB"},{"id":6,"nama":"*PROMO XTRA COMBO 10GB"},{"id":7,"nama":"XTRA COMBO 5GB"},{"id":8,"nama":"XTRA COMBO 10GB"},{"id":9,"nama":"XTRA COMBO 15GB"},{"id":10,"nama":"XTRA COMBO 20GB"},{"id":11,"nama":"XTRA COMBO 35GB"},{"id":12,"nama":"Hotrod 1.5GB, 24 Jam"},{"id":13,"nama":"HotRod 800MB, 30hr"},{"id":14,"nama":"HotRod 3GB, 30hr"},{"id":15,"nama":"HotRod 6GB, 30hr"},{"id":16,"nama":"HotRod 8GB, 30hr"},{"id":17,"nama":"HotRod 12GB, 30hr"},{"id":18,"nama":"HotRod 16GB, 30hr"},{"id":19,"nama":"12 Bulan XTRA Combo 12X 6GB"},{"id":20,"nama":"12 Bulan XTRA Combo 12X 12GB"},{"id":21,"nama":"12 Bulan XTRA Combo 12X 18GB"},{"id":22,"nama":"12 Bulan XTRA Combo 12X 30GB"},{"id":23,"nama":"12 Bulan XTRA Combo 12X 42GB"},{"id":24,"nama":"AnyNet 500 Menit 90 Hari"},{"id":25,"nama":"*PROMO AnyNet 250 Menit 30 Hari"},{"id":26,"nama":"*PROMO Anynet 325 Menit 30 hari"},{"id":27,"nama":"*PROMO Anynet 500 Menit 30hari"},{"id":28,"nama":"AnyNet 300 Menit 90 Hari"},{"id":29,"nama":"AnyNet 500 Menit 30 Hari"},{"id":30,"nama":"AnyNet 400 Menit 7 Hari"},{"id":31,"nama":"AnyNet 325 Menit 30 Hari"},{"id":32,"nama":"AnyNet 150 Menit 30 Hari"},{"id":33,"nama":"AnyNet 250 Menit 30 Hari"},{"id":34,"nama":"Xtra Kuota 30GB, 30hr"}]
}, {
    name: "AXIS",
    pattern: /^(\+62|\+0|0|62)8(3[1238]|31|32|33|38)[0-9]{5,9}$/g,
    paket: [{"id":1,"nama":"AXBB30"},{"id":2,"nama":"BRONET 4G OWSEM 1GB + 1GB"},{"id":3,"nama":"BRONET 4G OWSEM, 1GB + 3GB"},{"id":4,"nama":"BRONET 4G OWSEM, 2GB + 6GB"},{"id":5,"nama":"BRONET 4G OWSEM, 3GB + 9GB"},{"id":6,"nama":"BRONET 24jam, 10GB, 60hr"},{"id":7,"nama":"BRONET 24Jam, 12GB, 30hr"},{"id":8,"nama":"BRONET 24jam, 16GB, 60hr"},{"id":9,"nama":"Paket KZL Games (Vainglory) 500MB 7 Hari"},{"id":10,"nama":"Paket KZL Games (Vainglory) 1GB 30 Hari"},{"id":11,"nama":"Paket KZL SocMed (FB/IG/Twitter) Unlimited 7 Hari"},{"id":12,"nama":"Paket KZL SocMed (FB/IG/Twitter) Unlimited 30 Hari"},{"id":13,"nama":"Paket KZL Chat (WhatsApp/LINE/BBM) Unlimited 7 Hari"},{"id":14,"nama":"Paket KZL Chat (WhatsApp/LINE/BBM) Unlimited 30 Hari"},{"id":15,"nama":"Paket KZL Combo SocMed+Chat Unlimited 7 Hari"},{"id":16,"nama":"Paket KZL Combo SocMed+Chat Unlimited 30 Hari"},{"id":17,"nama":"*Promo BRONET 24Jam 8GB 60Hr"},{"id":18,"nama":"*Promo BRONET 24Jam 10GB 60Hr"},{"id":19,"nama":"*Promo BRONET 24Jam 12GB 60Hr"},{"id":20,"nama":"*Promo BRONET 24Jam 16GB 60Hr"}]
}, {
    name: "Smartfren",
    pattern: /^(\+62|\+0|0|62)8(8[123456789]|81|82|83|84|85|86|87|88|89)[0-9]{5,9}$/g,
    paket: [{"id":1,"nama":"Paket data MIFI 100 GB"},{"id":2,"nama":"Paket data MIFI 420 GB"},{"id":3,"nama":"Internet 10 ribu"},{"id":4,"nama":"Internet 20 ribu"},{"id":5,"nama":"Internet 30 ribu"},{"id":6,"nama":"Internet 60 ribu"},{"id":7,"nama":"Internet 100 ribu"},{"id":8,"nama":"Internet 150 ribu"},{"id":9,"nama":"Internet 200 ribu"},{"id":10,"nama":"Paket data MIFI 200 GB"},{"id":11,"nama":"Unlimited Smartfren 65 RB"}]
}, {
    name: "3 Indonesia",
    pattern: /^(\+62|\+0|0|62)8(9[56789]|95|96|97|98|99)[0-9]{5,9}$/g,
    paket: [{"id":1,"nama":"[Promo Produk Eksklusif] 15GB"},{"id":2,"nama":"Mix Kuota ++ 1.25 GB"},{"id":3,"nama":"Mix Kuota ++ 2.25GB"},{"id":4,"nama":"Mix Super 30 GB"},{"id":5,"nama":"Mix Combo 38 GB + 30 menit"},{"id":6,"nama":"Mix Small 3.75GB"},{"id":7,"nama":"Mix Combo 2 GB + 20 menit"},{"id":8,"nama":"Mix Combo 32 GB + 30 menit"},{"id":9,"nama":"Mix Small 3GB"},{"id":10,"nama":"Mix Small 2.75GB"},{"id":11,"nama":"Mix Small 5GB"},{"id":12,"nama":"Mix Super 10GB"}]
}]

export const API_REGISTER = `${BASE_URL}/register`;
export const API_LOGIN = `${BASE_URL}/login`;

// MASTER AGAMA
export const API_AGAMA = `${BASE_URL}/master/agama`;
// MASTER ALL LAYANAN
export const API_LAYANAN = `${BASE_URL}/all-layanan`;

// TRAVEL
export const API_PESAWAT = `${BASE_URL}/travel/pesawat`;
export const API_HOTEL = `${BASE_URL}/travel/hotel`;
export const API_KERETA = `${BASE_URL}/travel/kereta`;
export const API_BUS = `${BASE_URL}/travel/bus`;
export const API_SHUTTLE = `${BASE_URL}/travel/shuttle`;

// TAGIHAN
export const API_PULSA = `${BASE_URL}/travel/pulsa`;
export const API_PAKET_DATA = `${BASE_URL}/travel/paketdata`;
export const API_LISTRIK_TOKEN = `${BASE_URL}/listrik-token`;
export const API_LISTRIK_TAGIHAN = `${BASE_URL}/listrik-tagihan`;

// BANNER PROMO
export const API_BANNER_PROMO = `${BASE_URL}/promo`;

// MASTER BANDARA
export const API_MASTER_BANDARA = `${BASE_URL}/master/bandara`;
// MASTER KOTA
export const API_MASTER_KOTA = `${BASE_URL}/master/kota`;
// MASTER STASIUN
export const API_MASTER_STASIUN = `${BASE_URL}/master/stasiun`;
// MASTER MASKAPAI
export const API_MASTER_MASKAPAI = `${BASE_URL}/master/maskapai`;

// BERITA
export const API_BERITA = prm => `${BASE_URL}/berita?limit=${prm.limit}&offset=${prm.offset}`;
export const API_DETAIL_BERITA = `${BASE_URL}/berita/detail`;

// TOKO
export const API_TOKO = prm => `${BASE_URL}/toko/list?limit=${prm.limit}&offset=${prm.offset}`;
export const API_DETAIL_PRODUK = `${BASE_URL}/toko/detail`;
export const API_RELATED_PRODUK = `${BASE_URL}/toko/related`;
export const API_ORDER_TOKO = `${BASE_URL}/toko/buy`;
export const API_TOKO_SEARCHING = `${BASE_URL}/toko/searching`;
export const API_MASTER_KATEGORI_PRODUK = `${BASE_URL}/toko/list_kategori`;
export const API_PRODUK_BY_KATEGORI = `${BASE_URL}/toko/pilih_kategori`;

// TOPUP
export const API_ORDER_PULSA = `${BASE_URL}/pulsa`;
export const API_ORDER_PAKET_DATA = `${BASE_URL}/paketdata`;

// UPDATE FOTO PROFIL
export const API_UPDATE_FOTO_PROFIL = `${BASE_URL}/profile/image`;

// SERBA USAHA
export const API_LIST_SERBA_USAHA = `${BASE_URL}/serba-usaha/list`;
export const API_ORDER_SERBA_USAHA = `${BASE_URL}/serba-usaha/submit`;
export const API_DETAIL_SERBA_USAHA = `${BASE_URL}/serba-usaha/detail`

// RIWAYAT
export const API_RIWAYAT = `${BASE_URL}/riwayat/order`;
export const API_DETAIL_RIWAYAT = `${BASE_URL}/riwayat/order/detail`;

// SIMPAN
export const API_SIMPAN = `${BASE_URL}/simpan/content-form`;
export const API_ORDER_SIMPAN = `${BASE_URL}/simpan/store`;

// PINJAM
export const API_PINJAM = `${BASE_URL}/pinjam/content-form`;
export const API_ORDER_PINJAM = `${BASE_URL}/pinjam/store`;

// AKUN
export const API_CHECK_USERNAME = `${BASE_URL}/profile/check-username`;
export const API_UPDATE_AKUN = `${BASE_URL}/profile/edit`;
export const API_SALDO = prm => `${BASE_URL}/saldo/sisa?${objectToQueryString(prm)}`;

// GEDUNG
export const API_GEDUNG_LIST = `${BASE_URL}/gedung/list`;
export const API_GEDUNG_DETAIL = `${BASE_URL}/gedung/detail`;
export const API_POST_ORDER_GEDUNG = `${BASE_URL}/gedung/submit`;
export const API_BOOKING_DATE = `${BASE_URL}/gedung/periode-booking`;
export const API_DETAIL_REKANAN = prm => `${BASE_URL}/rekanan/detail?${objectToQueryString(prm)}`;     // ?kelompok_id=1

// NOTIFIKASI
export const API_NOTIFIKASI = prm => `${BASE_URL}/notif/list?${objectToQueryString(prm)}`; // ?id_anggota=20180105-09440610&limit=1&offset=0

// FORCE UPDATE
export const API_FORCE_UPDATE = `${BASE_URL}/force-update`;