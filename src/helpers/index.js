/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-16 10:24:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 13:03:29
 */
import _ from "lodash";

export function greetings() {
    var today = new Date();
    var hourNow = today.getHours();
    var greeting ;
    
    if (hourNow >= 0 && hourNow < 11) {
        greeting = "Selamat Pagi";
    } else if (hourNow >= 11 && hourNow < 14) {
        greeting = "Selamat Siang";
    } else if (hourNow >= 14 && hourNow < 18) {
        greeting = "Selamat Sore";
    } else if (hourNow >= 18) {
        greeting = "Selamat Malam";
    }

    return greeting;
}

export function objectToQueryString(obj) {
    var qs = _.reduce(obj, (result, value, key) => {
        if (!_.isNull(value) && !_.isUndefined(value)) {
            if (_.isArray(value)) {
                result += _.reduce(value, (result1, value1) => {
                    if (!_.isNull(value1) && !_.isUndefined(value1)) {
                        result1 += key + '=' + value1 + '&';
                        return result1
                    } else {
                        return result1;
                    }
                }, '')
            } else {
                result += key + '=' + value + '&';
            }
            return result;
        } else {
            return result
        }
    }, '').slice(0, -1);
    return qs;
};

export function rupiah(number, digitDelimiter = ".") {
    if (!number) {
      return `Rp ${number}`;
    }

    number = number + "";

    let numberLength = number.length;
    const numberSplit = [];
    if (number.length > 3) {
        while (numberLength > 3) {
            numberSplit.push(number.slice(-3));
            number = number.slice(0, -3)
            numberLength = number.length;
        }

        numberSplit.push(number);
        numberSplit.reverse();

        number = numberSplit.join(digitDelimiter);
    }

    return `Rp ${number}`;
}
// export const rupiah = new Intl.NumberFormat('id-ID', {
//     style: 'currency',
//     currency: 'IDR',
//     minimumFractionDigits: 2
// });