/**
 * @author: Artha Prihardana 
 * @Date: 2019-08-05 00:10:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-08-05 00:20:44
 */
import { PermissionsAndroid, Platform } from 'react-native';

class AndroidPermissions {

    async requestReadContactPermission() {
        try {
            if(Platform.OS === "android") {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
                        title: 'KP Lemigas Read Contact Permission',
                        message:'KP Lemigas membutuhkan akses untuk membaca kontak',
                        buttonPositive: 'Lanjutkan',
                    }
                );
                console.log('granted ==>', granted)
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    return "granted";
                } else {
                    return "denied";
                }
            }
        } catch (error) {
            console.log(err);
        }
    }

    async requestUseCameraPermission() {
        try {
            if(Platform.OS === "android") {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA, {
                        title: 'KP Lemigas Access Camera Permission',
                        message:'KP Lemigas membutuhkan akses untuk menggunakan kamera',
                        buttonPositive: 'Lanjutkan',
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    return "granted"
                } else {
                    return "denied";
                }
            }
        } catch (error) {
            console.log(err);
        }
    }

    async requestReadStoragePermission() {
        try {
            if(Platform.OS === "android") {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
                        title: 'KP Lemigas Read External Storage Permission',
                        message:'KP Lemigas membutuhkan akses untuk membaca penyimpanan eksternal',
                        buttonPositive: 'Lanjutkan',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    return "granted"
                } else {
                    return "denied";
                }
            } else {
                return "granted";
            }
        } catch (err) {
            console.log(err);
        }
    }

    async requestWriteStoragePermission() {
        try {
            if(Platform.OS === "android") {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
                        title: 'KP Lemigas Write External Storage Permission',
                        message:'KP Lemigas membutuhkan akses untuk menyimpan di penyimpanan eksternal',
                        buttonPositive: 'Lanjutkan',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    return "granted"
                } else {
                    return "denied";
                }
            } else {
                return "granted";
            }
        } catch (err) {
            console.log(err);
        }
    }

}

export default AndroidPermissions;