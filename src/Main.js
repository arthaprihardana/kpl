/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:13:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-12 22:45:55
 */
import React, { Component } from 'react';
import { StyleSheet, ToastAndroid, View, BackHandler, StatusBar, UIManager, Platform, Text, Dimensions } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {StackViewStyleInterpolator} from 'react-navigation-stack';
import {Scene, Router, Actions, Reducer, ActionConst, Overlay, Modal, Tabs, Drawer, Stack, Lightbox} from 'react-native-router-flux';
import {connect, Provider} from 'react-redux';
import firebase from 'react-native-firebase';
import type { Messaging } from 'react-native-firebase';
import type { Notification, NotificationOpen } from 'react-native-firebase';
import configureStore from './store/configureStore';
import { NetworkIndicator } from './containers/component';
const store = configureStore();
const mapStateToProps = state => ({
    state: state.nav,
});
const RouterWithRedux = connect(mapStateToProps)(Router);

// COMPONENT
import { TabBar, NavBar, OptionDrawer } from './containers/component'
// PAGES
import SplashScreen from './containers/pages/splashscreen';
import Beranda from './containers/pages/beranda';
import Riwayat from './containers/pages/riwayat';
import DetailRiwayat from './containers/pages/riwayat/detail';
import Berita from './containers/pages/berita';
import DetailBerita from './containers/pages/berita/detail';
import Akun from './containers/pages/akun';
import Login from './containers/pages/login';
import Register from './containers/pages/register';
import Ticketing from './containers/pages/ticketing';
import Pesawat from './containers/pages/pesawat';
import Hotel from './containers/pages/hotel';
import Kereta from './containers/pages/kereta';
import Bus from './containers/pages/bus';
import Shuttle from './containers/pages/shuttle';
import Toko from './containers/pages/toko';
import Produk from './containers/pages/toko/produk';
import Keranjang from './containers/pages/toko/keranjang';
import Order from './containers/pages/toko/order';
import TopUp from './containers/pages/topup';
import Pulsa from './containers/pages/pulsa';
import PaketData from './containers/pages/paketdata';
import ListrikToken from './containers/pages/listriktoken';
import ListrikTagihan from './containers/pages/listriktagihan';
import Kamera from './containers/pages/kamera';
import Galeri from './containers/pages/galeri';
import Notifikasi from './containers/pages/notification';
import SerbaUsaha from './containers/pages/serbausaha';
import FormSerbaUsaha from './containers/pages/serbausaha/form';
import SimpanPinjam from './containers/pages/simpanpinjam';
import FormSimpanPinjam from './containers/pages/simpanpinjam/form';
import FormAkun from './containers/pages/akun/form';
import Gedung from './containers/pages/gedung';
import FormGedung from './containers/pages/gedung/form';
import Kalender from './containers/pages/gedung/kalender';
import Dashboard from './containers/pages/dashboard';

import { DARK_PRIMARY_COLOR, WHITE_COLOR, ERROR_COLOR, BORDER_COLOR, ICON_COLOR } from './assets/colors';

// transition page
const transitionConfig = () => ({
    screenInterpolator: StackViewStyleInterpolator.forFadeFromBottomAndroid,
});

const reducerCreate = params => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
        return defaultReducer(state, action);
    };
};

const getSceneStyle = () => ({
    backgroundColor: WHITE_COLOR,
    shadowOpacity: 1,
    shadowRadius: 3,
});

let exitCount = 0;
const onBackPress = () => {
    let route = Actions.state.routes;
    let main = route[0].routes[0].routes;
    if(main.length === 1) {
        let topSection = main[0].routeName;
        switch (topSection) {
            case "main":
                let tab = main[0].routes[0].routes[0].index;
                if(tab == 0) {
                    exitCount++;
                    ToastAndroid.showWithGravityAndOffset(
                        'Tekan lagi untuk keluar',
                        ToastAndroid.SHORT,
                        ToastAndroid.BOTTOM,
                        25,
                        100
                    );
                    if(exitCount == 2) {
                        exitCount = 0
                        BackHandler.exitApp()
                    }
                } else {
                    exitCount = 0
                    Actions.pop()
                }
                return true;
            default:
                Actions.pop();
                return true;
        }
    }
};

const { width } = Dimensions.get("window");

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
// UIManager.setLayoutAnimationEnabledExperimental(true);

export default class Main extends Component {

    state = {
        isConnected: null,
        connectivityType: null
    }

    async componentDidMount() {
        this.checkPermission();
        this.createNotificationListeners();
    }

    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async getToken() {
        let fcmToken = await firebase.messaging().getToken();
        console.log('fcmToken', fcmToken);
      }
    
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            this.getToken();
        } catch (error) {
            console.log('permission rejected');
        }
    }

    async createNotificationListeners() {
        // Request FCM Template
        // {
        //     "to":"e1Ms4pUkaoA:APA91bHWDTzTa85964XdgRpgRVibcSXpg5jkCrwKnawgULLjbr4S28vWN_QhqAgg2ADZotZzTKNeQp6zSQlhKzulsinuClWE7EW7z1vMBfLAQ4ABtecf7fybJg06KZCt_jjiBtaZW8AW",
        //     "notification" : {
        //         "title": "Hello",
        //         "body": "Description",
        //         "sound": "default",
        //         "priority": "high",
        //         "icon": "ic_notification",
        //         "show_in_foreground": true
        //     },
        //      "data":{
        //         "action":"com.kpl",
        //         "data": {
        //             "key": "value"
        //         }
        //     },
        //     "priority": "high"
        // }
        // Triger pada saat foreground
        this.notificationListener = firebase.notifications().onNotification(notification => {
            console.log('notification ==>', notification);
            const { title, body, data, notificationId } = notification;
            const channelId = new firebase.notifications.Android.Channel("Default", "Default", firebase.notifications.Android.Importance.High);
            firebase.notifications().android.createChannel(channelId);
            let notif = new  firebase.notifications.Notification({
                                                        data: data,
                                                        sound: 'default',
                                                        show_in_foreground: true,
                                                        title: title,
                                                        body: body,
                                                    });
    
            if(Platform.OS == "android") {
                notif
                    .android.setPriority(firebase.notifications.Android.Priority.High)
                    .android.setChannelId("Default")
                    .android.setVibrate(1000)
                    .setNotificationId(notificationId);
            }
    
            firebase.notifications().displayNotification(notif);
        });
      
        // Triger pada saat klik tray notifikasi
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened(notificationOpen => {
            console.log('klik tray ==>', notificationOpen);
            const { notificationId, data } = notificationOpen.notification;
            firebase.notifications().removeDeliveredNotification(notificationId);
            this.handleShowPage();
        });
      
        // handle jika ada notifikasi pada saat aplikasi tertutup
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            console.log('background ==>', notificationOpen);
            const { action, notification: { notificationId, data } } = notificationOpen;
            this.handleShowPage();
        }
    }
    
    componentWillUnmount() {
        this.notificationListener();
        this.notificationOpenedListener();
    }

    handleShowPage = () => {
        // Actions.push("")
        console.log('handle open page');
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar backgroundColor={DARK_PRIMARY_COLOR} barStyle="light-content" />
                <Provider store={store.store}>
                    {/* <PersistGate loading={null} persistor={store.persistor}> */}
                        {/* <Router createReducer={reducerCreate} getSceneStyle={getSceneStyle} backAndroidHandler={onBackPress} > */}
                        <RouterWithRedux createReducer={reducerCreate} getSceneStyle={getSceneStyle} backAndroidHandler={onBackPress}>
                            <Modal key="modal" hideNavBar transitionConfig={transitionConfig}>
                                <Scene key="splashscreen" component={SplashScreen} hideNavBar hideTabBar initial />
                                <Lightbox key="lightbox">
                                    <Stack key="root">
                                        <Scene key="main" hideNavBar type={ActionConst.RESET} panHandlers={null}>
                                            <Drawer
                                                ref={ref => this._option = ref}
                                                hideNavBar
                                                key="option"
                                                drawerLockMode="locked-open"
                                                drawerPosition="right"
                                                onExit={() => console.log('Drawer closed')}
                                                onEnter={() => console.log('Drawer opened')}
                                                contentComponent={() => <OptionDrawer onCloseDrawer={() => Actions.drawerClose()} />}
                                                drawerWidth={300}>
                                                <Tabs
                                                    key="menu"
                                                    swipeEnabled={false}
                                                    tabBarComponent={ ({navigation}) => <TabBar selectedIndex={navigation.state.index} /> }
                                                    showLabel={false}
                                                    lazy={true}
                                                    tabBarPosition={'bottom'}>
                                                    <Scene key="beranda" title="Beranda" component={Beranda} panHandlers={null} initial type={ActionConst.RESET} navBar={props => <NavBar title={props.title} isImage image={require('./assets/image/logo.png')} isOption={true} onOptionClick={() => Actions.drawerOpen() } isNotification /> } />
                                                    <Scene key="riwayat" title="Riwayat" component={Riwayat} panHandlers={null} type={ActionConst.RESET} navBar={() => <NavBar title="Riwayat" /> } />
                                                    <Scene key="berita" title="Berita" component={Berita} panHandlers={null} type={ActionConst.RESET} navBar={() => <NavBar title="Berita" /> } />
                                                    <Scene key="akun" title="Akun" component={Akun} panHandlers={null} type={ActionConst.RESET} navBar={() => <NavBar title="Akun" /> } />
                                                </Tabs> 
                                            </Drawer>
                                        </Scene>
                                        <Scene key="login" component={Login} title="Login" navBar={() => <NavBar title="Login" isBack /> } hideTabBar />
                                        <Scene key="register" component={Register} title="Register" navBar={() => <NavBar title="Registrasi" isBack /> } />

                                        <Scene key="ticketing" component={Ticketing} title="Tiket" navBar={() => <NavBar title="Tiket" isBack /> } />
                                        <Scene key="pesawat" component={Pesawat} title="Pesawat" navBar={() => <NavBar title="Pesawat" isBack /> } />
                                        <Scene key="hotel" component={Hotel} title="Hotel" navBar={() => <NavBar title="Hotel" isBack /> } />
                                        <Scene key="kereta" component={Kereta} title="Kereta" navBar={() => <NavBar title="Kereta" isBack /> } />
                                        <Scene key="bus" component={Bus} title="Bus" navBar={() => <NavBar title="Bus" isBack /> } />
                                        <Scene key="shuttle" component={Shuttle} title="Shuttle" navBar={() => <NavBar title="Shuttle" isBack /> } />

                                        <Scene key="toko" component={Toko} title="Toko" navBar={() => <NavBar title="Toko" isBack /> } />
                                        <Scene key="produk" component={Produk} title="Produk" navBar={() => <NavBar title="Detail Produk" isBack /> } />
                                        <Scene key="keranjang" component={Keranjang} title="Keranjang" navBar={() => <NavBar title="Keranjang" isBack /> } />
                                        <Scene key="order" component={Order} title="Order" navBar={() => <NavBar title="Order" isBack /> } />

                                        <Scene key="detailBerita" title={this.props.title} component={DetailBerita} navBar={props => <NavBar title={props.title.length > 20 ? `${props.title.slice(0, 20)}...` : props.title} isBack />} />

                                        <Scene key="topup" component={TopUp} title="Top Up" navBar={() => <NavBar title="Top Up" isBack /> } />
                                        <Scene key="pulsa" component={Pulsa} title="Pulsa" navBar={() => <NavBar title="Pulsa" isBack /> } />
                                        <Scene key="paketData" component={PaketData} title="Paket Data" navBar={() => <NavBar title="Paket Data" isBack /> } />
                                        <Scene key="tokenPln" component={ListrikToken} title="Token Listrik" navBar={() => <NavBar title="Token Listrik" isBack /> } />
                                        <Scene key="tagihanListrik" component={ListrikTagihan} title="Tagihan Listrik" navBar={() => <NavBar title="Tagihan Listrik" isBack /> } />

                                        <Scene key="kamera" component={Kamera} title="Kamera" hideNavBar hideTabBar />
                                        <Scene key="galeri" component={Galeri} title="Galeri" hideTabBar />

                                        <Scene key="notification" component={Notifikasi} title="Notifikasi" navBar={() => <NavBar title="Notifikasi" isBack /> } />

                                        <Scene key="serbausaha" component={SerbaUsaha} title="Serba Usaha" navBar={() => <NavBar title="Serba Usaha" isBack /> } />
                                        <Scene key="formSerbausaha" component={FormSerbaUsaha} title="Form Serba Usaha" navBar={props => <NavBar title={props.title} isBack /> } />

                                        <Scene key="simpanpinjam" component={SimpanPinjam} title="Simpan Pinjam" navBar={() => <NavBar title="Simpan Pinjam" isBack /> } />
                                        <Scene key="formSimpanPinjam" component={FormSimpanPinjam} title="Form Simpan Pinjam" navBar={props => <NavBar title={props.title} isBack /> } />

                                        <Scene key="formAkun" component={FormAkun} title="Form Update Profile" navBar={props => <NavBar title="Update Profil" isBack /> } />

                                        <Scene key="detailRiwayat" component={DetailRiwayat} title="Detail Riwayat" navBar={props => <NavBar title={props.title} isBack /> } />

                                        <Scene key="gedung" component={Gedung} title={this.props.title} navBar={props => <NavBar title={props.title} isBack /> } />
                                        <Scene key="formGedung" component={FormGedung} title="" navBar={props => <NavBar title={props.title.length > 20 ? `${props.title.slice(0, 20)}...` : props.title} isBack /> } />
                                        <Scene key="kalender" component={Kalender} title="" navBar={props => <NavBar title="Jadwal Tersedia" isBack /> } />

                                        <Scene key="dashboard" component={Dashboard} title="Dashboard" navBar={() => <NavBar title="Dashboard" isBack /> } />
                                    </Stack>
                                </Lightbox>
                            </Modal>
                        {/* </Router> */}
                        </RouterWithRedux>
                    {/* </PersistGate> */}
                </Provider>
                {/* { !state.network.isConnected && <View style={{ position: 'absolute', top: 0, width: width, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: !state.network.isConnected ? ERROR_COLOR : LIGHT_PRIMARY_COLOR , flexDirection: 'row', zIndex: 100 }}>
                    <Feather name={!state.network.isConnected ? "wifi-off" : "wifi"} size={10} color={WHITE_COLOR} style={{ marginRight: 16 }} />
                    <Text style={{ fontSize: 10, color: WHITE_COLOR }}>{!state.network.isConnected ? "Anda sedang offline" : "Anda kembali online"}</Text>
                </View> } */}
                {/* { this.state.isConnected !== null && !this.state.isConnected && <NetworkIndicator type="offline" /> } */}
                {/* { this.state.showIsBackOnline && <NetworkIndicator type="online" /> } */}
            </View>
        );
    }
}