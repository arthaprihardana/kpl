/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 05:07:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-28 02:16:58
 */
// export const PRIMARY_COLOR = '#35ca7f';
// export const DARK_PRIMARY_COLOR = '#167245';
// export const LIGHT_PRIMARY_COLOR = '#c9fdde'
export const PRIMARY_COLOR = '#009d3b';
export const DARK_PRIMARY_COLOR = '#017b2f';
export const LIGHT_PRIMARY_COLOR = '#00b23e';
export const SECONDARY_COLOR = '#fff6bd';
export const DISABLED_COLOR = '#eeeeee' ;
export const WHITE_COLOR = '#ffffff';
export const TEXT_COLOR = '#504e42';
export const PLACEHOLDER_COLOR = '#cccccc';
export const BORDER_COLOR = '#cccccc';
export const ICON_COLOR = '#999999';
export const ERROR_COLOR = '#f34f49';
export const NAVBAR_COLOR = '#f3f3f3';
export const BACKGROUND_INPUT_FIELD = '#fbfbfb';
export const BADGE_COLOR = '#1e90ff'