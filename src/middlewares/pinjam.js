/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 21:14:10 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 21:23:36
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import {API_PINJAM, CONFIG_API, API_ORDER_PINJAM} from '../constant';
import {getContentFormPinjamSuccess, getContentFormPinjamFailure, postOrderPinjamFailure, postOrderPinjamSuccess} from '../actions';
import {GET_STATIC_FORM_PINJAM, POST_ORDER_PINJAM} from '../actions/types';

const getContentFormPinjamRequest = async () => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_PINJAM, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_PINJAM)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getContentFormPinjamFromServer({ payload }) {
    try {
        const response = yield call(getContentFormPinjamRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getContentFormPinjamSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getContentFormPinjamSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API STATIC FORM PINJAM ==>', error);
        yield put(getContentFormPinjamFailure('Oops! something went wrong!'));
    }
}

export function* getContentFormPinjamSaga() {
    yield takeEvery(GET_STATIC_FORM_PINJAM, getContentFormPinjamFromServer);
}

const postOrderPinjamRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_ORDER_PINJAM, FormData, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_ORDER_PINJAM, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(FormData)
        })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderPinjamFromServer({ payload }) {
    try {
        const response = yield call(postOrderPinjamRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderPinjamSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(postOrderPinjamSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API POST ORDER PINJAM ==>', error);
        yield put(postOrderPinjamFailure('Oops! something went wrong!'));
    }
}

export function* postOrderPinjamSaga() {
    yield takeEvery(POST_ORDER_PINJAM, postOrderPinjamFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getContentFormPinjamSaga),
        fork(postOrderPinjamSaga)
    ])
}