/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-15 19:35:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 05:40:30
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_BANNER_PROMO, CONFIG_API } from "../constant";
import { getBannerPromoSuccess, getBannerPromoFailure } from "../actions";
import { GET_BANNER_PROMO } from "../actions/types";

const getBannerPromoRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_BANNER_PROMO, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_BANNER_PROMO)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getBannerPromoFromServer({ payload }) {
    try {
        const response = yield call(getBannerPromoRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getBannerPromoSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getBannerPromoSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API BANNER ==>', error);
        yield put(getBannerPromoFailure('Oops! something went wrong!'));
    }
}

export function* getBannerPromoSaga() {
    yield takeEvery(GET_BANNER_PROMO, getBannerPromoFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getBannerPromoSaga)        
    ])
}