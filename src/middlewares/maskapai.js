/**
 * @Author: Artha Prihardana
 * @Date: 2019-10-10 23:46:30
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-10 23:52:03
 */
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_MASTER_MASKAPAI, CONFIG_API } from "../constant";
import { getMaskapaiSuccess, getMaskapaiFailure } from "../actions";
import { GET_MASTER_MASKAPAI } from "../actions/types";

const getMaskapaiRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_MASTER_MASKAPAI, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_MASTER_MASKAPAI)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getMaskapaiFromServer({ payload }) {
    try {
        const response = yield call(getMaskapaiRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getMaskapaiSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getMaskapaiSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API MASKAPAI ==>', error);
        yield put(getMaskapaiFailure('Oops! something went wrong!'));
    }
}

export function* getMaskapaiSaga() {
    yield takeLatest(GET_MASTER_MASKAPAI, getMaskapaiFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMaskapaiSaga)        
    ])
}