/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:29:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 05:40:42
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_MASTER_KOTA, CONFIG_API } from "../constant";
import { getKotaSuccess, getKotaFailure } from "../actions";
import { GET_MASTER_KOTA } from "../actions/types";

const getKotaRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_MASTER_KOTA, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_MASTER_KOTA)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getKotaFromServer({ payload }) {
    try {
        const response = yield call(getKotaRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getKotaSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getKotaSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API KOTA ==>', error);
        yield put(getKotaFailure('Oops! something went wrong!'));
    }
}

export function* getKotaSaga() {
    yield takeEvery(GET_MASTER_KOTA, getKotaFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getKotaSaga)        
    ])
}