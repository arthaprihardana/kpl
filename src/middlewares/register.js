/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 06:27:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 05:41:08
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_REGISTER, CONFIG_API } from "../constant";
import { postRegisterSuccess, postRegisterFailure } from "../actions";
import { POST_REGISTER } from "../actions/types";

const postRegisterRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_REGISTER, FormData, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_REGISTER, {
                method: 'POST',
                body: JSON.stringify(FormData),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postRegisterToServer({ payload }) {
    try {
        const response = yield call(postRegisterRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postRegisterSuccess(response.data))
        } else {
            // Using Fetch
            yield put(postRegisterSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API REGISTER ==>', error);
        yield put(postRegisterFailure('Oops! something went wrong!'));
    }
}

export function* postRegisterSaga() {
    yield takeEvery(POST_REGISTER, postRegisterToServer);
}

export default function* rootSaga() {
    yield all([
        fork(postRegisterSaga)        
    ])
}