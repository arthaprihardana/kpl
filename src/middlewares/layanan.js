/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:29:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-28 22:49:17
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_LAYANAN, CONFIG_API } from "../constant";
import { getLayananSuccess, getLayananFailure } from "../actions";
import { GET_ALL_LAYANAN } from "../actions/types";

const getLayananRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_LAYANAN, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_LAYANAN)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getLayananFromServer({ payload }) {
    try {
        const response = yield call(getLayananRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getLayananSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getLayananSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API LAYANAN ==>', error);
        yield put(getLayananFailure('Oops! something went wrong!'));
    }
}

export function* getLayananSaga() {
    yield takeEvery(GET_ALL_LAYANAN, getLayananFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getLayananSaga)        
    ])
}