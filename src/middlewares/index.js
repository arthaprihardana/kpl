/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 06:11:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-10 23:47:45
 */
import { all } from 'redux-saga/effects';

import LoginMiddleware from './login';
import RegisterMiddleware from './register';
import AgamaMiddleware from './agama';
import LayananMiddleware from './layanan';
import BannerPromoMiddleware from './banner';
import BandaraMiddleware from './bandara';
import StasiunMiddleware from './stasiun';
import KotaMiddleware from './kota';
import TiketMiddleware from './tiket';
import BeritaMiddleware from './berita';
import TokoMiddleware from './toko';
import TopUpMiddleware from './topup';
import AkunMiddleware from './akun';
import SerbaUsahaMiddleware from './serbausaha';
import RiwayatMiddleware from './riwayat';
import SimpanMiddleware from './simpan';
import PinjamMiddleware from './pinjam';
import GedungMiddleware from './gedung';
import NotifikasiMiddleware from './notifikasi';
import ForceUpdateMiddleware from './forceUpdate';
import MaskapaiMiddleware from './maskapai';

export default function* rootSaga(getState) {
    yield all([
        LoginMiddleware(),
        RegisterMiddleware(),
        AgamaMiddleware(),
        LayananMiddleware(),
        BannerPromoMiddleware(),
        BandaraMiddleware(),
        KotaMiddleware(),
        TiketMiddleware(),
        StasiunMiddleware(),
        BeritaMiddleware(),
        TokoMiddleware(),
        TopUpMiddleware(),
        AkunMiddleware(),
        SerbaUsahaMiddleware(),
        RiwayatMiddleware(),
        SimpanMiddleware(),
        PinjamMiddleware(),
        GedungMiddleware(),
        NotifikasiMiddleware(),
        ForceUpdateMiddleware(),
        MaskapaiMiddleware()
    ]);
}