/**
 * @Author: Artha Prihardana
 * @Date: 2019-10-03 18:18:49
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-03 19:13:30
 */
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_FORCE_UPDATE, CONFIG_API } from '../constant';
import { GET_FORCE_UPDATE } from '../actions/types';
import { getForceUpdateSuccess, getForceUpdateFailure } from '../actions';

const getForceUpdateRequest = async FormData => {
  if(CONFIG_API === "A") {
      // Using Axios
      return await Axios.get(API_FORCE_UPDATE, {
          headers: {
              'Content-Type': 'application/json'
          }
      })
      .then(response => response)
      .catch(error => error);
  } else {
      // Using Fetch
      return await fetch(API_FORCE_UPDATE)
          .then((response) => response.json())
          .then((responseJson) => responseJson)
          .catch((error) => error);
  }
}

function* getForceUpdateFromServer() {
  try {
      const response = yield call(getForceUpdateRequest);
      if(CONFIG_API === "A") {
          // Using Axios
          yield put(getForceUpdateSuccess(response.data.data[0].apps_version))
      } else {
          // Using Fetch
          yield put(getForceUpdateSuccess(response.data[0].apps_version))
      }
  } catch (error) {
      Sentry.captureMessage('API FORCE UPDATE ==>', error);
      yield put(getForceUpdateFailure('Oops! something went wrong!'));
  }
}

export function* getForceUpdateSaga() {
  yield takeLatest(GET_FORCE_UPDATE, getForceUpdateFromServer);
}

export default function* rootSaga() {
  yield all([
      fork(getForceUpdateSaga)        
  ])
}