/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:29:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-25 02:02:13
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_BERITA, CONFIG_API, API_DETAIL_BERITA } from "../constant";
import { getBeritaSuccess, getBeritaFailure, getDetailBeritaSuccess, getDetailBeritaFailure } from "../actions";
import { GET_BERITA, GET_DETAIL_BERITA } from "../actions/types";
import { objectToQueryString } from "../helpers";

const getBeritaRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_BERITA(params), {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_BERITA(params))
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getBeritaFromServer({ payload }) {
    try {
        const response = yield call(getBeritaRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getBeritaSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getBeritaSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API BERITA ==>', error);
        yield put(getBeritaFailure('Oops! something went wrong!'));
    }
}

export function* getBeritaSaga() {
    yield takeEvery(GET_BERITA, getBeritaFromServer);
}

const getDetailBeritaRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_DETAIL_BERITA}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_DETAIL_BERITA}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getDetailBeritaFromServer({ payload }) {
    try {
        const response = yield call(getDetailBeritaRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getDetailBeritaSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getDetailBeritaSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API DETAIL BERITA ==>', error);
        yield put(getDetailBeritaFailure('Oops! something went wrong!'));
    }
}

export function* getDetailBeritaSaga() {
    yield takeEvery(GET_DETAIL_BERITA, getDetailBeritaFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getBeritaSaga),
        fork(getDetailBeritaSaga)      
    ])
}