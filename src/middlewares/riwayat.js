/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 13:18:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 13:22:34
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import {API_RIWAYAT, CONFIG_API, API_DETAIL_RIWAYAT} from '../constant';
import {getRiwayatSuccess, getRiwayatFailure, getDetailRiwayatSuccess, getDetailRiwayatFailure} from '../actions/riwayat';
import {GET_RIWAYAT, GET_DETAIL_RIWAYAT} from '../actions/types';
import {objectToQueryString} from '../helpers/index';

const getRiwayatRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_RIWAYAT}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_RIWAYAT}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getRiwayatFromServer({ payload }) {
    try {
        const response = yield call(getRiwayatRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getRiwayatSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getRiwayatSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API RIWAYAT ==>', error);
        yield put(getRiwayatFailure('Oops! something went wrong!'));
    }
}

export function* getRiwayatSaga() {
    yield takeEvery(GET_RIWAYAT, getRiwayatFromServer);
}

const getDetailRiwayatRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_DETAIL_RIWAYAT}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_DETAIL_RIWAYAT}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getDetailRiwayatFromServer({ payload }) {
    try {
        const response = yield call(getDetailRiwayatRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getDetailRiwayatSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getDetailRiwayatSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API RIWAYAT ==>', error);
        yield put(getDetailRiwayatFailure('Oops! something went wrong!'));
    }
}

export function* getDetailRiwayatSaga() {
    yield takeEvery(GET_DETAIL_RIWAYAT, getDetailRiwayatFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getRiwayatSaga),
        fork(getDetailRiwayatSaga)
    ])
}