/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:29:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 05:40:25
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_MASTER_BANDARA, CONFIG_API } from "../constant";
import { getBandaraSuccess, getBandaraFailure } from "../actions";
import { GET_MASTER_BANDARA } from "../actions/types";

const getBandaraRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_MASTER_BANDARA, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_MASTER_BANDARA)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getBandaraFromServer({ payload }) {
    try {
        const response = yield call(getBandaraRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getBandaraSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getBandaraSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API BANDARA ==>', error);
        yield put(getBandaraFailure('Oops! something went wrong!'));
    }
}

export function* getBandaraSaga() {
    yield takeEvery(GET_MASTER_BANDARA, getBandaraFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getBandaraSaga)        
    ])
}