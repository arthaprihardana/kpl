/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:29:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 05:41:18
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_MASTER_STASIUN, CONFIG_API } from "../constant";
import { getStasiunSuccess, getStasiunFailure } from "../actions";
import { GET_MASTER_STASIUN } from "../actions/types";

const getStasiunRequest = async () => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_MASTER_STASIUN, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_MASTER_STASIUN)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getStasiunFromServer() {
    try {
        const response = yield call(getStasiunRequest);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getStasiunSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getStasiunSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API STASIUN ==>', error);
        yield put(getStasiunFailure('Oops! something went wrong!'));
    }
}

export function* getStasiunSaga() {
    yield takeEvery(GET_MASTER_STASIUN, getStasiunFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getStasiunSaga)        
    ])
}