/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 21:14:10 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 21:21:04
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import {API_SIMPAN, CONFIG_API, API_ORDER_SIMPAN} from '../constant';
import {getContentFormSimpanSuccess, getContentFormSimpanFailure, postOrderSimpanFailure, postOrderSimpanSuccess} from '../actions';
import {GET_STATIC_FORM_SIMPAN, POST_ORDER_SIMPAN} from '../actions/types';

const getContentFormSimpanRequest = async () => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_SIMPAN, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_SIMPAN)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getContentFormSimpanFromServer({ payload }) {
    try {
        const response = yield call(getContentFormSimpanRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getContentFormSimpanSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getContentFormSimpanSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API STATIC FORM SIMPAN ==>', error);
        yield put(getContentFormSimpanFailure('Oops! something went wrong!'));
    }
}

export function* getContentFormSimpanSaga() {
    yield takeEvery(GET_STATIC_FORM_SIMPAN, getContentFormSimpanFromServer);
}

const postOrderSimpanRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_ORDER_SIMPAN, FormData, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_ORDER_SIMPAN, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(FormData)
        })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderSimpanFromServer({ payload }) {
    try {
        const response = yield call(postOrderSimpanRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderSimpanSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(postOrderSimpanSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API POST ORDER SIMPAN ==>', error);
        yield put(postOrderSimpanFailure('Oops! something went wrong!'));
    }
}

export function* postOrderSimpanSaga() {
    yield takeEvery(POST_ORDER_SIMPAN, postOrderSimpanFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getContentFormSimpanSaga),
        fork(postOrderSimpanSaga)
    ])
}