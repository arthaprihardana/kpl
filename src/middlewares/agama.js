/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:29:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 05:39:18
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_AGAMA, CONFIG_API } from "../constant";
import { GET_AGAMA } from "../actions/types";
import { getAgamaSuccess, getAgamaFailure } from "../actions";

const getAgamaRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_AGAMA, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_AGAMA)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getAgamaFromServer({ payload }) {
    try {
        const response = yield call(getAgamaRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getAgamaSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getAgamaSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API AGAMA ==>', error);
        yield put(getAgamaFailure('Oops! something went wrong!'));
    }
}

export function* getAgamaSaga() {
    yield takeEvery(GET_AGAMA, getAgamaFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getAgamaSaga)        
    ])
}