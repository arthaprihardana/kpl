/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-17 09:52:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-22 11:56:03
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_PESAWAT, API_HOTEL, API_KERETA, API_BUS, API_SHUTTLE, CONFIG_API } from "../constant";
import { postOrderPesawatSuccess, postOrderPesawatFailure, postOrderHotelSuccess, postOrderHotelFailure, postOrderKeretaSuccess, postOrderKeretaFailure, postOrderBusSuccess, postOrderBusFailure, postOrderShuttleSuccess, postOrderShuttleFailure } from "../actions";
import { POST_ORDER_PESAWAT, POST_ORDER_HOTEL, POST_ORDER_KERETA, POST_ORDER_BUS, POST_ORDER_SHUTTLE } from "../actions/types";

// PESAWAT
const postOrderPesawatRequest = async form => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_PESAWAT, form, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_PESAWAT, {
                method: 'POST',
                body: JSON.stringify(form),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderPesawatToServer({ payload }) {
    try {
        const response = yield call(postOrderPesawatRequest, payload);
        console.log('response ==>', response);
        
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderPesawatSuccess(response.data))
        } else {
            // Using Fetch
            yield put(postOrderPesawatSuccess(response))
        }
    } catch (error) {
        Sentry.captureMessage('API PESAWAT ==>', error);
        yield put(postOrderPesawatFailure('Oops! something went wrong!'));
    }
}

export function* postOrderPesawatSaga() {
    yield takeEvery(POST_ORDER_PESAWAT, postOrderPesawatToServer);
}

// HOTEL
const postOrderHotelRequest = async form => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_HOTEL, form, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_HOTEL, {
                method: 'POST',
                body: JSON.stringify(form),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderHotelToServer({ payload }) {
    try {
        const response = yield call(postOrderHotelRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderHotelSuccess(response.data))
        } else {
            // Using Fetch
            yield put(postOrderHotelSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API HOTEL ==>', error);
        yield put(postOrderHotelFailure('Oops! something went wrong!'));
    }
}

export function* postOrderHotelSaga() {
    yield takeEvery(POST_ORDER_HOTEL, postOrderHotelToServer);
}

// KERETA
const postOrderKeretaRequest = async form => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_KERETA, form, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_KERETA, {
                method: 'POST',
                body: JSON.stringify(form),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderKeretaToServer({ payload }) {
    try {
        const response = yield call(postOrderKeretaRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderKeretaSuccess(response.data))
        } else {
            // Using Fetch
            yield put(postOrderKeretaSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API KERETA ==>', error);
        yield put(postOrderKeretaFailure('Oops! something went wrong!'));
    }
}

export function* postOrderKeretaSaga() {
    yield takeEvery(POST_ORDER_KERETA, postOrderKeretaToServer);
}

// BUS
const postOrderBusRequest = async form => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_BUS, form, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_BUS, {
                method: 'POST',
                body: JSON.stringify(form),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderBusToServer({ payload }) {
    try {
        const response = yield call(postOrderBusRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderBusSuccess(response.data))
        } else {
            // Using Fetch
            yield put(postOrderBusSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API BUS ==>', error);
        yield put(postOrderBusFailure('Oops! something went wrong!'));
    }
}

export function* postOrderBusSaga() {
    yield takeEvery(POST_ORDER_BUS, postOrderBusToServer);
}

// SHUTTLE
const postOrderShuttleRequest = async form => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_SHUTTLE, form, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_SHUTTLE, {
                method: 'POST',
                body: JSON.stringify(form),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderShuttleToServer({ payload }) {
    try {
        const response = yield call(postOrderShuttleRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderShuttleSuccess(response.data))
        } else {
            // Using Fetch
            yield put(postOrderShuttleSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API SHUTTLE ==>', error);
        yield put(postOrderShuttleFailure('Oops! something went wrong!'));
    }
}

export function* postOrderShuttleSaga() {
    yield takeEvery(POST_ORDER_SHUTTLE, postOrderShuttleToServer);
}

export default function* rootSaga() {
    yield all([
        fork(postOrderPesawatSaga),
        fork(postOrderHotelSaga),
        fork(postOrderKeretaSaga),
        fork(postOrderBusSaga),
        fork(postOrderShuttleSaga),
    ])
}