/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-25 22:17:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 21:06:47
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_ORDER_PULSA, API_ORDER_PAKET_DATA, CONFIG_API , API_LISTRIK_TOKEN, API_LISTRIK_TAGIHAN} from "../constant";
import { postOrderPulsaSuccess, postOrderPulsaFailure, postOrderPaketDataSuccess, postOrderPaketDataFailure, postOrderListrikTokenSuccess, postOrderListrikTokenFailure, postOrderListrikTagihanSuccess, postOrderListrikTagihanFailure } from "../actions";
import { POST_ORDER_PULSA, POST_ORDER_PAKET_DATA, POST_ORDER_LISTRIK_TOKEN, POST_ORDER_LISTRIK_TAGIHAN} from "../actions/types";

const postOrderPulsaRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(`${API_ORDER_PULSA}`, params, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_ORDER_PULSA}`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderPulsaToServer({ payload }) {
    try {
        const response = yield call(postOrderPulsaRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderPulsaSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(postOrderPulsaSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API ORDER PULSA ==>', error);
        yield put(postOrderPulsaFailure('Oops! something went wrong!'));
    }
}

export function* postOrderPulsaSaga() {
    yield takeEvery(POST_ORDER_PULSA, postOrderPulsaToServer);
}

const postOrderPaketDataRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(`${API_ORDER_PAKET_DATA}`, params, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_ORDER_PAKET_DATA}`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderPaketDataToServer({ payload }) {
    try {
        const response = yield call(postOrderPaketDataRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderPaketDataSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(postOrderPaketDataSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API ORDER PAKET DATA ==>', error);
        yield put(postOrderPaketDataFailure('Oops! something went wrong!'));
    }
}

export function* postOrderPaketDataSaga() {
    yield takeEvery(POST_ORDER_PAKET_DATA, postOrderPaketDataToServer);
}

const postOrderListrikTokenRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(`${API_LISTRIK_TOKEN}`, params, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_LISTRIK_TOKEN}`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderListrikTokenToServer({ payload }) {
    try {
        const response = yield call(postOrderListrikTokenRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderListrikTokenSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(postOrderListrikTokenSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API ORDER LISTRIK TOKEN ==>', error);
        yield put(postOrderListrikTokenFailure('Oops! something went wrong!'));
    }
}

export function* postOrderListrikTokenSaga() {
    yield takeEvery(POST_ORDER_LISTRIK_TOKEN, postOrderListrikTokenToServer);
}

const postOrderListrikTagihanRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(`${API_LISTRIK_TAGIHAN}`, params, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_LISTRIK_TAGIHAN}`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderListrikTagihanToServer({ payload }) {
    try {
        const response = yield call(postOrderListrikTagihanRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderListrikTagihanSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(postOrderListrikTagihanSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API ORDER LISTRIK TAGIHAN ==>', error);
        yield put(postOrderListrikTagihanFailure('Oops! something went wrong!'));
    }
}

export function* postOrderListrikTagihanSaga() {
    yield takeEvery(POST_ORDER_LISTRIK_TAGIHAN, postOrderListrikTagihanToServer);
}

export default function* rootSaga() {
    yield all([
        fork(postOrderPulsaSaga),
        fork(postOrderPaketDataSaga),
        fork(postOrderListrikTokenSaga),
        fork(postOrderListrikTagihanSaga)
    ])
}