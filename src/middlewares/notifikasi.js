/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-31 21:02:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-31 21:06:00
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { CONFIG_API, API_NOTIFIKASI } from "../constant";
import { getNotifikasiSuccess, getNotifikasiFailure } from "../actions";
import { GET_NOTIFIKASI } from "../actions/types";

const getNotifikasiRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_NOTIFIKASI(params), {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_NOTIFIKASI(params))
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getNotifikasiFromServer({ payload }) {
    try {
        const response = yield call(getNotifikasiRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getNotifikasiSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getNotifikasiSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API NOTIFIKASI ==>', error);
        yield put(getNotifikasiFailure('Oops! something went wrong!'));
    }
}

export function* getNotifikasiSaga() {
    yield takeEvery(GET_NOTIFIKASI, getNotifikasiFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getNotifikasiSaga)
    ])
}