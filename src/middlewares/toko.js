/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:29:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-28 02:31:49
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { CONFIG_API, API_TOKO, API_DETAIL_PRODUK, API_RELATED_PRODUK, API_ORDER_TOKO, API_TOKO_SEARCHING, API_MASTER_KATEGORI_PRODUK, API_PRODUK_BY_KATEGORI } from "../constant";
import { GET_TOKO, GET_DETAIL_PRODUK, GET_RELATED_PRODUK, POST_ORDER_TOKO, GET_SEARCH_TOKO, GET_MASTER_KATEGORI_PRODUK, GET_PRODUK_BY_KATEGORI } from "../actions/types";
import { getTokoSuccess, getTokoFailure, getDetailProdukSuccess, getDetailProdukFailure, getRelatedProdukSuccess, getRelatedProdukFailure, postOrderTokoSuccess, postOrderTokoFailure, getSearchTokoSuccess, getSearchTokoFailure, getMasterKategoriProdukSuccess, getMasterKategoriProdukFailure, getProductByCategorySuccess, getProductByCategoryFailure } from "../actions";
import { objectToQueryString } from "../helpers";

const getTokoRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_TOKO(params), {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_TOKO(params))
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getTokoFromServer({ payload }) {
    try {
        const response = yield call(getTokoRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getTokoSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getTokoSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API TOKO ==>', error);
        yield put(getTokoFailure('Oops! something went wrong!'));
    }
}

export function* getTokoSaga() {
    yield takeEvery(GET_TOKO, getTokoFromServer);
}

const getSearchTokoRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_TOKO_SEARCHING}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_TOKO_SEARCHING}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getSearchTokoFromServer({ payload }) {
    try {
        const response = yield call(getSearchTokoRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getSearchTokoSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getSearchTokoSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API SEARCH TOKO ==>', error);
        yield put(getSearchTokoFailure('Oops! something went wrong!'));
    }
}

export function* getSearchTokoSaga() {
    yield takeEvery(GET_SEARCH_TOKO, getSearchTokoFromServer);
}

const getDetailProdukRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_DETAIL_PRODUK}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_DETAIL_PRODUK}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getDetailProdukFromServer({ payload }) {
    try {
        const response = yield call(getDetailProdukRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getDetailProdukSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getDetailProdukSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API DETAIL PRODUK ==>', error);
        yield put(getDetailProdukFailure('Oops! something went wrong!'));
    }
}

export function* getDetailProdukSaga() {
    yield takeEvery(GET_DETAIL_PRODUK, getDetailProdukFromServer);
}

const getRelatedProdukRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_RELATED_PRODUK}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_RELATED_PRODUK}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getRelatedProdukFromServer({ payload }) {
    try {
        const response = yield call(getRelatedProdukRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getRelatedProdukSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getRelatedProdukSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API RELATED PRODUK ==>', error);
        yield put(getRelatedProdukFailure('Oops! something went wrong!'));
    }
}

export function* getRelatedProdukSaga() {
    yield takeEvery(GET_RELATED_PRODUK, getRelatedProdukFromServer);
}

const postOrderTokoRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(`${API_ORDER_TOKO}`, params, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_ORDER_TOKO}`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderTokoToServer({ payload }) {
    try {
        const response = yield call(postOrderTokoRequest, payload);
        console.log('response ==>', response);
        
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderTokoSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(postOrderTokoSuccess(response))
        }
    } catch (error) {
        Sentry.captureMessage('API ORDER TOKO ==>', error);
        yield put(postOrderTokoFailure('Oops! something went wrong!'));
    }
}

export function* postOrderTokoSaga() {
    yield takeEvery(POST_ORDER_TOKO, postOrderTokoToServer);
}

const getMasterKategoriProdukRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_MASTER_KATEGORI_PRODUK, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_MASTER_KATEGORI_PRODUK)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getMasterKategoriProdukFromServer({ payload }) {
    try {
        const response = yield call(getMasterKategoriProdukRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getMasterKategoriProdukSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getMasterKategoriProdukSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API MASTER KATEGORI PRODUK ==>', error);
        yield put(getMasterKategoriProdukFailure('Oops! something went wrong!'));
    }
}

export function* getMasterKategoriProdukSaga() {
    yield takeEvery(GET_MASTER_KATEGORI_PRODUK, getMasterKategoriProdukFromServer);
}

const getProductByCategoryRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_PRODUK_BY_KATEGORI}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_PRODUK_BY_KATEGORI}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getProductByCategoryFromServer({ payload }) {
    try {
        const response = yield call(getProductByCategoryRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getProductByCategorySuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getProductByCategorySuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API PRODUK BY KATEGORI ==>', error);
        yield put(getProductByCategoryFailure('Oops! something went wrong!'));
    }
}

export function* getProductByCategorySaga() {
    yield takeEvery(GET_PRODUK_BY_KATEGORI, getProductByCategoryFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getTokoSaga),
        fork(getSearchTokoSaga),
        fork(getDetailProdukSaga),
        fork(getRelatedProdukSaga),
        fork(postOrderTokoSaga),
        fork(getMasterKategoriProdukSaga),
        fork(getProductByCategorySaga)
    ])
}