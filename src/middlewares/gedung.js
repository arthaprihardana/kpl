/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-07 23:27:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-18 22:27:50
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import {API_GEDUNG_LIST, CONFIG_API, API_GEDUNG_DETAIL, API_POST_ORDER_GEDUNG, API_BOOKING_DATE, API_DETAIL_REKANAN} from '../constant';
import {getGedungListSuccess, getGedungListFailure, getGedungDetailSuccess, getGedungDetailFailure, postOrderGedungSuccess, postOrderGedungFailure, getBookingDateSuccess, getBookingDateFailure, getRekananSuccess, getRekananFailure} from '../actions';
import {GET_GEDUNG_LIST, GET_GEDUNG_DETAIL, POST_ORDER_GEDUNG, GET_BOOKING_DATE, GET_REKANAN} from '../actions/types';
import { objectToQueryString } from '../helpers';

const getGedungListRequest = async () => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_GEDUNG_LIST, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_GEDUNG_LIST)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getGedungListFromServer({ payload }) {
    try {
        const response = yield call(getGedungListRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getGedungListSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getGedungListSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API GEDUNG LIST ==>', error);
        yield put(getGedungListFailure('Oops! something went wrong!'));
    }
}

export function* getGedungListSaga() {
    yield takeEvery(GET_GEDUNG_LIST, getGedungListFromServer);
}

const getGedungDetailRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_GEDUNG_DETAIL}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_GEDUNG_DETAIL}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getGedungDetailFromServer({ payload }) {
    try {
        const response = yield call(getGedungDetailRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getGedungDetailSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getGedungDetailSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API GEDUNG DETAIL ==>', error);
        yield put(getGedungDetailFailure('Oops! something went wrong!'));
    }
}

export function* getGedungDetailSaga() {
    yield takeEvery(GET_GEDUNG_DETAIL, getGedungDetailFromServer);
}

const postOrderGedungRequest = async form => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(`${API_POST_ORDER_GEDUNG}`, form, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_POST_ORDER_GEDUNG}`, {
                method: 'POST',
                body: JSON.stringify(form),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderGedungFromServer({ payload }) {
    try {
        const response = yield call(postOrderGedungRequest, payload);
        console.log('response ==>', response);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderGedungSuccess(response.data.status))
        } else {
            // Using Fetch
            yield put(postOrderGedungSuccess(response.status))
        }
    } catch (error) {
        Sentry.captureMessage('API ORDER GEDUNG ==>', error);
        yield put(postOrderGedungFailure('Oops! something went wrong!'));
    }
}

export function* postOrderGedungSaga() {
    yield takeEvery(POST_ORDER_GEDUNG, postOrderGedungFromServer);
}

const getBookingDateRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_BOOKING_DATE}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_BOOKING_DATE}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getBookingDateFromServer({ payload }) {
    try {
        const response = yield call(getBookingDateRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getBookingDateSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getBookingDateSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API BOOKING DATE ==>', error);
        yield put(getBookingDateFailure('Oops! something went wrong!'));
    }
}

export function* getBookingDateSaga() {
    yield takeEvery(GET_BOOKING_DATE, getBookingDateFromServer);
}

const getRekananRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_DETAIL_REKANAN(params), {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_DETAIL_REKANAN(params))
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getRekananFromServer({ payload }) {
    try {
        const response = yield call(getRekananRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getRekananSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getRekananSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API REKANAN ==>', error);
        yield put(getRekananFailure('Oops! something went wrong!'));
    }
}

export function* getRekananSaga() {
    yield takeEvery(GET_REKANAN, getRekananFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getGedungListSaga),
        fork(getGedungDetailSaga),
        fork(postOrderGedungSaga),
        fork(getBookingDateSaga),
        fork(getRekananSaga)
    ])
}
