/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-19 22:27:12 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 21:08:06
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import {API_LIST_SERBA_USAHA, CONFIG_API, API_DETAIL_SERBA_USAHA, API_ORDER_SERBA_USAHA} from '../constant';
import {getListSerbaUsahaSuccess, getListSerbaUsahaFailure, getDetailSerbaUsahaSuccess, getDetailSerbaUsahaFailure, postOrderSerbaUsahaSuccess, postOrderSerbaUsahaFailure} from '../actions';
import {GET_LIST_SERBA_USAHA, GET_DETAIL_SERBA_USAHA, POST_SERBA_USAHA} from '../actions/types';
import {objectToQueryString} from '../helpers/index';

const getListSerbaUsahaRequest = async () => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_LIST_SERBA_USAHA, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_LIST_SERBA_USAHA)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getListSerbaUsahaFromServer({ payload }) {
    try {
        const response = yield call(getListSerbaUsahaRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getListSerbaUsahaSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getListSerbaUsahaSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API SERBA USAHA ==>', error);
        yield put(getListSerbaUsahaFailure('Oops! something went wrong!'));
    }
}

export function* getListSerbaUsahaSaga() {
    yield takeEvery(GET_LIST_SERBA_USAHA, getListSerbaUsahaFromServer);
}

const getDetailSerbaUsahaRequest = async params => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_DETAIL_SERBA_USAHA}?${objectToQueryString(params)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_DETAIL_SERBA_USAHA}?${objectToQueryString(params)}`)
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getDetailSerbaUsahaFromServer({ payload }) {
    try {
        const response = yield call(getDetailSerbaUsahaRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getDetailSerbaUsahaSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getDetailSerbaUsahaSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API DETAIL SERBA USAHA ==>', error);
        yield put(getDetailSerbaUsahaFailure('Oops! something went wrong!'));
    }
}

export function* getDetailSerbaUsahaSaga() {
    yield takeEvery(GET_DETAIL_SERBA_USAHA, getDetailSerbaUsahaFromServer);
}

const postOrderSerbaUsahaRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_ORDER_SERBA_USAHA, FormData, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_ORDER_SERBA_USAHA, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(FormData)
        })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postOrderSerbaUsahaFromServer({ payload }) {
    try {
        const response = yield call(postOrderSerbaUsahaRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postOrderSerbaUsahaSuccess(response.data))
        } else {
            // Using Fetch
            yield put(postOrderSerbaUsahaSuccess(response))
        }
    } catch (error) {
        Sentry.captureMessage('API POST SERBA USAHA ==>', error);
        yield put(postOrderSerbaUsahaFailure('Oops! something went wrong!'));
    }
}

export function* postOrderSerbaUsahaSaga() {
    yield takeEvery(POST_SERBA_USAHA, postOrderSerbaUsahaFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getListSerbaUsahaSaga),
        fork(getDetailSerbaUsahaSaga),
        fork(postOrderSerbaUsahaSaga)
    ])
}