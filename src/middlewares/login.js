/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 06:27:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 05:41:00
 */
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import { API_LOGIN, CONFIG_API } from "../constant";
import { postLoginSuccess, postLoginFailure } from "../actions";
import { POST_LOGIN } from "../actions/types";

const postLoginRequest = async FormData => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_LOGIN, FormData, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_LOGIN, {
                method: 'POST',
                body: JSON.stringify(FormData),
                headers: {
                    'Content-Type': 'application/json'
                },
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postLoginToServer({ payload }) {
    try {
        const response = yield call(postLoginRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            if(response.data.status) {
                yield put(postLoginSuccess(response.data.data))   
            } else {
                yield put(postLoginFailure(response.data.message));
            }
        } else {
            // Using Fetch
            if(response.status) {
                yield put(postLoginSuccess(response.data))   
            } else {
                yield put(postLoginFailure(response.message));
            }
        }
    } catch (error) {
        Sentry.captureMessage('API LOGIN ==>', error);
        yield put(postLoginFailure('Oops! something went wrong!'));
    }
}

export function* postLoginSaga() {
    yield takeEvery(POST_LOGIN, postLoginToServer);
}

export default function* rootSaga() {
    yield all([
        fork(postLoginSaga)        
    ])
}