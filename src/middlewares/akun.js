/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-18 22:36:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-26 20:52:24
 */
import { takeEvery, all, call, put, fork, takeLatest } from 'redux-saga/effects';
import Axios from 'axios';
import { Sentry } from 'react-native-sentry';
import {CONFIG_API, API_UPDATE_FOTO_PROFIL, API_CHECK_USERNAME, API_UPDATE_AKUN, API_SALDO} from '../constant';
import {postFotoProfilSuccess, postFotoProfilFailure, getCheckUsernameSuccess, getCheckUsernameFailure, updateUserSuccess, updateUserFailure, getSaldoSuccess, getSaldoFailure} from '../actions';
import {POST_FOTO_PROFIL, GET_CHECK_USERNAME, PUT_AKUN, GET_SALDO} from '../actions/types';
import {objectToQueryString} from '../helpers/index';

const postFotoProfilRequest = async form => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_UPDATE_FOTO_PROFIL, form, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_UPDATE_FOTO_PROFIL, {
                method: 'POST',
                body: JSON.stringify(form),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* postFotoProfilToServer({ payload }) {
    try {
        const response = yield call(postFotoProfilRequest, payload);
        console.log('response ==>', response);
        
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(postFotoProfilSuccess(response.data))
        } else {
            // Using Fetch
            yield put(postFotoProfilSuccess(response))
        }
    } catch (error) {
        Sentry.captureMessage('API FOTO PROFIL ==>', error);
        yield put(postFotoProfilFailure('Oops! something went wrong!'));
    }
}

export function* postFotoProfilSaga() {
    yield takeEvery(POST_FOTO_PROFIL, postFotoProfilToServer);
}

const getCheckUsernameRequest = async form => {
    console.log(`${API_CHECK_USERNAME}?${objectToQueryString(form)}`)
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(`${API_CHECK_USERNAME}?${objectToQueryString(form)}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(`${API_CHECK_USERNAME}?${objectToQueryString(form)}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getCheckUsernameToServer({ payload }) {
    try {
        const response = yield call(getCheckUsernameRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getCheckUsernameSuccess(response.data))
        } else {
            // Using Fetch
            yield put(getCheckUsernameSuccess(response))
        }
    } catch (error) {
        Sentry.captureMessage('API GET USERNAME ==>', error);
        yield put(getCheckUsernameFailure('Oops! something went wrong!'));
    }
}

export function* getCheckUsernameSaga() {
    yield takeEvery(GET_CHECK_USERNAME, getCheckUsernameToServer);
}

const putUpdateAkunRequest = async form => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.post(API_UPDATE_AKUN, form, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_UPDATE_AKUN, {
                method: 'POST',
                body: JSON.stringify(form),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* putUpdateAkunToServer({ payload }) {
    try {
        const response = yield call(putUpdateAkunRequest, payload);
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(updateUserSuccess(response.data))
        } else {
            // Using Fetch
            yield put(updateUserSuccess(response))
        }
    } catch (error) {
        Sentry.captureMessage('API UPDATE PROFIL ==>', error);
        yield put(updateUserFailure('Oops! something went wrong!'));
    }
}

export function* putUpdateAkunSaga() {
    yield takeLatest(PUT_AKUN, putUpdateAkunToServer);
}

const getSaldoRequest = async form => {
    if(CONFIG_API === "A") {
        // Using Axios
        return await Axios.get(API_SALDO(form), form, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response)
        .catch(error => error);
    } else {
        // Using Fetch
        return await fetch(API_SALDO(form), {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => response.json())
            .then((responseJson) => responseJson)
            .catch((error) => error);
    }
}

function* getSaldoToServer({ payload }) {
    try {
        const response = yield call(getSaldoRequest, payload);
        
        if(CONFIG_API === "A") {
            // Using Axios
            yield put(getSaldoSuccess(response.data.data))
        } else {
            // Using Fetch
            yield put(getSaldoSuccess(response.data))
        }
    } catch (error) {
        Sentry.captureMessage('API SALDO ==>', error);
        yield put(getSaldoFailure('Oops! something went wrong!'));
    }
}

export function* getSaldoSaga() {
    yield takeLatest(GET_SALDO, getSaldoToServer);
}

export default function* rootSaga() {
    yield all([
        fork(postFotoProfilSaga),
        fork(getCheckUsernameSaga),
        fork(putUpdateAkunSaga),
        fork(getSaldoSaga)
    ])
}