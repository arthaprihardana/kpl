/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-20 07:51:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-10 10:12:46
 */
import React, { Component } from 'react';
import { View, Text, Dimensions, Platform, Animated, Easing, StyleSheet } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import Feather from 'react-native-vector-icons/Feather';
import { WHITE_COLOR, ERROR_COLOR, LIGHT_PRIMARY_COLOR } from '../../../assets/colors';
import { networkStatus } from '../../../actions';

const { width } = Dimensions.get("window");

class NetworkIndicator extends Component {

    state = {
        showNetworkInfo: false,
        // fadeAnim: new Animated.Value(0),
        // xPosition: new Animated.ValueXY({ x: 0, y: -30 }),
        // animated: false
    }

    componentDidMount() {
        this.checkConnectivity();
        // // setTimeout(() => this.animated, 3000)
        // // Animated.timing(                  // Animate over time
        // //     this.state.fadeAnim,            // The animated value to drive
        // //     {
        // //         toValue: 1,                   // Animate to opacity: 1 (opaque)
        // //         duration: 10000,              // Make it take a while
        // //         useNativeDriver: true
        // //     }
        // //   ).start(); 
        // Animated.timing(this.state.xPosition, {
        //     toValue: 0,
        //     easing: Easing.out(),
        //     duration: 2000,
        //     useNativeDriver: true
        // }).start();
    }
    
    checkConnectivity = () => {
        if (Platform.OS === "android") {
            const subscription = NetInfo.addEventListener(state => {
                this.props.networkStatus({
                    isConnected: state.isConnected,
                    connectivityType: state.type,
                });
            });
        }
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.isConnected !== prevProps.isConnected) {
            return { isConnected: this.props.isConnected }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.isConnected) {
                this.setState({
                    showNetworkInfo: true
                })
            }
        }
    }

    render() {
        return (
            this.state.showNetworkInfo && !this.props.isConnected ?
            <Animated.View style={[styles.container, { top: 0, backgroundColor: !this.props.isConnected ? ERROR_COLOR : LIGHT_PRIMARY_COLOR } ]}>
                <Feather name={!this.props.isConnected ? "wifi-off" : "wifi"} size={10} color={WHITE_COLOR} style={{ marginRight: 16 }} />
                <Text style={{ fontSize: 10, color: WHITE_COLOR }}>{!this.props.isConnected ? "Anda sedang offline" : "Anda kembali online"}</Text>
            </Animated.View> 
            : <View />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        // top: 0,
        width: width,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: !this.props.isConnected ? ERROR_COLOR : LIGHT_PRIMARY_COLOR,
        flexDirection: 'row'
    }
})

const mapStateToProps = ({ network }) => {
    const { isConnected } = network;
    return { isConnected };
}

export default connect(mapStateToProps, {
    networkStatus
})(NetworkIndicator);