/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:02:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-22 01:59:56
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform, DatePickerAndroid } from 'react-native';
import moment from 'moment';
import "moment/locale/id";
import { SECONDARY_COLOR, TEXT_COLOR, ICON_COLOR, BORDER_COLOR, BACKGROUND_INPUT_FIELD, PLACEHOLDER_COLOR } from '../../../assets/colors';
import Feather from 'react-native-vector-icons/Feather';

export default class DatePicker extends Component {

    openCalendar = async () => {
        try {
            const format = { date: new Date };
            if(this.props.hasMinDate) {
                format.minDate = this.props.minDate || new Date();
            }
            const {action, year, month, day} = await DatePickerAndroid.open(format);
            if (action !== DatePickerAndroid.dismissedAction) {
                this.props.callback(moment({day: day, month: month, year: year}));
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.openCalendar() }>
                    <View style={styles.picker}>
                        <Text style={{ color: this.props.value ? TEXT_COLOR : PLACEHOLDER_COLOR }}>{this.props.value || this.props.placeholder}</Text>
                    </View>
                    <Feather name="calendar" size={20} color={ICON_COLOR} style={{ position: 'absolute', right: 20, top: 15 }} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        marginBottom: 10
    },
    picker: {
        height: 50, 
        borderWidth: 0.5, 
        borderColor: BORDER_COLOR, 
        backgroundColor: BACKGROUND_INPUT_FIELD,
        borderRadius: 25, 
        paddingHorizontal: 25,
        justifyContent: 'center'
    }
});