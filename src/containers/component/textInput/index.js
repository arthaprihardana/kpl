/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:31:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-08-01 07:00:05
 */
import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Text } from 'react-native';
import { TEXT_COLOR, BORDER_COLOR, BACKGROUND_INPUT_FIELD, PLACEHOLDER_COLOR, PRIMARY_COLOR } from '../../../assets/colors';

export default class InputText extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    {...this.props}
                    ref={this.props.refs}
                    style={[styles.inputText, this.props.style || {}, this.props.multiline ? {} : {height:  50}]} 
                    placeholder={this.props.placeholder}
                    placeholderTextColor={PLACEHOLDER_COLOR}
                    />
                { this.props.errorMessage && 
                <View style={{ width: '100%', height: 30, flexWrap: "wrap", paddingHorizontal: 30, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 10, color: '#f66' }}>{this.props.errorMessage}</Text>
                </View> }
                { this.props.info && 
                <View style={{ width: '100%', height: 30, flexWrap: "wrap", paddingHorizontal: 30, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 10, color: TEXT_COLOR }}>{this.props.info}</Text>
                </View> }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // height: 80,
        marginTop: 10,
        marginBottom: 10,
        justifyContent: 'flex-end'
    },
    inputText: {
        // height:  50, 
        borderWidth: 0.5, 
        borderColor: BORDER_COLOR, 
        backgroundColor: BACKGROUND_INPUT_FIELD,
        color: TEXT_COLOR,
        borderRadius: 25, 
        paddingHorizontal: 25
    }
})