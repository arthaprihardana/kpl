/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:31:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-23 09:11:27
 */
import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Text, TouchableOpacity, Dimensions } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import _ from "lodash";
import { TEXT_COLOR, BORDER_COLOR, BACKGROUND_INPUT_FIELD, PLACEHOLDER_COLOR, PRIMARY_COLOR, ICON_COLOR } from '../../../assets/colors';

const { width } = Dimensions.get("window");

export default class Counter extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPressIn={() => this.props.value !== "" && this.props.value > this.props.minValue ? this.props.onSub((parseInt(this.props.value) - 1).toString()) : this.props.onSub(_.toString(this.props.minValue))  } style={styles.buttonMin}>
                        <Feather name="minus" size={24} color={ICON_COLOR} />
                    </TouchableOpacity>
                    <TextInput
                        {...this.props}
                        ref={this.props.refs}
                        style={[styles.inputText, this.props.style || {}]} 
                        placeholder={this.props.minValue < 1 ? this.props.placeholder : null}
                        placeholderTextColor={PLACEHOLDER_COLOR}
                        />
                    <TouchableOpacity onPressIn={() => this.props.value !== "" ? this.props.onAdd((parseInt(this.props.value) + 1).toString()) : this.props.onSub(_.toString(this.props.minValue + 1)) } style={styles.buttonMax}>
                        <Feather name="plus" size={16} color={ICON_COLOR} />
                    </TouchableOpacity>
                </View>
                { this.props.errorMessage && 
                <View style={{ width: '100%', height: 30, flexWrap: "wrap", paddingHorizontal: 30, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 10, color: '#f66' }}>{this.props.errorMessage}</Text>
                </View> }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        marginBottom: 10,
        justifyContent: 'flex-end'
    },
    inputText: {
        flex: 1,
        height: 50, 
        borderWidth: 0.5, 
        borderColor: BORDER_COLOR, 
        backgroundColor: BACKGROUND_INPUT_FIELD,
        color: TEXT_COLOR,
        // borderRadius: 25, 
        paddingHorizontal: -10,
        textAlign: 'center'
    },
    buttonMin: {
        height: 50,
        width: 50,
        borderBottomLeftRadius: 25,
        borderTopLeftRadius: 25,
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonMax: {
        height: 50,
        width: 50,
        borderBottomRightRadius: 25,
        borderTopRightRadius: 25,
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        justifyContent: 'center',
        alignItems: 'center'
    }
})