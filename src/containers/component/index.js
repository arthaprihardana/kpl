/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:35:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-31 07:37:55
 */
import TextInputComponent from './textInput';
import ButtonComponent from './button';
import PickerComponent from './picker';
import DatePickerComponent from './datePicker';
import TimePickerComponent from './timePicker';
import BannerComponent from './banner';
import TabBarComponent from './tabBar';
import NavBarComponent from './navbar';
import ModalResponseComponent from './modalResponse';
import HeaderElipseComponent from './headerElipse';
import NetworkIndicatorComponent from './network';
import OptionDrawerComponent from './optionDrawer';
import CounterComponent from './counter';

export const InputText = TextInputComponent;
export const Button = ButtonComponent;
export const Picker = PickerComponent;
export const DatePicker = DatePickerComponent;
export const TimePicker = TimePickerComponent;
export const Banner = BannerComponent;
export const TabBar = TabBarComponent;
export const NavBar = NavBarComponent;
export const ModalResponse = ModalResponseComponent;
export const HeaderElipse = HeaderElipseComponent;
export const NetworkIndicator = NetworkIndicatorComponent;
export const OptionDrawer = OptionDrawerComponent;
export const InputCounter = CounterComponent;