/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:40:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-22 12:44:41
 */
import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { PRIMARY_COLOR, DISABLED_COLOR, WHITE_COLOR } from '../../../assets/colors';

export default class Button extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={[styles.base, { height: this.props.small ? 35 : 50 }, this.props.primary ? styles.primary : {}, this.props.secondary ? styles.secondary : {}, this.props.disabled ? styles.disabled : {}, ]}
                    {...this.props} >
                    { this.props.isLoading ? 
                        <View style={{ flexDirection: 'row' }}>
                            <ActivityIndicator size="small" color={WHITE_COLOR} />
                            <Text style={[styles.labelPrimary, { fontSize: 16, fontWeight: '300', marginLeft: 20 }]}>Loading ...</Text>
                        </View>
                        :
                        <Text style={[ this.props.primary ? styles.labelPrimary : {}, this.props.secondary ? styles.labelSecondary : {}, this.props.disabled ? styles.labelDisabled : {}, { fontSize: 16, fontWeight: '500' } ]}>{this.props.label}</Text>
                    }
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        marginBottom: 10
    },
    base: {
        // height: 50, 
        width: '100%', 
        // flex: 1,
        borderRadius: 25, 
        paddingHorizontal: 25, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    primary: {
        backgroundColor: PRIMARY_COLOR, 
    },
    secondary: {
        backgroundColor: '#629749', 
    },
    disabled: {
        backgroundColor: DISABLED_COLOR, 
    },
    labelPrimary: { color: '#FFFFFF' },
    labelSecondary: { color: '#616161' },
    labelDisabled: { color: '#babdbe' }
})