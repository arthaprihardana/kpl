/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-10 06:00:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-01 10:10:31
 */
import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, Dimensions, Image, LayoutAnimation, StyleSheet, ImageBackground, ActivityIndicator } from 'react-native';
import { connect } from "react-redux";
import { WHITE_COLOR, DISABLED_COLOR } from '../../../assets/colors';
import { getBannerPromo } from '../../../actions';
import Indicator from './indicator';

const { width, height } = Dimensions.get('window');

class Banner extends Component {

    state = {
        banner: [],
        bannerIndex: 0
    }

    componentDidMount() {
        if(this.props.isConnected) {
            this.props.getBannerPromo();
        }
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.isConnected !== prevProps.isConnected) {
            return { isConnected: this.props.isConnected }
        }
        if(this.props.banner !== prevProps.banner) {
            return { banner: this.props.banner }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.isConnected) {
                this.props.getBannerPromo();
            }
            if(snapshot.banner) {
                this.setState({
                    banner: snapshot.banner
                });
            }
        }
    }

    isFloat(n) {
        return Number(n) === n && n % 1 !== 0;
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ScrollView ref={ ref => this._banner = ref } horizontal pagingEnabled showsHorizontalScrollIndicator={false} style={{ 
                    width: width
                }} 
                contentContainerStyle={{
                    paddingHorizontal: 10
                }}
                onResponderRelease={()=>{
                    var interval = width;
                    var snapTo = (this.scrollingRight)? Math.ceil(this.lastx / interval) : 
                    Math.floor(this.lastx / interval);
                    var scrollTo = snapTo * interval;
                    this._banner.scrollTo(0,scrollTo);
                }}
                scrollEventThrottle={32}
                onScroll={ event => {
                    var nextx = event.nativeEvent.contentOffset.x;
                    this.scrollingRight = (nextx > this.lastx);
                    this.lastx = nextx;
                }}
                decelerationRate="fast"
                onMomentumScrollEnd={event => {
                    let pos = Math.floor(event.nativeEvent.contentOffset.x);
                    let w = Math.floor(width-20);
                    let idx = pos / w;
                    {/* let idx = (pos % w) + 1; */}
                    this.setState(prevState => ({ bannerIndex: this.isFloat(Math.floor(idx)) ? prevState.bannerIndex : Math.floor(idx) }))
                    // this.setState(prevState => ({ bannerIndex: this.state.banner.length > idx ? idx : prevState.bannerIndex }))
                }}
                snapToInterval={width - 20}
                snapToAlignment="center">
                    { 
                    this.props.loading ? 
                        <View style={{ width: width, height: 140, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size="large" color={WHITE_COLOR} />
                        </View>
                    : 
                    this.state.banner.map((value, key) => 
                        <View key={key} style={{ width: width - 20, height: 140, paddingTop: 10, paddingLeft: 3, paddingRight: 3 }}>
                            {/* <ImageBackground style={styles.image} imageStyle={{ borderRadius: 10 }} source={{uri: value.url_promo }} resizeMode="cover" fadeDuration={0} /> */}
                            <Image style={styles.image} source={{uri: value.url_promo}} resizeMode="contain" fadeDuration={0} />
                        </View>
                    )}
                </ScrollView>
                { !this.props.loading && <Indicator banner={this.state.banner} bannerIndex={this.state.bannerIndex} /> }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    imagePlaceholder: {
        flex: 1,
        width: '100%',
        height: '100%',
        borderRadius: 10,
        backgroundColor: DISABLED_COLOR
    },
    image: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover',
        borderRadius: 10
    }
})

const mapStateToProps = ({ bannerPromoReducer, network }) => {
    const { loading, banner } = bannerPromoReducer;
    const { isConnected } = network;
    return { loading, banner, isConnected } 
}

export default connect(mapStateToProps, {
    getBannerPromo
})(Banner);