/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-16 11:08:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-21 01:53:59
 */
import React, { Component } from 'react';
import { View, TouchableOpacity, LayoutAnimation, Dimensions, StyleSheet } from 'react-native';
import { connect } from "react-redux";
import { WHITE_COLOR } from '../../../assets/colors';

const { width } = Dimensions.get("window");

class Indicator extends Component {
    render() {
        LayoutAnimation.spring();
        return (
            <View style={styles.container}>
                { !this.props.loading && this.props.banner.map((value, key) => 
                    <TouchableOpacity key={key} style={[{ width: this.props.bannerIndex === key ? 10 : 5}, styles.indicator]} /> 
                )}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 18,
        left: 16,
        width: width,
        // justifyContent: 'center',
        // alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 10
    },
    indicator: {
        // height: 10,
        // backgroundColor: WHITE_COLOR,
        // borderRadius: 5,
        // marginLeft: 5,
        // marginRight: 5
        height: 5,
        backgroundColor: WHITE_COLOR,
        borderRadius: 2.5,
        marginLeft: 2.5,
        marginRight: 2.5
    }
});

const mapStateToProps = ({ bannerPromoReducer }) => {
    const { loading } = bannerPromoReducer;
    return { loading } 
}

export default connect(mapStateToProps, {})(Indicator);