/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:31:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-26 15:10:01
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Modal, ActivityIndicator, TextInput, ScrollView, Dimensions, TouchableHighlight } from 'react-native';
import { SECONDARY_COLOR, TEXT_COLOR, ICON_COLOR, WHITE_COLOR, BORDER_COLOR, DISABLED_COLOR, BACKGROUND_INPUT_FIELD, PLACEHOLDER_COLOR } from '../../../assets/colors';
import Feather from 'react-native-vector-icons/Feather';
import _ from 'lodash';

const { height } = Dimensions.get("window");

export default class Picker extends Component {

    state = {
        modalVisible: false,
        data: []
    }

    componentDidMount() {
        this.setState({ data: this.props.data });
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props !== prevProps) {
            this.setState({ data: this.props.data });
        }
    }
    
    setModalVisible = (visible) => {
        this.setState({modalVisible: visible});
    }

    onSearch = text => {
        let reg = new RegExp(text.toLowerCase(), "g");
        let s = _.filter(this.props.data, v => reg.test(v.nama.toLowerCase()) );
        this.setState({ data: text.length > 0 ? s : this.props.data });
    }

    onSelected = value => {
        this.props.callback(value);
        this.setModalVisible(!this.state.modalVisible);
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableHighlight underlayColor="rgba(255,255,255, 0.1)" onPress={() => !this.props.isLoading ? this.setModalVisible(!this.state.modalVisible) : {} }>
                    <View>
                        <View style={styles.picker}>
                            <Text style={{ color: this.props.value ? TEXT_COLOR : PLACEHOLDER_COLOR, paddingRight: 40 }}>{this.props.value || this.props.placeholder}</Text>
                        </View>
                        { this.props.isLoading ? <ActivityIndicator size="small" color={ICON_COLOR} style={styles.iconPosition} /> : <Feather name="chevron-down" size={20} color={ICON_COLOR} style={styles.iconPosition} /> }
                    </View>
                </TouchableHighlight>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.setModalVisible(!this.state.modalVisible)}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalHeader}>
                            <TouchableOpacity style={styles.closeButton} onPress={() => this.setModalVisible(!this.state.modalVisible) }>
                                <Feather name="x" size={22} color={ICON_COLOR} />
                            </TouchableOpacity>
                            <View style={styles.modalHeaderTitle}>
                                <Text style={styles.headerTitle}>{this.props.placeholder}</Text>
                            </View>
                        </View>
                        <View>
                            { this.props.isSearchable && 
                            <View style={styles.searchContainer}>
                                <TextInput 
                                    style={styles.searchInput}
                                    placeholder={this.props.searchPlaceholder || "Cari ..."}
                                    placeholderTextColor={PLACEHOLDER_COLOR}
                                    onChangeText={ text =>  this.onSearch(text) } />
                                <Feather name="search" size={14} color={ICON_COLOR} style={{ position: 'absolute', right: 30, top: 20 }} />
                            </View> }
                            <ScrollView style={{ marginBottom: 220 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                            { this.state.data && _.map(this.state.data, (v, k) => (
                                <TouchableOpacity key={k} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }} onPress={() => this.onSelected(v) }>
                                    <Text>{v.nama}</Text>
                                </TouchableOpacity>
                            )) }
                            </ScrollView>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // height: 80,
        marginTop: 10,
        marginBottom: 10,
        justifyContent: 'flex-end'
    },
    picker: {
        height: 50, 
        borderWidth: 0.5, 
        borderColor: BORDER_COLOR, 
        backgroundColor: BACKGROUND_INPUT_FIELD,
        borderRadius: 25, 
        paddingHorizontal: 25,
        justifyContent: 'center'
    },
    modalContainer: { 
        marginTop: 0 
    },
    modalHeader: { 
        width: '100%', 
        height: 56, 
        backgroundColor: WHITE_COLOR, 
        borderBottomWidth: .5, 
        borderBottomColor: BORDER_COLOR, 
        flexDirection: 'row' 
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    modalHeaderTitle: { 
        width: 150, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'flex-start' 
    },
    headerTitle: { 
        color: TEXT_COLOR, 
        fontWeight: '600' 
    },
    searchContainer: { 
        width: '100%', 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 16, 
        backgroundColor: DISABLED_COLOR, 
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 1 
    },
    searchInput: {
        width: '100%',
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 25,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        paddingRight: 40
    },
    iconPosition: {
        position: 'absolute', right: 20, top: 15
    }
});