/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-14 22:33:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-14 23:47:52
 */
import React, { Component } from 'react';
import { View, Modal, Text, TouchableOpacity, Image, Dimensions, StyleSheet, StatusBar } from 'react-native';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, ERROR_COLOR } from '../../../assets/colors';

const { width, height } = Dimensions.get("window");

export default class ModalResponse extends Component {
    render() {
        return (
        <View style={{ flex: 1}}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.responseVisible}
                onRequestClose={() => this.props.setResponseVisible(false)}>
                <StatusBar backgroundColor={WHITE_COLOR} barStyle="dark-content" />
                <View style={styles.container}>
                    <View style={styles.content}>
                        <View style={styles.responseIcon}>
                            { this.props.type === "sukses" ?
                                <Image source={require('../../../assets/image/success.png')} style={{ width: 100, height: 100}} /> : 
                                <Image source={require('../../../assets/image/error.png')} style={{ width: 100, height: 100}} /> 
                            }
                        </View>

                        <View style={styles.responseMessage}>
                            <Text style={{ fontWeight: '600', fontSize: 18 }}>{ this.props.type === "sukses" ? "Sukses" : "Gagal" }</Text>

                            <View style={{ width: '100%', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontWeight: '300', fontSize: 12, textAlign: 'center'}}>
                                    {this.props.message}
                                </Text>
                            </View>
                        </View>

                        <View style={{ flex: 1 }}>
                            <TouchableOpacity onPress={this.props.action}>
                                <Text style={{ color: this.props.type === "sukses" ? DARK_PRIMARY_COLOR : ERROR_COLOR, fontWeight: '500', fontSize: 14 }}>
                                    { this.props.type === "sukses" ? "Lanjutkan" : "Coba Lagi" }
                                </Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            </Modal>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width,
        height: height,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    content: {
        backgroundColor: WHITE_COLOR,
        width: '70%',
        minHeight: 300,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 2
    },
    responseIcon: {
        flex: 2,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    responseMessage: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 16
    }
})