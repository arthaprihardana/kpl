/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-15 00:12:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-22 05:49:27
 */
import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { NAVBAR_COLOR, PRIMARY_COLOR } from '../../../assets/colors';

const { width, height } = Dimensions.get("window");

export default class HeaderElipse extends Component {
    render() {
        return (
            <View style = {
                {
                    width: width,
                    height: width,
                    position: 'absolute',
                    top: -280,
                    backgroundColor: PRIMARY_COLOR,
                    borderBottomLeftRadius: (width / 2),
                    borderBottomRightRadius: (width / 2),
                    transform: [{
                        scaleX: 2
                    }]
                }
            }
            />  
        )
    }
}
