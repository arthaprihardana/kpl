/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:02:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-22 01:59:56
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform, TimePickerAndroid } from 'react-native';
import moment from 'moment';
import "moment/locale/id";
import { SECONDARY_COLOR, TEXT_COLOR, ICON_COLOR, BORDER_COLOR, BACKGROUND_INPUT_FIELD, PLACEHOLDER_COLOR } from '../../../assets/colors';
import Feather from 'react-native-vector-icons/Feather';

export default class TimePicker extends Component {

    openTimer = async () => {
        try {
            const {action, hour, minute} = await TimePickerAndroid.open({
              hour: parseInt(moment().format('HH')),
              minute: parseInt(moment().format('mm')),
              is24Hour: true, // Will display '2 PM'
            });
            if (action !== TimePickerAndroid.dismissedAction) {
              this.props.callback(moment({hour: hour, minute: minute}).format('HH:mm:ss'));
            }
          } catch ({code, message}) {
            console.warn('Cannot open time picker', message);
          }
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.openTimer() }>
                    <View style={styles.picker}>
                        <Text style={{ color: this.props.value ? TEXT_COLOR : PLACEHOLDER_COLOR }}>{this.props.value || this.props.placeholder}</Text>
                    </View>
                    <Feather name="clock" size={20} color={ICON_COLOR} style={{ position: 'absolute', right: 20, top: 15 }} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        marginBottom: 10
    },
    picker: {
        height: 50, 
        borderWidth: 0.5, 
        borderColor: BORDER_COLOR, 
        backgroundColor: BACKGROUND_INPUT_FIELD,
        borderRadius: 25, 
        paddingHorizontal: 25,
        justifyContent: 'center'
    }
});