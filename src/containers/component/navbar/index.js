/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-10 06:36:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-31 21:44:18
 */
import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { PRIMARY_COLOR, WHITE_COLOR, ERROR_COLOR } from '../../../assets/colors';
import Feather from 'react-native-vector-icons/Feather';
import { Actions } from 'react-native-router-flux';
import { NetworkIndicator } from '../../component';

class NavBar extends Component {
    render() {
        return (
            <View style={[styles.container, { paddingHorizontal: this.props.isBack ? 8 : 16 }]}>
                { this.props.isImage && 
                <View style={styles.image}>
                    <Image source={ this.props.image } style={{ width: 40, height: 40 }} resizeMode="cover" />
                </View> }
                { this.props.isBack && 
                <TouchableHighlight style={styles.back} onPress={() => Actions.pop() } underlayColor={'rgba(255,255,255,0.3)'}>
                    <Feather name="arrow-left" size={28} color={WHITE_COLOR} />
                </TouchableHighlight> }
                <Text style={styles.title}>{ this.props.isImage ? (this.props.isLogin ? `Hai, ${this.props.login.nama.slice(0, 10)}...` : this.props.title ) : this.props.title}</Text>
                { (this.props.isNotification && this.props.isLogin) && 
                <TouchableHighlight style={[styles.notif, { position: 'absolute', right: 45, top: 0 }]} onPress={() => Actions.push('notification')} underlayColor={'rgba(255,255,255,0.3)'}>
                    <View>
                        <Feather name="bell" size={22} color={WHITE_COLOR} />
                        {/* <View style={{ position: 'absolute', top: -4, right: -4, backgroundColor: ERROR_COLOR, width: 18, height: 18, borderRadius: 9, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: WHITE_COLOR, fontSize: 8 }}>2</Text>
                        </View> */}
                    </View>
                </TouchableHighlight> }
                { this.props.isOption && 
                <TouchableHighlight style={[styles.back, { position: 'absolute', right: 6, top: 0 }]} onPress={this.props.onOptionClick} underlayColor={'rgba(255,255,255,0.3)'}>
                    <Feather name="more-vertical" size={18} color={WHITE_COLOR} />
                </TouchableHighlight> }
                <NetworkIndicator />
            </View> 
        )
    }
}

const styles = StyleSheet.create({
    container: { 
        flexDirection: 'row', 
        width: '100%', 
        height: 56, 
        // backgroundColor: 'rgba(255, 255, 255, 0)', 
        backgroundColor: PRIMARY_COLOR,
        justifyContent: 'flex-start', 
        alignItems: 'center'
    },
    title: { 
        fontWeight: '600', 
        fontSize: 18,
        color: WHITE_COLOR 
    },
    image: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'flex-start' 
    },
    back: {
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center',
        borderRadius: 28
    },
    notif: {
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center',
        borderRadius: 28
    }
});

const mapStateToProps = ({ network, loginReducer }) => {
    const { isConnected } = network;
    const { isLogin, login } = loginReducer;
    return { isConnected, isLogin, login };
}

export default connect(mapStateToProps,{})(NavBar)