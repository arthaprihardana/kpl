/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-21 20:24:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-11-12 07:19:33
 */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Linking } from 'react-native';
import { connect } from 'react-redux';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { BORDER_COLOR, ICON_COLOR } from '../../../assets/colors';
import Config from 'react-native-config';
// const BASE_URL = `${Config.SARAN_URL}`;
const BASE_URL = 'https://chat.whatsapp.com/6OoUm569yMQ1kQHLmkdCEc';

class OptionDrawer extends Component {

    state = {
        showTelp: false
    }

    onClick = url => {
        Linking.openURL(url).catch((err) => console.error('An error occurred', err));
    }

    render() {
        return (
            <View style={{ flex: 1, borderWidth: .5, borderColor: BORDER_COLOR }}>
                <View style={{ height: 50, paddingLeft: 16, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ height: 50, justifyContent: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: '500' }}>Kontak Kami</Text>
                    </View>
                    <TouchableOpacity onPress={this.props.onCloseDrawer} style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Feather name="x" size={20} color={ICON_COLOR} />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => {
                    this.setState({ showTelp: !this.state.showTelp })
                    // this.onClick('tel://0217394422') 
                    }} style={{ height: 50, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                    <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Feather name="phone" size={18} color={ICON_COLOR} />
                    </View>
                    <View style={{ height: 50, justifyContent: 'center' }}>
                        <Text>Telepon</Text>
                    </View>
                    <View style={{ position: 'absolute', right: 16, top: 0, height: 50, justifyContent: 'center' }}>
                        <Feather name={this.state.showTelp ? "chevron-down" : "chevron-right"} size={18} color={ICON_COLOR} />
                    </View>
                </TouchableOpacity>
                { this.state.showTelp &&
                <View>
                    <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                        <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Feather name="phone" size={18} color={ICON_COLOR} />
                        </View>
                        <View style={{ height: 50, justifyContent: 'center' }}>
                            <Text>Sekertariat - 5000</Text>
                        </View>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                        <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Feather name="phone" size={18} color={ICON_COLOR} />
                        </View>
                        <View style={{ height: 50, justifyContent: 'center' }}>
                            <Text>Toko dan Simpan Pinjam - 5017</Text>
                        </View>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                        <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Feather name="phone" size={18} color={ICON_COLOR} />
                        </View>
                        <View style={{ height: 50, justifyContent: 'center' }}>
                            <Text>Koordinator - 5021</Text>
                        </View>
                    </View>
                </View>
                }
                <TouchableOpacity onPress={() => this.onClick('geo:-6.2390976,106.7645825?q=') } style={{ height: 50, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                    <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Feather name="map-pin" size={18} color={ICON_COLOR} />
                    </View>
                    <View style={{ height: 50, justifyContent: 'center' }}>
                        <Text>Alamat</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.onClick('whatsapp://send?text=&phone=6281293370337') } style={{ height: 50, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                    <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Ionicons name="logo-whatsapp" size={18} color={ICON_COLOR} />
                    </View>
                    <View style={{ height: 50, justifyContent: 'center' }}>
                        <Text>WhatsApp</Text>
                    </View>
                </TouchableOpacity>
                {this.props.isLogin && 
                <TouchableOpacity onPress={() => this.onClick(BASE_URL) } style={{ height: 50, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                    <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Feather name="bookmark" size={18} color={ICON_COLOR} />
                    </View>
                    <View style={{ height: 50, justifyContent: 'center' }}>
                        <Text>Saran</Text>
                    </View>
                </TouchableOpacity>
                }
            </View>
        )
    }
}

const mapStateToProps = ({ loginReducer }) => {
    const { isLogin } = loginReducer;
    return { isLogin }
}

export default connect(mapStateToProps, {

})(OptionDrawer);