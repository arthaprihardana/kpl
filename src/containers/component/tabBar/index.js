/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:48:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-07 22:56:34
 */
import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, Text, TouchableNativeFeedback } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Feather from 'react-native-vector-icons/Feather';
import { connect } from 'react-redux';
import { BORDER_COLOR, PRIMARY_COLOR, WHITE_COLOR, TEXT_COLOR, ICON_COLOR, DARK_PRIMARY_COLOR } from '../../../assets/colors';

class TabBar extends Component {
    render() {
        const { selectedIndex, isLogin, login } = this.props;
        return (
            <View style={styles.container}>
                <View style={[styles.tabs]}>
                    <TouchableOpacity 
                        onPressIn={() => Actions.jump('beranda', { title: isLogin ? `Hai, ${login.nama.slice(0, 10)}` : 'Beranda' })}
                        style={[styles.tab, selectedIndex === 0 ? { backgroundColor: WHITE_COLOR } : {} ]}>
                        {/* <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: WHITE_COLOR, justifyContent: 'center', alignItems: 'center' }}> */}
                            <Feather name="home" size={24} color={selectedIndex === 0 ? PRIMARY_COLOR : ICON_COLOR} />
                        {/* </View> */}
                        <Text style={{ fontSize: 12, lineHeight: 15, color: selectedIndex === 0 ? PRIMARY_COLOR : ICON_COLOR, fontWeight: '500' }}>Beranda</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPressIn={() => {
                            if(this.props.isLogin) {
                                Actions.jump('riwayat')
                            } else {
                                Actions.push('login')
                            }
                        }}
                        style={[styles.tab, selectedIndex === 1 ? { backgroundColor: WHITE_COLOR } : {} ]}>
                        {/* <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: WHITE_COLOR, justifyContent: 'center', alignItems: 'center' }}> */}
                            <Feather name="activity" size={24} color={selectedIndex === 1 ? PRIMARY_COLOR : ICON_COLOR} />
                        {/* </View> */}
                        <Text style={{ fontSize: 12, lineHeight: 15, color: selectedIndex === 1 ? PRIMARY_COLOR : ICON_COLOR, fontWeight: '500' }}>Riwayat</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPressIn={() => Actions.jump('berita')}
                        style={[styles.tab, selectedIndex === 2 ? { backgroundColor: WHITE_COLOR } : {} ]}>
                        {/* <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: WHITE_COLOR, justifyContent: 'center', alignItems: 'center' }}> */}
                            <Feather name="layers" size={24} color={selectedIndex === 2 ? PRIMARY_COLOR : ICON_COLOR} />
                        {/* </View> */}
                        <Text style={{ fontSize: 12, lineHeight: 15, color: selectedIndex === 2 ? PRIMARY_COLOR : ICON_COLOR, fontWeight: '500' }}>Berita</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPressIn={() => {
                            if(this.props.isLogin) {
                                Actions.jump('akun')
                            } else {
                                Actions.push('login')
                            }
                        }}
                        style={[styles.tab, selectedIndex === 3 ? { backgroundColor: WHITE_COLOR } : {} ]}>
                        {/* <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: WHITE_COLOR, justifyContent: 'center', alignItems: 'center' }}> */}
                            <Feather name="user" size={24} color={selectedIndex === 3 ? PRIMARY_COLOR : ICON_COLOR} />
                        {/* </View> */}
                        <Text style={{ fontSize: 12, lineHeight: 15, color: selectedIndex === 3 ? PRIMARY_COLOR : ICON_COLOR, fontWeight: '500' }}>Akun</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: { 
        marginBottom: -1, 
        marginTop: 0, 
        borderTopWidth: .8, 
        borderTopColor: BORDER_COLOR,
        // shadowRadius: 2,
        // shadowOffset: {
        //     width: 0,
        //     height: 0,
        // },
        // shadowColor: '#ccc',
        // elevation: 2,
    },
    tab: {
        flex: 1,
        height: 68,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabs: {
        height: 70,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0,
        // backgroundColor: PRIMARY_COLOR
    },
});

const mapStateToProps = ({ loginReducer }) => {
    const { isLogin, login } = loginReducer;
    return { isLogin, login }
}

export default connect(mapStateToProps, {})(TabBar)