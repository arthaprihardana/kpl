/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:07:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 22:15:12
 */
import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Keyboard, Text, TouchableOpacity, Modal, TextInput, Platform, Alert } from 'react-native';
import { connect } from "react-redux";
import _ from "lodash";
import Contacts from 'react-native-contacts';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

import { InputText, Button, Picker, DatePicker, HeaderElipse, ModalResponse } from '../../component';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, BORDER_COLOR, ICON_COLOR, PLACEHOLDER_COLOR, TEXT_COLOR } from '../../../assets/colors';
import { postOrderPulsa } from '../../../actions'
import { Actions } from 'react-native-router-flux';
import { PROVIDER } from '../../../constant';
import AndroidPermissions from '../../../helpers/permissions';

class Pulsa extends Component {

    state = {
        forms: {
            nama_anggota: this.props.isLogin ? this.props.login.nama : "",
            id_anggota: this.props.isLogin ? this.props.login.id : "",
            provider: "",
            nohp: "",
            nominal: ""
        },
        errors: {
            err_provider: null,
            err_nohp: null,
            err_nominal: null,
            err_nama_anggota: null,
            err_id_anggota: null
        },
        kontak: [],
        searchKontak: [],
        multiKontak: false,
        dataMultiKontak: [],
        masterNominal: [{
            id: 1,
            nama: "Rp. 5.000",
            value: "5000",
        }, {
            id: 2,
            nama: "Rp. 10.000",
            value: "10000",
        }, {
            id: 3,
            nama: "Rp. 25.000",
            value: "25000",
        }, {
            id: 4,
            nama: "Rp. 50.000",
            value: "50000",
        }, {
            id: 5,
            nama: "Rp. 100.000",
            value: "100000",
        }],
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null,
        modalContact: false
    }

    componentDidMount() {
        
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.orderPulsa !== prevProps.orderPulsa) {
            return { orderPulsa: this.props.orderPulsa }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.orderPulsa) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.orderPulsa.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.orderPulsa.status ? "Selamat, pemesanan pulsa sedang diproses" : "Terjadi kesalahan dalam pemesanan",
                    responseAction: () => {
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
        }
    }

    onChange = (key, value) => {
        let { forms, errors } = this.state;
        if(key === "nohp") {
            let f = PROVIDER.filter(v => value.match(v.pattern));
            forms["provider"] = f.length > 0 ? f[0].name : null;
            forms["nominal"] = "";
        }
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: (typeof value === "object" ? Object.keys(value).length > 0 : value.length > 0) && val === undefined }))
        });Text
    }

    onSubmit = () => {
        Keyboard.dismiss();
        this.props.postOrderPulsa({
            nama_anggota: this.state.forms.nama_anggota,
            id_anggota: this.state.forms.id_anggota,
            provider: this.state.forms.provider,
            nohp: this.state.forms.nohp,
            nominal: this.state.forms.nominal.value
        });
    }

    onValidate = async key => {
        let except = ["id_anggota", "nama_anggota", "provider"];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        console.log('a' , a)
        return a;
    }

    onOpenContact = () => {
        if(this.state.kontak.length > 0) {
            this.setState({
                searchKontak: this.state.kontak,
                modalContact: !this.state.modalContact
            });
        } else {
            let req = this.requestContactPermission();
            req.then(value => {
                if(value === "granted") {
                    Contacts.getAll((err, contacts) => {
                        // contacts returned in Array
                        let kontak = _.filter(contacts, v => v.phoneNumbers.length > 0);
                        this.setState({
                            kontak: kontak,
                            searchKontak: kontak,
                            modalContact: !this.state.modalContact
                        })
                    })
                } else {
                    Alert.alert(
                        'Info',
                        'Anda tidak diizinkan untuk dapat mengakses kontak',
                        [
                            {text: 'Tanya Nanti', onPress: () => console.log('Ask me later pressed')},
                            {
                                text: 'Batal',
                                onPress: () => this.onOpenContact(),
                                style: 'cancel',
                            },
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        {cancelable: false},
                    );
                }
            });
        }
    }

    onContactSearch = text => {
        let reg = new RegExp(text.toLowerCase(), "g");
        let s = _.filter(this.state.kontak, v => reg.test(v.givenName.toLowerCase()));
        this.setState({ searchKontak: text.length > 0 ? s : this.state.kontak });
    }

    onContactSelected = value => {
        if(value.phoneNumbers.length > 1) {
            this.setState({
                multiKontak: true,
                dataMultiKontak: value
            })
        } else {
            this.onChange("nohp", value.phoneNumbers[0].number.replace(/\s+|-/g, ''))
            this.setState({
                modalContact: !this.state.modalContact
            });
        }
    }

    onContactMultiSelected = value => {
        this.onChange("nohp", value.replace(/\s+|-/g, ''))
        this.setState({
            modalContact: !this.state.modalContact,
            multiKontak: false,
            dataMultiKontak: [],
            searchKontak: this.state.kontak
        });
    }

    onRequestContactClose = () => {
        this.setState({
            multiKontak: false,
            modalContact: !this.state.modalContact
        })
    }

    requestContactPermission = async () => {
        try {
            if(Platform.OS === "android") {
                let Permissions = new AndroidPermissions();
                let reqContact = await Permissions.requestReadContactPermission();
                return reqContact;
            }
        } catch (err) {
            console.warn(err);
        }
    }

    render() {
        const { forms } = this.state;
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                {/* <HeaderElipse /> */}
                <View style={styles.container}>
                    {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                        <View style={{ width: 110, height: 110, borderRadius: 55, justifyContent: 'center', alignItems: 'center', backgroundColor: DARK_PRIMARY_COLOR }}>
                            <FontAwesome name="bed" size={60} color={WHITE_COLOR} />
                        </View>
                    </View> */}
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pemesan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="nama_anggota"
                                refs={ref => this._nama_anggota = ref}
                                placeholder="Nama Anggota"
                                keyboardType="default"
                                value={forms.nama_anggota}
                                onChangeText={text => this.onChange("nama_anggota", text)} />
                        </View>
                    </View>

                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pembelian</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="nohp"
                                refs={ref => this._nohp = ref}
                                keyboardType="phone-pad"
                                placeholder="No Handphone"
                                value={forms.nohp}
                                onChangeText={text => this.onChange("nohp", text)}
                                maxLenght={15}
                                style={{ paddingRight: 60 }}/>
                            <TouchableOpacity onPress={() => this.onOpenContact()} style={{ position: 'absolute', right: 20, top: 24 }}>
                                <FontAwesome name="address-book" size={20} color={ICON_COLOR} />
                            </TouchableOpacity>
                        </View>
                        {/* <View>
                            <Picker 
                                nativeID="provider"
                                refs={ref => this._provider = ref}
                                placeholder="Provider"
                                data={this.state.masterProvider}
                                isLoading={this.props.loading}
                                isSearchable={false}
                                value={this.state.forms.provider.nama}
                                callback={value => this.onChange("provider", value)} />
                        </View> */}
                        <View>
                            <Picker 
                                nativeID="nominal"
                                refs={ref => this._nominal = ref}
                                placeholder="Nominal"
                                data={this.state.masterNominal}
                                isLoading={false}
                                isSearchable={false}
                                value={forms.nominal.nama}
                                callback={value => this.onChange("nominal", value)} />
                        </View>
                    </View>
                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Beli"
                            onPress={() => this.onSubmit()}
                            isLoading={this.props.loading} />
                    </View>
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalContact}
                    onRequestClose={() => this.onRequestContactClose()}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalHeader}>
                            <TouchableOpacity style={styles.closeButton} onPress={() => this.onRequestContactClose()}>
                                <Feather name="x" size={22} color={ICON_COLOR} />
                            </TouchableOpacity>
                            <View style={styles.modalHeaderTitle}>
                                <Text style={styles.headerTitle}>Kontak</Text>
                            </View>
                        </View>
                        {this.state.multiKontak ? 
                            <View>
                                <TouchableOpacity onPress={() => this.setState({ multiKontak: false })} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 16, flexDirection: 'row' }}>
                                    <Feather name="arrow-left" size={16} />
                                    <Text>Kembali</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }}>
                                    <Text>Pilih No Telepon dari Kontak : {this.state.dataMultiKontak.givenName} {this.state.dataMultiKontak.familyName}</Text>
                                </View>
                                <ScrollView style={{ marginBottom: 220 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                                    {this.state.dataMultiKontak.phoneNumbers && _.map(this.state.dataMultiKontak.phoneNumbers, (v, k) => (
                                        <TouchableOpacity key={k} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }} onPress={() => this.onContactMultiSelected(v.number) }>
                                            <Text>{v.number}</Text>
                                        </TouchableOpacity>
                                    ))}
                                </ScrollView>
                            </View>
                            :
                            <View>
                                <View style={styles.searchContainer}>
                                    <TextInput 
                                        style={styles.searchInput}
                                        placeholder={"Cari Kontak..."}
                                        placeholderTextColor={PLACEHOLDER_COLOR}
                                        onChangeText={ text =>  this.onContactSearch(text) } />
                                    <Feather name="search" size={14} color={ICON_COLOR} style={{ position: 'absolute', right: 30, top: 20 }} />
                                </View>
                                <ScrollView style={{ marginBottom: 220 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                                { this.state.searchKontak && _.map(this.state.searchKontak, (v, k) => (
                                    <TouchableOpacity key={k} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }} onPress={() => this.onContactSelected(v) }>
                                        <Text>{v.givenName} {v.familyName}</Text>
                                    </TouchableOpacity>
                                )) }
                                </ScrollView>
                            </View>
                        }
                    </View>
                </Modal>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    },
    modalHeader: { 
        width: '100%', 
        height: 56, 
        backgroundColor: WHITE_COLOR, 
        borderBottomWidth: .5, 
        borderBottomColor: BORDER_COLOR, 
        flexDirection: 'row' 
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    modalHeaderTitle: { 
        width: 150, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'flex-start' 
    },
    headerTitle: { 
        color: TEXT_COLOR, 
        fontWeight: '600' 
    },
    searchContainer: { 
        width: '100%', 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 16, 
        backgroundColor: DISABLED_COLOR, 
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 1 
    },
    searchInput: {
        width: '100%',
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 25,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        paddingRight: 40
    },
    iconPosition: {
        position: 'absolute', right: 20, top: 15
    }
})

const mapStateToProps = ({ loginReducer, topupReducer }) => {
    const { isLogin, login } = loginReducer;
    const { loading, orderPulsa } = topupReducer;
    return {isLogin, login, loading, orderPulsa }
}

export default connect(mapStateToProps, {
    postOrderPulsa
})(Pulsa);
