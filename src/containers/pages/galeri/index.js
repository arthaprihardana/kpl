/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-31 23:56:23 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-08-05 00:11:59
 */
import React, { Component } from 'react';
import {  View, Image, TouchableOpacity, ScrollView, Keyboard, Alert, PermissionsAndroid, FlatList, Dimensions, CameraRoll, Platform, Text } from 'react-native';
import { onTakePicture } from '../../../actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import AndroidPermissions from '../../../helpers/permissions';

const { width } = Dimensions.get("window");

class Galeri extends Component {

    state = {
        photos: [],
        page: {},
        isInfinityScroll: false,
        isNoMoreData: false,
        showInfoNoMoreData: false,
        capture: {}
    }

    componentDidMount() {
        this.onOpenGaleri();
    }

    componentWillUnmount() {
        Actions.refresh();
    }

    onOpenGaleri = () => {
        let req = this.requestReadStoragePermission();
        req.then(value => {
            if(value === "granted") {
                this.getCameraRoll();
            } else {
                Alert.alert(
                    'Info',
                    'Anda tidak diizinkan untuk dapat mengakses galeri',
                    [
                        {text: 'Tanya Nanti', onPress: () => console.log('Ask me later pressed')},
                        {
                            text: 'Batal',
                            onPress: () => this.getCameraRoll(),
                            style: 'cancel',
                        },
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    {cancelable: false},
                );
            }
        });
    }

    requestReadStoragePermission = async () => {
        try {
            if(Platform.OS === "android") {
                let Permissions = new AndroidPermissions();
                let reqStorage = await Permissions.requestReadStoragePermission();
                return reqStorage;
            }
        } catch (err) {
            console.log(err);
        }
    }

    getCameraRoll = has_next_page => {
        let options = {
            first: 20,
            assetType: 'Photos'
        };
        if(has_next_page) {
            options.after = this.state.page.end_cursor
        }
        CameraRoll.getPhotos(options).then(r => {
            this.setState(prevState => ({ 
                photos: _.concat(prevState.photos, r.edges),
                page: r.page_info,
                isNoMoreData: !r.page_info.has_next_page,
                showInfoNoMoreData: !r.page_info.has_next_page
            }), () => {
                if(this.state.isNoMoreData) {
                    setTimeout(() => {
                        this.setState({ showInfoNoMoreData: false })
                    }, 2000);
                }
            });
        }).catch((err) => {
            console.log('err==>', err);
        });
    }

    renderFooterComponent = () => {
        if (!this.state.isInfinityScroll) return null;
        return (
            <View
                style={{
                    position: 'relative',
                    width: width,
                    paddingVertical: 10,
                    borderTopWidth: 0.5,
                    borderColor: '#ccc',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Text style={{ fontSize: 12 }}>Sedang memuat gambar ...</Text>
            </View>
          );
    }

    handleLoadMore = () => {
        if(!this.state.isNoMoreData) {
            this.setState(prevState => ({
                isInfinityScroll: true
            }), () => this.getCameraRoll(this.state.page.has_next_page));
        }
    }

    onSelectedImage = async node => {
        this.props.onTakePicture(node.image.uri);
        Actions.jump('akun');
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    data={this.state.photos}
                    numColumns={4}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.5}
                    ListFooterComponent={() => this.renderFooterComponent()}
                    renderItem={({item, index}) => <TouchableOpacity onPress={() => this.onSelectedImage(item.node)}>
                        <Image
                            key={index}
                            style={{
                                width: width / 4,
                                height: width / 4,
                            }}
                            source={{ uri: item.node.image.uri }}
                            resizeMode="cover"
                            loadingIndicatorSource={require('../../../assets/image/image_default.png')}/>
                        </TouchableOpacity>
                    }
                    />
                { this.state.showInfoNoMoreData && 
                <View style={{ position: 'absolute', bottom: 0, width: width, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: '#f08080' }}>
                    <Text style={{ fontSize: 10, color: '#fff' }}>Tidak ada lagi gambar untuk ditampilkan</Text>
                </View> }
            </View>
        )
    }
}

const mapStateToProps = ({  }) => {
    return {  };
}

export default connect(mapStateToProps, {
    onTakePicture
})(Galeri);
