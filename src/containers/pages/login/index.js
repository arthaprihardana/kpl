/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:07:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-30 05:43:23
 */
import React, { Component } from 'react';
import { ScrollView, View, Image, StyleSheet, TouchableOpacity, Text, Keyboard } from 'react-native';
import { connect } from "react-redux";
import _ from "lodash";
import firebase from 'react-native-firebase';
import { InputText, Button, ModalResponse, HeaderElipse } from '../../component';

// import InputText from '../../component/textInput';
// import Button from '../../component/button';

import { Actions } from 'react-native-router-flux';
import { DARK_PRIMARY_COLOR, WHITE_COLOR } from '../../../assets/colors';
import { postLogin, clearPrevLogin } from '../../../actions';

class Login extends Component {

    state = {
        forms: {
            nik: null,
            pin: null
        },
        errors: {
            err_nik: null,
            err_pin: null
        },
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null
    }

    componentDidMount() {
        this.props.clearPrevLogin();
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.login !== prevProps.login) {
            return { login: this.props.login }
        }
        return null;
    }

    componentWillUnmount() {
        Actions.refresh();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.login) {
                this.setState({
                    isResponse: true,
                    responseType: _.isObject(snapshot.login) ? "sukses" : "gagal",
                    responseMessage: _.isObject(snapshot.login) ? "Selamat, NIK anda terdaftar di sistem Koperasi Pegawai Lamigas" : snapshot.login,
                    responseAction: () => {
                        if(_.isObject(snapshot.login)) {
                            firebase.messaging().subscribeToTopic(snapshot.login.noanggota);
                        }
                        this.setState({ isResponse: false});
                        if(typeof snapshot.login === "object") {
                            Actions.reset("main");
                        } else {
                            this.props.clearPrevLogin();
                        }
                    }
                });
            }
        }
    }

    onChange = (key, value) => {
        let forms = this.state.forms;
        let errors = this.state.errors;
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: val === undefined }))
        });
    }

    onValidate = async key => {
        let except = [];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    onSubmit = () => {
        Keyboard.dismiss();
        this.props.postLogin(this.state.forms);
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <HeaderElipse />
                <View style={styles.container}>
                    <View style={{ width: '100%', justifyContent: 'flex-start', alignItems: 'center', height: 180 }}>
                        <Image source={ require('../../../assets/image/logo.png') } style={{ width: 100, height: 100}} />
                    </View>
                    <View>
                        <InputText 
                            nativeID="nik"
                            refs={ref => this._nik = ref}
                            placeholder="NIK"
                            onChangeText={text => this.onChange("nik", text)}
                            style={{ textAlign: 'center' }}
                            returnKeyType="next"
                            onSubmitEditing={() => this._pin.focus()} />
                    </View>
                    <View>
                        <InputText 
                            nativeID="pin"
                            refs={ref => this._pin = ref}
                            placeholder="PIN"
                            secureTextEntry
                            onChangeText={text => this.onChange("pin", text)}
                            style={{ textAlign: 'center' }} 
                            returnKeyType="go"
                            onSubmitEditing={() => this.state.readyToSubmit ? this.onSubmit() : {} } />
                    </View>
                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            onPress={() => this.onSubmit()} 
                            isLoading={this.props.loading}
                            label="Login" />
                    </View>
                    <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <Text>Belum Punya Akun ? </Text>
                        <TouchableOpacity onPress={() => Actions.push('register')}>
                            <Text style={{ color: DARK_PRIMARY_COLOR }}>Daftar Disini</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'flex-start'
    },
    contentContainer: {
        paddingVertical: 20
    }
})

const mapStateToProps = ({ loginReducer }) => {
    const { isLogin, login, loading } = loginReducer;
    return { isLogin, login, loading };
}

export default connect(mapStateToProps, {
    postLogin,
    clearPrevLogin
})(Login)