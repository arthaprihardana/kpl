/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-10 09:58:49 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-18 22:12:46
 */
import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import {PRIMARY_COLOR} from '../../../assets/colors/index';
import { WebView } from 'react-native-webview';
import Config from 'react-native-config';
const BASE_URL =  `${Config.DASHBOARD_URL}`;

class Dashboard extends Component {

    state = {
        url: `${BASE_URL}/pengurus/dashboard?anggotaid=${this.props.login.id}`
    }

    renderLoading = () => {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="small" color={PRIMARY_COLOR} />
            </View>
        );
    }

    render() {
        const { url } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <WebView 
                    startInLoadingState={true}
                    renderLoading={() => this.renderLoading()}
                    source={{ uri: url }}
                />
            </View>
        );
    }
}

const mapStateToProps = ({ loginReducer }) => {
    const { login } = loginReducer;
    return { login }
}

export default connect(mapStateToProps, {})(Dashboard);