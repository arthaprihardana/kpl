/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-10 23:56:52
 */
import React, { Component } from 'react';
import { View, Text, ActivityIndicator, Image, StyleSheet, Dimensions, ScrollView, Linking, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Config from 'react-native-config';
import { connect } from "react-redux";
import moment from 'moment';
import 'moment/locale/id';
import { PRIMARY_COLOR, WHITE_COLOR, DARK_PRIMARY_COLOR, LIGHT_PRIMARY_COLOR } from '../../../assets/colors';
import { getForceUpdate } from '../../../actions';

let timer = 3000;
const { width, height } = Dimensions.get("window");
const version = require('../../../../version.json').version;

class SplashScreen extends Component {

    state = {
        isUpdate: false,
    }
    
    componentDidMount() {
        const { getForceUpdate } = this.props;
        if(Config.NODE_ENV === "development") {
            setTimeout(() => Actions.reset('lightbox') , timer)
        } else {
            getForceUpdate();
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.update !== prevProps.update) {
            return this.props.update;
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot !== `${version}`) {
                this.setState({
                    isUpdate: true,
                });
                // Linking.openURL("market://details?id=com.kp.lemigas").catch((err) => console.error('An error occurred', err));
            } else {
                setTimeout(() => Actions.reset('lightbox') , timer)
            }
        }
    }
    
    render() {
        const { loading } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <View style={[styles.triangleCorner]} />
                <View style={[styles.triangleCorner, styles.triangleCornerBottomRight, styles.right]} />
                {this.state.isUpdate ? 
                    <View style={{ position: 'absolute', top: 0, left: 0, width: width, height: height, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={ require('../../../assets/image/logo.png') } style={{ width: 100, height: 100 }} resizeMode="cover" />

                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                                <Text style={{ fontSize: 16, fontWeight: "500", marginBottom: 22, color: WHITE_COLOR, textAlign: 'center' }}>Untuk menikmati fitur terbaru, Aplikasi harus diupdate.</Text>
                                <TouchableOpacity 
                                    style={{width: 300, height: 55, backgroundColor: '#FFFFFF', borderRadius: 5, justifyContent: 'center', alignItems: 'center'}}
                                    onPress={() => Linking.openURL("market://details?id=com.kp.lemigas").catch((err) => console.error('An error occurred', err))}>
                                    <Text>Update Sekarang</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View> : 
                    <View style={{ position: 'absolute', top: 0, left: 0, width: width, height: height, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={ require('../../../assets/image/logo.png') } style={{ width: 100, height: 100 }} resizeMode="cover" />

                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                                <Text style={{ fontSize: 22, fontWeight: "600", marginBottom: 22, color: WHITE_COLOR, textAlign: 'center' }}>Koperasi{'\n'}Pegawai Lemigas</Text>
                                <Text style={{ fontSize: 12, fontWeight: '200', color: WHITE_COLOR }}>Jl. Ciledug Raya Kav. 109, Cipulir, Kebayoran Lama</Text>
                                <Text style={{ fontSize: 12, fontWeight: '200', color: WHITE_COLOR }}>Jakarta Selatan 12230</Text>
                            </View>

                            <ActivityIndicator size="small" color={WHITE_COLOR} style={{ marginTop: 16 }} />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', paddingBottom: 20 }}>
                            <Text style={{ fontSize: 10, color: WHITE_COLOR }}>Version {version}</Text>
                            <Text style={{ fontSize: 10, color: WHITE_COLOR }}>Copyright {moment().year()}</Text>
                        </View>
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    left: {
        position: 'absolute',
        top: 0,
        left: 0
    },
    right: {
        position: 'absolute',
        top: 0,
        right: 0
    },
    triangleCorner: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderRightWidth: width,
        borderTopWidth: height,
        borderRightColor: 'transparent',
        borderTopColor: PRIMARY_COLOR
    },
    triangleCornerBottomRight: {
        transform: [
            {rotate: '180deg'}
        ],
        borderTopColor: PRIMARY_COLOR,
        opacity: 0.95
    }
});

const mapStateToProps = ({ forceUpdate }) => {
    const { loading, update } = forceUpdate;
    return { loading, update }
}

export default connect(mapStateToProps, {
    getForceUpdate
})(SplashScreen)