/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-25 01:51:41
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, FlatList, Image, Dimensions, RefreshControl, LayoutAnimation, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import _ from "lodash";
import moment from 'moment';
import 'moment/locale/id';
import { HeaderElipse } from '../../component';
import { getBerita } from '../../../actions';
import { BORDER_COLOR, WHITE_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, TEXT_COLOR, ERROR_COLOR } from '../../../assets/colors';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get("window");

class Berita extends Component {

    state = {
        berita: [],
        limit: 10,
        offset: 0,
        isLoading: false,
        isRefresh: false,
        isInfinityScroll: false,
        isNoMoreData: false,
        showInfoNoMoreData: false
    }

    componentDidMount() {
        this.props.getBerita({
            limit: this.state.limit,
            offset: this.state.offset
        });
    }
    
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.berita !== prevProps.berita) {
            return { berita: this.props.berita }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.berita) {
                if(snapshot.berita.length > 0) {
                    this.setState(prevState => ({
                        berita: _.concat(prevState.berita, snapshot.berita),
                        isInfinityScroll: true
                    }))
                } else {
                    LayoutAnimation.easeInEaseOut();
                    this.setState(prevState => ({
                        isInfinityScroll: false,
                        offset: prevState.berita.length,
                        isNoMoreData: true,
                        showInfoNoMoreData: true
                    }));
                }
            }
        }
        if(this.props.loading !== prevProps.loading) {
            this.setState({ isLoading: this.props.loading })
        }
        if(this.state.showInfoNoMoreData !== prevState.showInfoNoMoreData) {
            setTimeout(() => {
                LayoutAnimation.easeInEaseOut();
                this.setState({ showInfoNoMoreData: false })
            }, 2000);
        }
    }

    onRefresh = () => {
        this.setState({
            berita: [],
            limit: 10,
            offset: 0,
            isNoMoreData: false
        }, () => {
            this.props.getBerita({
                limit: this.state.limit,
                offset: this.state.offset
            });
        })
    }

    handleLoadMore = () => {
        if(!this.state.isNoMoreData) {
            this.setState(prevState => ({
                offset: prevState.offset + prevState.limit,
                isInfinityScroll: true
            }), () => this.props.getBerita({
                limit: this.state.limit,
                offset: this.state.offset
            }));
        }
    }

    renderItem = (item, index) => {
        return (
            <TouchableOpacity onPress={() => Actions.push("detailBerita", { title: item.judul_berita, id: item.id_berita })} style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                <View style={{ flexDirection: 'row', width: width }}>
                    <View style={{ width: (1/4) * width, justifyContent: 'flex-start', alignItems: 'center' }}>
                        <Image source={{ uri: item.gambar_berita }} style={{ width: (1/4) * width, height: (1/4) * width, borderRadius: 10}} />
                    </View>
                    <View style={{ width: ((3/4) * width) - 20, paddingRight: 20, paddingLeft: 20 }}>
                        <Text style={{ fontSize: 18, fontWeight: '800' }}>{item.judul_berita.length > 55 ? item.judul_berita.slice(0, 55) : item.judul_berita }</Text>
                        {/* <Text style={{ fontSize: 14, fontWeight: '300', textAlign: "justify" }} textBreakStrategy="highQuality">{item.isi_berita.length > 50 ? item.isi_berita.slice(0,50) : item.isi_berita}</Text> */}
                        <View style={{ flexDirection: 'column', marginTop: 16 }}>
                            <Text style={{ fontSize: 10, fontWeight: '400', color: '#cccccc' }}>KPL | {moment(item.tanggal_berita).format("LLLL")}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderHeader = item => {
        return (
            <TouchableOpacity onPress={() => Actions.push("detailBerita", { title: item.judul_berita, id: item.id_berita })} style={styles.newsHeaderCard}>
                <ImageBackground source={{ uri: item.gambar_berita }} style={styles.newsHeaderImage} resizeMode="cover">
                    <View style={styles.newsContainerTitle}>
                        <Text style={styles.newsTitle}>{ item.judul_berita.length > 55 ? `${item.judul_berita.slice(0, 55)}...` : item.judul_berita }</Text>

                        {/* <Text style={{ textAlign: "justify", color: WHITE_COLOR }}>{item.isi_berita.length > 50 ? `${item.isi_berita.slice(0,50)}...` : item.isi_berita }</Text> */}
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        )
    }

    renderSeparator = () => {
        return (
            <View style={{ width: '100%', borderBottomColor: BORDER_COLOR, borderBottomWidth: 0.5 }} />
        )
    }

    renderEmptyComponent = () => {
        return (
            this.state.isLoading && this.state.berita.length === 0 ? 
            <View>
                <View style={styles.newsHeaderCard}>
                    <View style={[styles.newsHeaderImage, { backgroundColor: DISABLED_COLOR }]}>
                        <View style={[styles.newsContainerTitle]}>
                            <View style={{ width: 100, height: 16, backgroundColor: DISABLED_COLOR, marginBottom: 16, borderRadius: 8 }} />
                            <View style={{ width: '100%', height: 14, backgroundColor: DISABLED_COLOR, marginBottom: 10, borderRadius: 7 }} />
                            <View style={{ width: '100%', height: 14, backgroundColor: DISABLED_COLOR, borderRadius: 7 }} />
                        </View>
                    </View>
                </View>

                { _.map([1,2,3], (v, k) => (
                    <View key={k} style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                        <View style={{ flexDirection: 'row', width: width }}>
                            <View style={{ width: (1/4) * width, justifyContent: 'flex-start', alignItems: 'center' }}>
                                <View style={{ width: (1/4) * width, height: (1/4) * width, borderRadius: 10, backgroundColor: DISABLED_COLOR}} />
                            </View>
                            <View style={{ width: ((3/4) * width) - 20, paddingRight: 20, paddingLeft: 20 }}>
                                <View style={{ height: 18, width: 100, borderRadius: 9, backgroundColor: DISABLED_COLOR, marginBottom: 16 }} />
                                { _.map([1,2], (vv, kk) => <View key={kk} style={{ height: 14, width: '100%', borderRadius: 7, backgroundColor: DISABLED_COLOR, marginBottom: 5 }} /> ) }
                                <View style={{ flexDirection: 'column', marginTop: 16 }}>
                                    <View style={{ height: 10, width: 140, backgroundColor: DISABLED_COLOR, borderRadius: 5 }} />
                                </View>
                            </View>
                        </View>
                    </View>
                )) }
            </View> : <View />
        )
    }

    renderFooterComponent = () => {
        if (!this.state.isInfinityScroll) return null;
        return (
            <View
                style={{
                    position: 'relative',
                    width: width,
                    paddingVertical: 10,
                    borderTopWidth: 0.5,
                    borderColor: BORDER_COLOR,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Text style={{ fontSize: 12, color: TEXT_COLOR }}>Sedang memuat berita ...</Text>
            </View>
          );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.berita}
                    keyExtractor={item => item.id_berita.toString()}
                    ItemSeparatorComponent={() => this.renderSeparator()}
                    ListEmptyComponent={() => this.renderEmptyComponent()}
                    ListHeaderComponent={() => this.state.berita.length > 0 ? this.renderHeader(this.state.berita[0]) : <View /> }
                    ListFooterComponent={() => this.renderFooterComponent()}
                    renderItem={({item, index}) => this.renderItem(item, index) }
                    refreshControl={<RefreshControl
                            refreshing={this.state.isRefresh}
                            onRefresh={this.onRefresh}
                            colors={[DARK_PRIMARY_COLOR]}
                        />
                    }
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={this.state.limit}
                    />
                { this.state.showInfoNoMoreData && 
                <View style={{ position: 'absolute', bottom: 0, width: width, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: ERROR_COLOR }}>
                    <Text style={{ fontSize: 10, color: WHITE_COLOR }}>Tidak ada lagi berita untuk ditampilkan</Text>
                </View> }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    newsHeaderCard: {
        width: '100%',
        height: 200,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 0.5
    },
    newsContentCard: {
        width: '100%',
        height: 100,
        paddingHorizontal: 16, 
        paddingVertical: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 0.5,
        borderBottomColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
    },
    newsContainerTitle: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.1)',
        paddingHorizontal: 16,
        paddingVertical: 10
    },
    newsTitle: {
        fontSize: 16,
        color: WHITE_COLOR,
        textAlign: 'left',
        fontWeight: 'bold'
    },
    newsHeaderImage: {
        width: '100%',
        height: 200
    }
})

const mapStateToProps = ({ beritaReducer }) => {
    const { loading, berita } = beritaReducer;
    return { loading, berita }
}

export default connect(mapStateToProps, {
    getBerita
})(Berita);