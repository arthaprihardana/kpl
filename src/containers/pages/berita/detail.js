/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-25 02:05:15
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, ImageBackground, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { getDetailBerita } from '../../../actions';
import { WebView } from 'react-native-webview';
import { TEXT_COLOR, WHITE_COLOR, BORDER_COLOR, DISABLED_COLOR, PRIMARY_COLOR } from '../../../assets/colors';
import HTML from 'react-native-render-html';
import moment from 'moment';
import 'moment/locale/id';

class DetailBerita extends Component {

    state = {
        berita: []
    }

    componentDidMount() {
        this.props.getDetailBerita({
            id_berita: this.props.id
        });
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.detail !== prevProps.detail) {
            return { detail: this.props.detail }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.detail) {
                this.setState({ berita: snapshot.detail })
            }
        }
    }

    renderLoading = () => {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="small" color={PRIMARY_COLOR} />
            </View>
        );
    }

    render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                {/* <View> */}
                    <View style={styles.newsHeaderCard}>
                        { this.state.berita.length > 0 ?
                        <ImageBackground source={{ uri: this.state.berita[0].gambar_berita }} style={styles.newsHeaderImage} resizeMode="cover">
                            <View style={styles.newsContainerTitle}>
                                <Text style={styles.newsTitle}>{ this.state.berita[0].judul_berita.length > 55 ? `${this.state.berita[0].judul_berita.slice(0, 55)}...` : this.state.berita[0].judul_berita }</Text>
                                <Text style={{ color: WHITE_COLOR, fontSize: 10 }}>KPL | {moment(this.state.berita[0].tanggal_berita).format("LLL")}</Text>
                            </View>
                        </ImageBackground> :
                        <View style={[styles.newsHeaderImage, { backgroundColor: DISABLED_COLOR }]}>
                            <View style={[styles.newsContainerTitle]}>
                                <View style={{ width: 100, height: 16, backgroundColor: DISABLED_COLOR, marginBottom: 16, borderRadius: 8 }} />
                                <View style={{ width: '100%', height: 14, backgroundColor: DISABLED_COLOR, marginBottom: 10, borderRadius: 7 }} />
                            </View>
                        </View>
                        }
                    </View>
                    {/* <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                        <Text style={{ textAlign: "justify", color: TEXT_COLOR }}>{ this.state.berita[0].isi_berita }</Text>
                    </View> */}
                    { this.state.berita.length > 0 ?
                    <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                        <HTML html={this.state.berita[0].isi_berita}  />
                    </View>
                    :
                    <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                        <View style={{ width: '100%', height: 14, backgroundColor: DISABLED_COLOR, marginBottom: 10, borderRadius: 7 }} />
                        <View style={{ width: '100%', height: 14, backgroundColor: DISABLED_COLOR, marginBottom: 10, borderRadius: 7 }} />
                        <View style={{ width: '80%', height: 14, backgroundColor: DISABLED_COLOR, marginBottom: 10, borderRadius: 7 }} />
                    </View>
                    }
                {/* </View> */}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    newsHeaderCard: {
        width: '100%',
        height: 200,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 0.5
    },
    newsContentCard: {
        width: '100%',
        height: 100,
        paddingHorizontal: 16, 
        paddingVertical: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 0.5,
        borderBottomColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
    },
    newsContainerTitle: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.1)',
        paddingHorizontal: 16,
        paddingVertical: 10
    },
    newsTitle: {
        fontSize: 16,
        color: WHITE_COLOR,
        textAlign: 'left',
        fontWeight: 'bold'
    },
    newsHeaderImage: {
        width: '100%',
        height: 200
    }
})

const mapStateToProps = ({ beritaReducer }) => {
    const { loading, detail } = beritaReducer;
    return { loading, detail }
}

export default connect(mapStateToProps, {
    getDetailBerita
})(DetailBerita);