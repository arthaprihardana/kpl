/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-15 07:12:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 21:23:04
 */
import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Dimensions, Text, TouchableHighlight } from 'react-native';
import _ from 'lodash';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { HeaderElipse } from '../../component';
import { WHITE_COLOR, BORDER_COLOR, PRIMARY_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR } from '../../../assets/colors';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get("window");

export default class TopUp extends Component {

    state = {
        menuTopUp: [{
            id: 1, 
            nama: "Pulsa",
            action: "pulsa",
            icon: "signal"
        }, {
            id: 2, 
            nama: "Paket Data",
            action: "paketData",
            icon: "mobile"
        }, {
            id: 3, 
            nama: "Tagihan Listrik",
            action: "tagihanListrik",
            icon: "plug"
        }, {
            id: 4, 
            nama: "Token PLN",
            action: "tokenPln",
            icon: "bolt"
        }]
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <HeaderElipse />
                <View style={{ paddingHorizontal: 16, paddingVertical: 16, marginTop: 0 }}>
                    <View style={{ width: '100%', height: 50, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 16, marginBottom: 20 }}>
                        <Text style={{ color: WHITE_COLOR, fontSize: 18, fontWeight: '500', textAlign: 'center' }}>Sekarang Top Up</Text>
                        <Text style={{ color: WHITE_COLOR, fontSize: 18, fontWeight: '500', textAlign: 'center' }}>Bisa di Koperasi Lemigas Looh :)</Text>
                    </View>
                    <View style={{ flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between'}}>
                        { _.map(this.state.menuTopUp, (v, k) => (
                            v.nama !== null ?
                            <TouchableOpacity 
                                key={k} 
                                activeOpacity={0.9}
                                style={{ 
                                    width: (width / 3) - 20, 
                                    height: (width / 3) - 20, 
                                    borderWidth: .4, 
                                    borderColor: BORDER_COLOR, 
                                    justifyContent: 'center', 
                                    alignItems: 'center', 
                                    borderRadius: 10, 
                                    marginBottom: 10, 
                                    backgroundColor: WHITE_COLOR 
                                }} 
                                onPress={() => {
                                    Actions.push(v.action)
                                }}
                                underlayColor={DISABLED_COLOR}>
                                <FontAwesome name={v.icon} size={42} color={PRIMARY_COLOR} />
                                <Text style={{ fontSize: 12, fontWeight: '300', color: DARK_PRIMARY_COLOR }}>{v.nama}</Text>
                            </TouchableOpacity> : <View key={k} style={{
                                width: (width / 2) - 20, 
                                height: (width / 2) - 20, 
                            }} />
                        ))}
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        flex: 1,
        paddingVertical: 20
    }
})