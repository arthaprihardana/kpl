/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:07:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 22:14:57
 */
import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Keyboard, Text, TouchableOpacity, Modal, TextInput, PermissionsAndroid } from 'react-native';
import { connect } from "react-redux";
import _ from "lodash";

import { InputText, Button, Picker, DatePicker, HeaderElipse, ModalResponse } from '../../component';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, BORDER_COLOR, ICON_COLOR, PLACEHOLDER_COLOR, TEXT_COLOR } from '../../../assets/colors';
import { postOrderListrikToken } from '../../../actions'
import { Actions } from 'react-native-router-flux';
import {rupiah} from '../../../helpers';

class ListrikToken extends Component {

    state = {
        forms: {
            nama_anggota: this.props.isLogin ? this.props.login.nama : "",
            id_anggota: this.props.isLogin ? this.props.login.id : "",
            no_meter: "",
            nominal_token: ""
        },
        errors: {
            err_no_meter: null,
            err_nominal_token: null,
            err_nama_anggota: null,
            err_id_anggota: null
        },
        masternominal: [{
            id: 1,
            nama: rupiah(20000),
            value: "20000",
        }, {
            id: 2,
            nama: rupiah(50000),
            value: "50000",
        }, {
            id: 3,
            nama: rupiah(100000),
            value: "100000",
        }, {
            id: 4,
            nama: rupiah(200000),
            value: "200000",
        }, {
            id: 5,
            nama: rupiah(500000),
            value: "500000",
        }, {
            id: 6,
            nama: rupiah(1000000),
            value: "1000000",
        }],
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null,
        modalContact: false
    }

    componentDidMount() {
        
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.listrikToken !== prevProps.listrikToken) {
            return { listrikToken: this.props.listrikToken }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.listrikToken) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.listrikToken.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.listrikToken.status ? "Selamat, pembelian token listrik sedang diproses" : "Terjadi kesalahan dalam pembelian",
                    responseAction: () => {
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
        }
    }

    onChange = (key, value) => {
        let { forms, errors } = this.state;
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: (typeof value === "object" ? Object.keys(value).length > 0 : value.length > 0) && val === undefined }))
        });Text
    }

    onSubmit = () => {
        Keyboard.dismiss();
        this.props.postOrderListrikToken({
            nama_anggota: this.state.forms.nama_anggota,
            id_anggota: this.state.forms.id_anggota,
            no_meter: this.state.forms.no_meter,
            nominal_token: this.state.forms.nominal_token.value
        });
    }

    onValidate = async key => {
        let except = ["id_anggota", "nama_anggota"];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    render() {
        const { forms } = this.state;
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                {/* <HeaderElipse /> */}
                <View style={styles.container}>
                    {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                        <View style={{ width: 110, height: 110, borderRadius: 55, justifyContent: 'center', alignItems: 'center', backgroundColor: DARK_PRIMARY_COLOR }}>
                            <FontAwesome name="bed" size={60} color={WHITE_COLOR} />
                        </View>
                    </View> */}
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pemesan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="nama_anggota"
                                refs={ref => this._nama_anggota = ref}
                                placeholder="Nama Anggota"
                                keyboardType="default"
                                value={forms.nama_anggota}
                                onChangeText={text => this.onChange("nama_anggota", text)} />
                        </View>
                    </View>

                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pembelian</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="no_meter"
                                refs={ref => this._no_meter = ref}
                                keyboardType="phone-pad"
                                placeholder="No Meter"
                                value={forms.no_meter}
                                onChangeText={text => this.onChange("no_meter", text)}
                                maxLenght={15}
                                style={{ paddingRight: 60 }}/>
                        </View>
                        <View>
                            <Picker 
                                nativeID="nominal_token"
                                refs={ref => this._nominal_token = ref}
                                placeholder="Nominal"
                                data={this.state.masternominal}
                                isLoading={false}
                                isSearchable={false}
                                value={forms.nominal_token.nama}
                                callback={value => this.onChange("nominal_token", value)} />
                        </View>
                    </View>
                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Beli"
                            onPress={() => this.onSubmit()}
                            isLoading={this.props.loading} />
                    </View>
                </View>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    },
    modalHeader: { 
        width: '100%', 
        height: 56, 
        backgroundColor: WHITE_COLOR, 
        borderBottomWidth: .5, 
        borderBottomColor: BORDER_COLOR, 
        flexDirection: 'row' 
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    modalHeaderTitle: { 
        width: 150, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'flex-start' 
    },
    headerTitle: { 
        color: TEXT_COLOR, 
        fontWeight: '600' 
    },
    searchContainer: { 
        width: '100%', 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 16, 
        backgroundColor: DISABLED_COLOR, 
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 1 
    },
    searchInput: {
        width: '100%',
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 25,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        paddingRight: 40
    },
    iconPosition: {
        position: 'absolute', right: 20, top: 15
    }
})

const mapStateToProps = ({ loginReducer, topupReducer }) => {
    const { isLogin, login } = loginReducer;
    const { loading, listrikToken } = topupReducer;
    return {isLogin, login, loading, listrikToken }
}

export default connect(mapStateToProps, {
    postOrderListrikToken
})(ListrikToken);