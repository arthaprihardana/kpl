/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:07:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-06 11:52:35
 */
import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Keyboard, Text } from 'react-native';
import { connect } from "react-redux";
import _ from "lodash";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { InputText, Button, Picker, DatePicker, HeaderElipse, ModalResponse } from '../../component';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, BORDER_COLOR } from '../../../assets/colors';
import { postOrderKereta, clearPrevTiket, getStasiun } from '../../../actions';
import { Actions } from 'react-native-router-flux';

class Kereta extends Component {

    state = {
        forms: {
            dari: "",
            ke: "",
            penumpang_dewasa: "",
            penumpang_balita: "",
            waktu_kedatangan: "",
            kursi_kelas: "",
            nama_anggota: this.props.isLogin ? this.props.login.nama : "",
            id_anggota: this.props.isLogin ? this.props.login.id : ""
        },
        errors: {
            err_dari: null,
            err_ke: null,
            err_penumpang_dewasa: null,
            err_penumpang_balita: null,
            err_waktu_kedatangan: null,
            err_kursi_kelas: null,
            err_nama_anggota: null,
            err_id_anggota: null
        },
        masterStasiun: [],
        masterKelas: [{id: 1, nama: "Ekonomi"}, {id: 2, nama: "Bisnis"}, {id: 3, nama: "Eksekutif"}],
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null
    }

    componentDidMount() {
        this.props.getStasiun();
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.stasiun !== prevProps.stasiun) {
            return { stasiun: this.props.stasiun }
        }
        if(this.props.orderKereta !== prevProps.orderKereta) {
            return { orderKereta: this.props.orderKereta }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.stasiun) {
                this.setState({
                    masterStasiun: snapshot.stasiun
                });
            }
            if(snapshot.orderKereta) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.orderKereta.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.orderKereta.status ? "Selamat, pemesanan tiket kereta sedang diproses" : "Terjadi kesalahan dalam pemesanan",
                    responseAction: () => {
                        this.props.clearPrevTiket();
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
        }
    }

    onChange = (key, value) => {
        let forms = this.state.forms;
        let errors = this.state.errors;
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms });
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: val === undefined }))
        });
    }

    onSubmit = () => {
        const { forms } = this.state;
        Keyboard.dismiss();
        this.props.postOrderKereta({
            dari: forms.dari.nama,
            ke: forms.ke.nama,
            penumpang_dewasa: forms.penumpang_dewasa,
            penumpang_balita: forms.penumpang_balita,
            waktu_kedatangan: forms.waktu_kedatangan,
            kursi_kelas: forms.kursi_kelas.nama,
            nama_anggota: forms.nama_anggota,
            id_anggota: forms.id_anggota,
        });
    }

    onValidate = async key => {
        let except = ["id_anggota", "nama_anggota"];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    render() {
        const { forms } = this.state;
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                {/* <HeaderElipse /> */}
                <View style={styles.container}>
                    {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                        <View style={{ width: 110, height: 110, borderRadius: 55, justifyContent: 'center', alignItems: 'center', backgroundColor: DARK_PRIMARY_COLOR }}>
                            <FontAwesome name="train" size={60} color={WHITE_COLOR} />
                        </View>
                    </View> */}
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pemesan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="nama_anggota"
                                refs={ref => this._nama_anggota = ref}
                                placeholder="Nama Anggota"
                                keyboardType="default"
                                value={forms.nama_anggota}
                                onChangeText={text => this.onChange("nama_anggota", text)} />
                        </View>
                    </View>

                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Perjalanan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <Picker 
                                nativeID="dari"
                                refs={ref => this._dari = ref}
                                placeholder="Dari"
                                data={this.state.masterStasiun}
                                isLoading={this.props.loading}
                                isSearchable={true}
                                value={forms.dari.nama}
                                callback={value => this.onChange("dari", value)} />
                        </View>
                        <View>
                            <Picker 
                                nativeID="ke"
                                refs={ref => this._ke = ref}
                                placeholder="Ke"
                                data={this.state.masterStasiun}
                                isLoading={this.props.loading}
                                isSearchable={true}
                                value={forms.ke.nama}
                                callback={value => this.onChange("ke", value)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="penumpang_dewasa"
                                refs={ref => this._penumpang_dewasa = ref}
                                placeholder="Jumlah Penumpang Dewasa"
                                keyboardType="phone-pad"
                                maxLength={2}
                                onChangeText={text => this.onChange("penumpang_dewasa", text)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="penumpang_balita"
                                refs={ref => this._penumpang_balita = ref}
                                placeholder="Jumlah Penumpang Balita"
                                keyboardType="phone-pad"
                                maxLength={2}
                                onChangeText={text => this.onChange("penumpang_balita", text)} />
                        </View>
                        <View>
                            <DatePicker 
                                nativeID="waktu_kedatangan"
                                refs={ref => this._waktu_kedatangan = ref}
                                placeholder="Tanggal Keberangkatan"
                                hasMinDate={true}
                                value={forms.waktu_kedatangan}
                                callback={date => this.onChange("waktu_kedatangan", date.format("YYYY-MM-DD"))} />
                        </View>
                        <View>
                            <Picker 
                                nativeID="kursi_kelas"
                                refs={ref => this._kursi_kelas = ref}
                                placeholder="Kursi Kelas"
                                data={this.state.masterKelas}
                                value={forms.kursi_kelas.nama}
                                callback={value => this.onChange("kursi_kelas", value)} />
                        </View>
                    </View>
                    
                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Pesan"
                            onPress={() => this.onSubmit()}
                            isLoading={this.props.orderLoader} />
                    </View>
                </View>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    }
})

const mapStateToProps = ({ tiketReducer, loginReducer, stasiunReducer }) => {
    const { orderKereta } = tiketReducer;
    const { isLogin, login } = loginReducer;
    const { loading, stasiun } = stasiunReducer;
    const orderLoader = tiketReducer.loading;
    return { loading, stasiun, orderKereta, isLogin, login, orderLoader }
}

export default connect(mapStateToProps, {
    getStasiun,
    postOrderKereta,
    clearPrevTiket
})(Kereta);