/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 21:30:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 22:12:08
 */
import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Keyboard, Text, TouchableOpacity, Modal, TextInput, Platform, Alert } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import Contacts from 'react-native-contacts';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import 'moment/locale/id';
import Feather from 'react-native-vector-icons/Feather';
import { Actions } from 'react-native-router-flux';
import { getDetailSerbaUsaha, postOrderSerbaUsaha } from '../../../actions';
import { InputText, Button, Picker, DatePicker, HeaderElipse, ModalResponse, TimePicker } from '../../component';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, BORDER_COLOR, ICON_COLOR, PLACEHOLDER_COLOR, TEXT_COLOR } from '../../../assets/colors';
import {rupiah} from '../../../helpers';
import AndroidPermissions from '../../../helpers/permissions';

class FormSerbaUsaha extends Component {

    state = {
        detailSerbaUsaha: null,
        forms: {
            nama_anggota: this.props.isLogin ? this.props.login.nama : "",
            id_anggota: this.props.isLogin ? this.props.login.id : "",
            tanggal_book: "",
            jam_book: "",
            id_layanan: this.props.id_layanan,
            id_kategori: this.props.id_kategori,
            nama_kategori: this.props.title,
            telepon: "",
            ekstensi: ""
        },
        errors: {
            err_nama_anggota: null,
            err_id_anggota: null,
            err_tanggal_book: null,
            err_jam_book: null,
            err_id_layanan: null,
            err_id_kategori: null,
            err_nama_kategori: null,
            err_telepon: null,
            err_ekstensi: null
        },
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null,
        modalContact: false,
        kontak: [],
        searchKontak: [],
        multiKontak: false,
        dataMultiKontak: [],
    }

    componentDidMount() {
        
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.serbaUsaha !== prevProps.serbaUsaha) {
            return { serbaUsaha: this.props.serbaUsaha }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.serbaUsaha) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.serbaUsaha.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.serbaUsaha.status ? `Selamat, pemesanan ${this.state.forms.nama_kategori} sedang diproses` : `Terjadi kesalahan dalam pemesanan ${this.state.forms.nama_kategori}`,
                    responseAction: () => {
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
        }
    }

    onChange = (key, value) => {
        let { forms, errors } = this.state;
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: (typeof value === "object" ? Object.keys(value).length > 0 : value.length > 0) && val === undefined }))
        });Text
    }

    onSubmit = () => {
        const { forms } = this.state;
        Keyboard.dismiss();
        this.props.postOrderSerbaUsaha({
            nama_anggota: forms.nama_anggota,
            id_anggota: forms.id_anggota,
            tanggal_book: moment(`${forms.tanggal_book} ${forms.jam_book}`).format("YYYY-MM-DD HH:mm:ss"),
            id_layanan: forms.id_layanan,
            id_kategori: forms.id_kategori,
            nama_kategori: forms.nama_kategori,
            telepon: forms.telepon,
            ekstensi: forms.ekstensi
        });
    }

    onValidate = async key => {
        let except = ["id_anggota", "nama_anggota", "id_layanan", "id_kategori", "nama_kategori"];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    onOpenContact = () => {
        if(this.state.kontak.length > 0) {
            this.setState({
                searchKontak: this.state.kontak,
                modalContact: !this.state.modalContact
            });
        } else {
            let req = this.requestContactPermission();
            req.then(value => {
                if(value === "granted") {
                    Contacts.getAll((err, contacts) => {
                        // contacts returned in Array
                        let kontak = _.filter(contacts, v => v.phoneNumbers.length > 0);
                        this.setState({
                            kontak: kontak,
                            searchKontak: kontak,
                            modalContact: !this.state.modalContact
                        })
                    })
                } else {
                    Alert.alert(
                        'Info',
                        'Anda tidak diizinkan untuk dapat mengakses kontak',
                        [
                            {text: 'Tanya Nanti', onPress: () => console.log('Ask me later pressed')},
                            {
                                text: 'Batal',
                                onPress: () => this.onOpenContact(),
                                style: 'cancel',
                            },
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        {cancelable: false},
                    );
                }
            });
        }
    }

    onContactSearch = text => {
        let reg = new RegExp(text.toLowerCase(), "g");
        let s = _.filter(this.state.kontak, v => reg.test(v.givenName.toLowerCase()));
        this.setState({ searchKontak: text.length > 0 ? s : this.state.kontak });
    }

    onContactSelected = value => {
        if(value.phoneNumbers.length > 1) {
            this.setState({
                multiKontak: true,
                dataMultiKontak: value
            })
        } else {
            this.onChange("nohp", value.phoneNumbers[0].number.replace(/\s+|-/g, ''))
            this.setState({
                modalContact: !this.state.modalContact
            });
        }
    }

    onContactMultiSelected = value => {
        this.onChange("nohp", value.replace(/\s+|-/g, ''))
        this.setState({
            modalContact: !this.state.modalContact,
            multiKontak: false,
            dataMultiKontak: [],
            searchKontak: this.state.kontak
        });
    }

    onRequestContactClose = () => {
        this.setState({
            multiKontak: false,
            modalContact: !this.state.modalContact
        })
    }

    requestContactPermission = async () => {
        try {
            if(Platform.OS === "android") {
                let Permissions = new AndroidPermissions();
                let reqContact = await Permissions.requestReadContactPermission();
                return reqContact;
            }
        } catch (err) {
            console.warn(err);
        }
    }

    render() {
        const { forms, errors } = this.state;
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <View style={styles.container}>
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pemesan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="nama_anggota"
                                refs={ref => this._nama_anggota = ref}
                                placeholder="Nama Anggota"
                                keyboardType="default"
                                value={forms.nama_anggota}
                                onChangeText={text => this.onChange("nama_anggota", text)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="telepon"
                                refs={ref => this._telepon = ref}
                                keyboardType="phone-pad"
                                placeholder="No Handphone"
                                value={forms.telepon}
                                onChangeText={text => this.onChange("telepon", text)}
                                maxLenght={15}
                                style={{ paddingRight: 60 }}/>
                            <TouchableOpacity onPress={() => this.onOpenContact()} style={{ position: 'absolute', right: 20, top: 24 }}>
                                <FontAwesome name="address-book" size={20} color={ICON_COLOR} />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <InputText 
                                nativeID="ekstensi"
                                refs={ref => this._ekstensi = ref}
                                placeholder="Ekstensi"
                                keyboardType="phone-pad"
                                value={forms.ekstensi}
                                onChangeText={text => this.onChange("ekstensi", text)} />
                        </View>
                    </View>

                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pesanan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <DatePicker 
                                nativeID="tanggal_book"
                                refs={ref => this._tanggal_book = ref}
                                placeholder="Tanggal Booking"
                                value={forms.tanggal_book}
                                hasMinDate={true}
                                errorMessage={errors.err_tanggal_book}
                                callback={date => this.onChange("tanggal_book", date.format("YYYY-MM-DD"))} />
                        </View>
                        <View>
                            <TimePicker 
                                nativeID="jam_book"
                                refs={ref => this._jam_book = ref}
                                placeholder="Jam Booking"
                                value={forms.jam_book}
                                hasMinDate={false}
                                errorMessage={errors.err_jam_book}
                                callback={time => this.onChange("jam_book", time)} />
                        </View>
                    </View>
                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Pesan"
                            onPress={() => this.onSubmit()}
                            isLoading={this.props.loading} />
                    </View>
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalContact}
                    onRequestClose={() => this.onRequestContactClose()}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalHeader}>
                            <TouchableOpacity style={styles.closeButton} onPress={() => this.onRequestContactClose()}>
                                <Feather name="x" size={22} color={ICON_COLOR} />
                            </TouchableOpacity>
                            <View style={styles.modalHeaderTitle}>
                                <Text style={styles.headerTitle}>Kontak</Text>
                            </View>
                        </View>
                        {this.state.multiKontak ? 
                            <View>
                                <TouchableOpacity onPress={() => this.setState({ multiKontak: false })} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 16, flexDirection: 'row' }}>
                                    <Feather name="arrow-left" size={16} />
                                    <Text>Kembali</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }}>
                                    <Text>Pilih No Telepon dari Kontak : {this.state.dataMultiKontak.givenName} {this.state.dataMultiKontak.familyName}</Text>
                                </View>
                                <ScrollView style={{ marginBottom: 220 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                                    {this.state.dataMultiKontak.phoneNumbers && _.map(this.state.dataMultiKontak.phoneNumbers, (v, k) => (
                                        <TouchableOpacity key={k} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }} onPress={() => this.onContactMultiSelected(v.number) }>
                                            <Text>{v.number}</Text>
                                        </TouchableOpacity>
                                    ))}
                                </ScrollView>
                            </View>
                            :
                            <View>
                                <View style={styles.searchContainer}>
                                    <TextInput 
                                        style={styles.searchInput}
                                        placeholder={"Cari Kontak..."}
                                        placeholderTextColor={PLACEHOLDER_COLOR}
                                        onChangeText={ text =>  this.onContactSearch(text) } />
                                    <Feather name="search" size={14} color={ICON_COLOR} style={{ position: 'absolute', right: 30, top: 20 }} />
                                </View>
                                <ScrollView style={{ marginBottom: 220 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                                { this.state.searchKontak && _.map(this.state.searchKontak, (v, k) => (
                                    <TouchableOpacity key={k} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }} onPress={() => this.onContactSelected(v) }>
                                        <Text>{v.givenName} {v.familyName}</Text>
                                    </TouchableOpacity>
                                )) }
                                </ScrollView>
                            </View>
                        }
                    </View>
                </Modal>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    },
    modalHeader: { 
        width: '100%', 
        height: 56, 
        backgroundColor: WHITE_COLOR, 
        borderBottomWidth: .5, 
        borderBottomColor: BORDER_COLOR, 
        flexDirection: 'row' 
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    modalHeaderTitle: { 
        width: 150, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'flex-start' 
    },
    headerTitle: { 
        color: TEXT_COLOR, 
        fontWeight: '600' 
    },
    searchContainer: { 
        width: '100%', 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 16, 
        backgroundColor: DISABLED_COLOR, 
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 1 
    },
    searchInput: {
        width: '100%',
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 25,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        paddingRight: 40
    },
    iconPosition: {
        position: 'absolute', right: 20, top: 15
    }
})

const mapStateToProps = ({ loginReducer, serbaUsahaReducer }) => {
    const { isLogin, login } = loginReducer;
    const { loading, detailSerbaUsaha, serbaUsaha } = serbaUsahaReducer;
    return { isLogin, login, loading, detailSerbaUsaha, serbaUsaha };
}

export default connect(mapStateToProps, {
    getDetailSerbaUsaha,
    postOrderSerbaUsaha
})(FormSerbaUsaha)
