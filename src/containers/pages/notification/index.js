/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-01 10:29:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-31 21:45:58
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, FlatList, Image, Dimensions, RefreshControl, LayoutAnimation, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import _ from "lodash";
import moment from 'moment';
import 'moment/locale/id';
import Feather from 'react-native-vector-icons/Feather';
import { Actions } from 'react-native-router-flux';
import { BORDER_COLOR, WHITE_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, TEXT_COLOR, ERROR_COLOR } from '../../../assets/colors';
import {getNotifikasi} from '../../../actions';

const { width, height } = Dimensions.get("window");

class Notification extends Component {

    state = {
        notifikasi: [],
        limit: 10,
        offset: 0,
        isLoading: false,
        isRefresh: false,
        isInfinityScroll: false,
        isNoMoreData: false,
        showInfoNoMoreData: false
    }

    componentWillUnmount() {
        
    }

    componentDidMount() {
        this.props.getNotifikasi({
            id_anggota: this.props.login.id,
            limit: this.state.limit,
            offset: this.state.offset
        })
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.notif !== prevProps.notif) {
            return { notifikasi: this.props.notif }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.notifikasi) {
                if(snapshot.notifikasi.length > 0) {
                    this.setState(prevState => ({
                        notifikasi: _.concat(prevState.notifikasi, snapshot.notifikasi),
                        isInfinityScroll: true
                    }))
                } else {
                    LayoutAnimation.easeInEaseOut();
                    this.setState(prevState => ({
                        isInfinityScroll: false,
                        offset: prevState.notifikasi.length,
                        isNoMoreData: true,
                        showInfoNoMoreData: true
                    }));
                }
            }
        }
        if(this.props.loading !== prevProps.loading) {
            this.setState({ isLoading: this.props.loading })
        }
        if(this.state.showInfoNoMoreData !== prevState.showInfoNoMoreData) {
            setTimeout(() => {
                LayoutAnimation.easeInEaseOut();
                this.setState({ showInfoNoMoreData: false })
            }, 2000);
        }
    }

    onRefresh = () => {
        this.setState({
            notifikasi: [],
            limit: 10,
            offset: 0,
            isNoMoreData: false
        }, () => {
            this.props.getNotifikasi({
                id_anggota: this.props.login.id,
                limit: this.state.limit,
                offset: this.state.offset
            })
        })
    }

    handleLoadMore = () => {
        if(!this.state.isNoMoreData) {
            this.setState(prevState => ({
                offset: prevState.offset + prevState.limit,
                isInfinityScroll: true
            }), () => this.props.getNotifikasi({
                id_anggota: this.props.login.id,
                limit: this.state.limit,
                offset: this.state.offset
            }));
        }
    }

    renderItem = (item, index) => {
        return (
            <TouchableOpacity onPress={() => {}} style={{  }}>
                <View style={{ flexDirection: 'row', width: width }}>
                    <View style={{ width: 50, height: 100, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ borderLeftColor: BORDER_COLOR, borderLeftWidth: 0.5, height: 35 }} />
                        <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: DISABLED_COLOR, justifyContent: 'center', alignItems: 'center' }}>
                            <Feather name="message-circle" size={16} />
                        </View>
                        <View style={{ borderLeftColor: BORDER_COLOR, borderLeftWidth: 0.5, height: 35 }} />
                    </View>
                    <View style={{ width: (width - 50) - 20, height: 100, paddingHorizontal: 16, paddingVertical: 16 }}>
                        <Text style={{ fontSize: 18, fontWeight: '800' }}>{item.title}</Text>
                        <Text style={{ fontSize: 14, fontWeight: '300', textAlign: "justify" }} textBreakStrategy="highQuality">{item.message}</Text>
                        <View style={{ flexDirection: 'column', marginTop: 16 }}>
                            <Text style={{ fontSize: 10, fontWeight: '400', color: '#cccccc' }}>{moment(item.created_at).format("LLLL")}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderSeparator = () => {
        return (
            <View style={{ width: '100%', borderBottomColor: BORDER_COLOR, borderBottomWidth: 0.5 }} />
        )
    }

    renderEmptyComponent = () => {
        return (
            this.state.isLoading && this.state.notifikasi.length === 0 ? 
            _.map([1,2,3], (v, k) => (
                <View key={k} style={{ flexDirection: 'row', width: width }}>
                    <View style={{ width: 50, height: 100, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ borderLeftColor: BORDER_COLOR, borderLeftWidth: 0.5, height: 35 }} />
                        <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: DISABLED_COLOR, justifyContent: 'center', alignItems: 'center' }}>
                            <Feather name="message-circle" size={16} />
                        </View>
                        <View style={{ borderLeftColor: BORDER_COLOR, borderLeftWidth: 0.5, height: 35 }} />
                    </View>
                    <View style={{ width: (width - 50) - 20, height: 100, paddingHorizontal: 16, paddingVertical: 16 }}>
                        <View style={{ height: 18, width: '80%', backgroundColor: DISABLED_COLOR, marginBottom: 10, borderRadius: 8 }} />
                        <View style={{ height: 14, width: '100%', backgroundColor: DISABLED_COLOR, borderRadius: 7 }} />
                        <View style={{ height: 10, marginTop: 16, backgroundColor: DISABLED_COLOR, borderRadius: 5 }} />
                    </View>
                </View>
            ))
            : <View />
        )
    }

    renderFooterComponent = () => {
        if (!this.state.isInfinityScroll) return null;
        return (
            <View
                style={{
                    position: 'relative',
                    width: width,
                    paddingVertical: 10,
                    borderTopWidth: 0.5,
                    borderColor: BORDER_COLOR,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Text style={{ fontSize: 12, color: TEXT_COLOR }}>Sedang memuat notifikasi ...</Text>
            </View>
          );
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.notifikasi}
                    keyExtractor={item => item.notif_id.toString()}
                    // ItemSeparatorComponent={() => this.renderSeparator()}
                    ListEmptyComponent={() => this.renderEmptyComponent()}
                    // ListFooterComponent={() => this.renderFooterComponent()}
                    renderItem={({item, index}) => this.renderItem(item, index) }
                    refreshControl={<RefreshControl
                            refreshing={this.state.isRefresh}
                            onRefresh={this.onRefresh}
                            colors={[DARK_PRIMARY_COLOR]}
                        />
                    }
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={this.state.limit}
                    />
                { this.state.showInfoNoMoreData && 
                <View style={{ position: 'absolute', bottom: 0, width: width, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: ERROR_COLOR }}>
                    <Text style={{ fontSize: 10, color: WHITE_COLOR }}>Tidak ada lagi notifikasi untuk ditampilkan</Text>
                </View> }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

const mapStateToProps = ({ notifikasiReducer, loginReducer }) => {
    const { isLogin, login } = loginReducer;
    const { loading, notif } = notifikasiReducer;
    return { loading, notif, isLogin, login }
}

export default connect(mapStateToProps, {
    getNotifikasi
})(Notification);