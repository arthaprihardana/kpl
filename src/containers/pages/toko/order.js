/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 21:17:34
 */
import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, StyleSheet, TouchableOpacity, Modal, TextInput, Platform, Alert } from 'react-native';
import _ from "lodash";
import { connect } from "react-redux";
import Contacts from 'react-native-contacts';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import { DARK_PRIMARY_COLOR, BORDER_COLOR, WHITE_COLOR, DISABLED_COLOR, ICON_COLOR, PLACEHOLDER_COLOR, TEXT_COLOR } from '../../../assets/colors';
import { Button, InputText, ModalResponse } from '../../component'
import { rupiah } from '../../../helpers';
import { postOrderToko, cleanCart } from '../../../actions';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get("window");

class Order extends Component {

    state = {
        cart: this.props.cart,
        forms: {
            id_anggota: this.props.isLogin ? this.props.login.id : "",
            nama_anggota: this.props.isLogin ? this.props.login.nama : "",
            telepon: "",
            ekstensi: "-",
            cart: this.props.cart,
            total: null
        },
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null,
        kontak: [],
        searchKontak: [],
        multiKontak: false,
        dataMultiKontak: [],
        modalContact: false
    }

    componentDidMount() {
        const { forms } = this.state;
        let total = this.totalHarga(this.props.cart);
        forms.total = total;
        this.setState({ forms })   
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.order !== prevProps.order) {
            return { order: this.props.order }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.order) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.order.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.order.status ? "Selamat, pemesanan di toko sedang diproses" : "Terjadi kesalahan dalam pemesanan",
                    responseAction: () => {
                        this.props.cleanCart();
                        this.setState({isResponse : false });
                        Actions.popTo("toko");
                    }
                });
            }
        }
    }
    
    handleOrder = () => {
        this.props.postOrderToko(this.state.forms);
    }

    totalHarga = data => {
        let total =  _.reduce(data, (sum, n) => {
            return (n.harga_barang * parseInt(n.qty)) + sum;
        }, 0);
        return total;
    }

    requestContactPermission = async () => {
        try {
            if(Platform.OS === "android") {
                let Permissions = new AndroidPermissions();
                let reqContact = await Permissions.requestReadContactPermission();
                return reqContact;
            }
        } catch (err) {
            console.warn(err);
        }
    }

    onChange = (key, value, index = 0) => {
        let forms = this.state.forms;
        forms[key] = value;
        this.setState({ forms }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: !_.isEmpty(value) && val === undefined }))
        });
    }

    onValidate = async key => {
        let except = ['id_anggota', 'ekstensi', 'cart', 'total'];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    onOpenContact = () => {
        if(this.state.kontak.length > 0) {
            this.setState({
                searchKontak: this.state.kontak,
                modalContact: !this.state.modalContact
            });
        } else {
            let req = this.requestContactPermission();
            req.then(value => {
                if(value === "granted") {
                    Contacts.getAll((err, contacts) => {
                        // contacts returned in Array
                        let kontak = _.filter(contacts, v => v.phoneNumbers.length > 0);
                        this.setState({
                            kontak: kontak,
                            searchKontak: kontak,
                            modalContact: !this.state.modalContact
                        })
                    })
                } else {
                    Alert.alert(
                        'Info',
                        'Anda tidak diizinkan untuk dapat mengakses kontak',
                        [
                            {text: 'Tanya Nanti', onPress: () => console.log('Ask me later pressed')},
                            {
                                text: 'Batal',
                                onPress: () => this.onOpenContact(),
                                style: 'cancel',
                            },
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        {cancelable: false},
                    );
                }
            });
        }
    }

    onContactSearch = text => {
        let reg = new RegExp(text.toLowerCase(), "g");
        let s = _.filter(this.state.kontak, v => reg.test(v.givenName.toLowerCase()));
        this.setState({ searchKontak: text.length > 0 ? s : this.state.kontak });
    }

    onContactSelected = value => {
        if(value.phoneNumbers.length > 1) {
            this.setState({
                multiKontak: true,
                dataMultiKontak: value
            })
        } else {
            this.onChange("telepon", value.phoneNumbers[0].number.replace(/\s+|-/g, ''))
            this.setState({ modalContact: !this.state.modalContact });
        }
    }

    onContactMultiSelected = value => {
        this.onChange("telepon", value.replace(/\s+|-/g, ''))
        this.setState({
            modalContact: !this.state.modalContact,
            multiKontak: false,
            dataMultiKontak: [],
            searchKontak: this.state.kontak
        });
    }

    onRequestContactClose = () => {
        this.setState({
            multiKontak: false,
            modalContact: !this.state.modalContact
        })
    }

    render() {
        const { forms } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={{ width: width, marginBottom: 56 }}>
                    <View style={{ width: width, minHeight: 200, paddingHorizontal: 16, paddingVertical: 16 }}>
                        <View style={styles.blockTitle}>
                            <Text style={{ fontWeight: '500' }}>Info Pemesan</Text>
                        </View>
                        <View style={styles.blockContent}>
                            <View>
                                <InputText 
                                    nativeID="nama_anggota"
                                    refs={ref => this._nama_anggota = ref}
                                    placeholder="Nama Anggota"
                                    keyboardType="default"
                                    value={forms.nama_anggota}
                                    editable={false}
                                    onChangeText={text => this.onChange("nama_anggota", text)} />
                            </View>
                            <View>
                                <InputText 
                                    nativeID="telepon"
                                    refs={ref => this._telepon = ref}
                                    placeholder="Telepon"
                                    keyboardType="phone-pad"
                                    value={forms.telepon}
                                    onChangeText={text => this.onChange("telepon", text)}
                                    style={{ paddingRight: 60 }} />
                                <TouchableOpacity onPress={() => this.onOpenContact()} style={{ position: 'absolute', right: 20, top: 24 }}>
                                    <FontAwesome name="address-book" size={20} color={ICON_COLOR} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{ width: width, paddingHorizontal: 16, borderBottomWidth: .5, borderBottomColor: BORDER_COLOR }}>
                        <Text style={{ fontSize: 18, fontWeight: '500' }}>Detail Pembelian</Text>
                    </View>
                    {_.map(this.state.cart, (item, index) => {
                        var image = item.image !== null ? { uri: item.image } : require('../../../assets/image/image_default.png');
                        return (
                            <View key={index} style={{ flexDirection: 'row', width: width, paddingHorizontal: 16, paddingVertical: 16 }}>
                                <View style={{ width: 100, height: 100 }}>
                                    <Image source={image} style={{ width: 100, height: 100 }} resizeMode="cover" />
                                </View>
                                <View style={{ width: width - 140, minHeight: 100, paddingHorizontal: 16, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                    <Text style={{ fontSize: 18, fontWeight: '300', }}>{ item.nama_barang }</Text>
                                    <Text style={{ fontSize: 14, fontWeight: '300', }}>Harga : {rupiah(item.harga_barang)}</Text>
                                    <Text style={{ fontSize: 14, fontWeight: '300', }}>Qty : { item.qty }</Text>
                                    <View style={{ width: '100%', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR, marginTop: 5, marginBottom: 5 }} />
                                    <Text style={{ fontSize: 18, fontWeight: '500', color: DARK_PRIMARY_COLOR }}>Sub Total : {rupiah(item.harga_barang * item.qty)}</Text>
                                </View>
                            </View>
                        )
                    })}
                </ScrollView>
                <View style={{ width: width, height: 56, position: 'absolute', bottom: 0, left: 0, backgroundColor: WHITE_COLOR, flexDirection: 'row', justifyContent: 'space-between', borderTopColor: BORDER_COLOR, borderTopWidth: .5 }}>
                    <View style={{ height: 56, justifyContent: 'center', paddingHorizontal: 16 }}>
                        <Text>Total Harga</Text>
                        <Text style={{ fontSize: 18, fontWeight: '500', color: DARK_PRIMARY_COLOR }}>{rupiah(this.totalHarga(this.props.cart))}</Text>
                    </View>
                    <View style={{ height: 56, justifyContent: 'center', paddingHorizontal: 16 }}>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            small
                            label="Pesan"
                            onPress={() => this.handleOrder()}
                            isLoading={this.props.loading} />
                    </View>
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalContact}
                    onRequestClose={() => this.onRequestContactClose()}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalHeader}>
                            <TouchableOpacity style={styles.closeButton} onPress={() => this.onRequestContactClose()}>
                                <Feather name="x" size={22} color={ICON_COLOR} />
                            </TouchableOpacity>
                            <View style={styles.modalHeaderTitle}>
                                <Text style={styles.headerTitle}>Kontak</Text>
                            </View>
                        </View>
                        {this.state.multiKontak ? 
                            <View>
                                <TouchableOpacity onPress={() => this.setState({ multiKontak: false })} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 16, flexDirection: 'row' }}>
                                    <Feather name="arrow-left" size={16} />
                                    <Text>Kembali</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }}>
                                    <Text>Pilih No Telepon dari Kontak : {this.state.dataMultiKontak.givenName} {this.state.dataMultiKontak.familyName}</Text>
                                </View>
                                <ScrollView style={{ marginBottom: 220 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                                    {this.state.dataMultiKontak.phoneNumbers && _.map(this.state.dataMultiKontak.phoneNumbers, (v, k) => (
                                        <TouchableOpacity key={k} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }} onPress={() => this.onContactMultiSelected(v.number) }>
                                            <Text>{v.number}</Text>
                                        </TouchableOpacity>
                                    ))}
                                </ScrollView>
                            </View>
                            :
                            <View>
                                <View style={styles.searchContainer}>
                                    <TextInput 
                                        style={styles.searchInput}
                                        placeholder={"Cari Kontak..."}
                                        placeholderTextColor={PLACEHOLDER_COLOR}
                                        onChangeText={ text =>  this.onContactSearch(text) } />
                                    <Feather name="search" size={14} color={ICON_COLOR} style={{ position: 'absolute', right: 30, top: 20 }} />
                                </View>
                                <ScrollView style={{ marginBottom: 220 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                                { this.state.searchKontak && _.map(this.state.searchKontak, (v, k) => (
                                    <TouchableOpacity key={k} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }} onPress={() => this.onContactSelected(v) }>
                                        <Text>{v.givenName} {v.familyName}</Text>
                                    </TouchableOpacity>
                                )) }
                                </ScrollView>
                            </View>
                        }
                    </View>
                </Modal>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    },
    modalHeader: { 
        width: '100%', 
        height: 56, 
        backgroundColor: WHITE_COLOR, 
        borderBottomWidth: .5, 
        borderBottomColor: BORDER_COLOR, 
        flexDirection: 'row' 
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    modalHeaderTitle: { 
        width: 150, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'flex-start' 
    },
    headerTitle: { 
        color: TEXT_COLOR, 
        fontWeight: '600' 
    },
    searchContainer: { 
        width: '100%', 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 16, 
        backgroundColor: DISABLED_COLOR, 
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 1 
    },
    searchInput: {
        width: '100%',
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 25,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        paddingRight: 40
    },
    iconPosition: {
        position: 'absolute', right: 20, top: 15
    }
})

const mapStateToProps = ({ tokoReducer, loginReducer }) => {
    const { loading, cart, order } = tokoReducer; 
    const { isLogin, login } = loginReducer;
    return { loading, cart, order, isLogin, login };
}

export default connect(mapStateToProps, {
    postOrderToko,
    cleanCart
})(Order);
