/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 21:16:45
 */
import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, TouchableOpacity, Alert } from 'react-native';
import _ from "lodash";
import { connect } from "react-redux";
import Feather from 'react-native-vector-icons/Feather';
import { DARK_PRIMARY_COLOR, BORDER_COLOR, WHITE_COLOR, ICON_COLOR, ERROR_COLOR } from '../../../assets/colors';
import { Button } from '../../component'
import { rupiah } from '../../../helpers';
import { deleteFromCart } from '../../../actions';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get("window");

class Keranjang extends Component {

    state = {
        cart: this.props.cart
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.cart !== prevProps.cart) {
            this.setState({ cart: this.props.cart })
        }
    }

    totalHarga = data => {
        let total =  _.reduce(data, (sum, n) => {
            return (n.harga_barang * parseInt(n.qty)) + sum;
        }, 0);
        console.log('total', total);
        return total;
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                { this.state.cart.length > 0 ? 
                <View style={{ flex: 1 }}>
                    <ScrollView style={{ width: width, marginBottom: 56 }}>
                        {_.map(this.state.cart, (item, index) => {
                            var image = item.image !== null ? { uri: item.image } : require('../../../assets/image/image_default.png');
                            return (
                                <View key={index} style={{ flexDirection: 'row', width: width, paddingHorizontal: 16, paddingVertical: 16 }}>
                                    <View style={{ width: 100, height: 100 }}>
                                        <Image source={image} style={{ width: 100, height: 100 }} resizeMode="cover" />
                                    </View>
                                    <View style={{ width: width - 140, minHeight: 100, paddingHorizontal: 16, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                        <Text style={{ fontSize: 18, fontWeight: '300', marginRight: 40 }}>{ item.nama_barang }</Text>
                                        <Text style={{ fontSize: 14, fontWeight: '300', }}>Harga : {rupiah(item.harga_barang)}</Text>
                                        <Text style={{ fontSize: 14, fontWeight: '300', }}>Qty : { item.qty }</Text>
                                        <View style={{ width: '100%', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR, marginTop: 5, marginBottom: 5 }} />
                                        <Text style={{ fontSize: 18, fontWeight: '500', color: DARK_PRIMARY_COLOR }}>Sub Total : {rupiah(item.harga_barang * item.qty)}</Text>
                                        <TouchableOpacity onPress={() => {
                                            Alert.alert(
                                                'Info',
                                                `Apakah anda ingin membatalkan membeli ${item.nama_barang}?`,
                                                [
                                                    {
                                                        text: 'Batal',
                                                        onPress: () => console.log('Cancel Pressed'),
                                                        style: 'cancel',
                                                    },
                                                    {text: 'Ya', onPress: () => this.props.deleteFromCart(item.index) },
                                                ],
                                                {cancelable: false},
                                            );
                                            }} style={{ position: 'absolute', top: 0, right: 6, width: 20, height: 20, justifyContent: 'center', alignItems: 'center'}}>
                                            <Feather name="trash-2" size={18} color={ERROR_COLOR} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        })}
                    </ScrollView>
                    <View style={{ width: width, height: 56, position: 'absolute', bottom: 0, left: 0, backgroundColor: WHITE_COLOR, flexDirection: 'row', justifyContent: 'space-between', borderTopColor: BORDER_COLOR, borderTopWidth: .5 }}>
                        <View style={{ height: 56, justifyContent: 'center', paddingHorizontal: 16 }}>
                            <Text>Total Harga</Text>
                            <Text style={{ fontSize: 18, fontWeight: '500', color: DARK_PRIMARY_COLOR }}>{rupiah(this.totalHarga(this.props.cart))}</Text>
                        </View>
                        <View style={{ height: 56, justifyContent: 'center', paddingHorizontal: 16 }}>
                            <Button 
                                small
                                primary={true} 
                                label="Lanjutkan"
                                onPress={() => Actions.push("order")}
                                isLoading={false} />
                        </View>
                    </View>
                </View>
                :
                <View style={{ width: width, height: height - 56, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require("../../../assets/image/empty-cart.png")} style={{ width: 250 }} resizeMode="contain" />
                </View> }
            </View>
        );
    }
}

const mapStateToProps = ({ tokoReducer }) => {
    const { cart } = tokoReducer; 
    return { cart };
}

export default connect(mapStateToProps, {
    deleteFromCart
})(Keranjang);