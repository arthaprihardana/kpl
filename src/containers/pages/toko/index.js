/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 21:16:26
 */
import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity, FlatList, RefreshControl, Image, Dimensions, LayoutAnimation, Keyboard, ScrollView } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { connect } from "react-redux";
import _ from 'lodash';
import { HeaderElipse } from '../../component'
import { TEXT_COLOR, DISABLED_COLOR, WHITE_COLOR, BORDER_COLOR, ICON_COLOR, DARK_PRIMARY_COLOR, ERROR_COLOR, PLACEHOLDER_COLOR, BADGE_COLOR } from '../../../assets/colors';
import { Actions } from 'react-native-router-flux';
import { getToko, getSearchToko, getMasterKategoriProduk, getProductByCategory } from '../../../actions';
import { rupiah } from '../../../helpers';

const { width, height } = Dimensions.get("window");

class Toko extends Component {

    state = {
        product: [],
        kategori: [{id: "semua", nama: "semua"}],
        limit: 20,
        offset: 0,
        isLoading: true,
        isRefresh: false,
        isInfinityScroll: false,
        isNoMoreData: false,
        showInfoNoMoreData: false,
        isSearch: false,
        search: null,
        activeCategory: 0,
        isFilter: false,
        namaKategori: null
    }

    componentDidMount() {
        this.props.getMasterKategoriProduk();
        this.props.getToko({
            offset: this.state.offset,
            limit: this.state.limit
        })
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.toko !== prevProps.toko) {
            return { toko: this.props.toko }
        }
        if(this.props.kategori !== prevProps.kategori) {
            return { kategori: this.props.kategori }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.toko) {
                if(snapshot.toko.length > 0) {
                    this.setState(prevState => ({
                        product: _.concat(prevState.product, snapshot.toko),
                        isInfinityScroll: true
                    }))
                } else {
                    LayoutAnimation.easeInEaseOut();
                    this.setState(prevState => ({
                        isInfinityScroll: false,
                        offset: prevState.product.length,
                        isNoMoreData: true,
                        showInfoNoMoreData: true
                    }));
                }
            }
            if(snapshot.kategori) {
                this.setState(prevState => ({ kategori: _.concat(prevState.kategori, snapshot.kategori) }))
            }
        }
        if(this.props.loading !== prevProps.loading) {
            this.setState({ isLoading: this.props.loading })
        }
        if(this.state.showInfoNoMoreData !== prevState.showInfoNoMoreData) {
            setTimeout(() => {
                LayoutAnimation.easeInEaseOut();
                this.setState({ showInfoNoMoreData: false })
            }, 2000);
        }
    }

    renderItem = (item, index) => {
        let image = !_.isEmpty(item.apps_gambar_barang) ? { uri: item.apps_gambar_barang } : require('../../../assets/image/image_default.png')
        return (
            <TouchableOpacity key={index} style={{ paddingHorizontal: 6, paddingVertical: 6, width: '50%' }} onPress={() => Actions.push("produk", { item: item })}>
                <View style={{ borderWidth: 0.5, borderColor: BORDER_COLOR, width: '100%', height: 280, borderRadius: 10, backgroundColor: WHITE_COLOR }}>
                    <Image source={image} style={{ width: '100%', height: 180, borderTopLeftRadius: 10, borderTopRightRadius: 10 }} resizeMode="cover" />
                    <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                        <Text style={{ fontSize: 18, fontWeight: '400', textAlign: 'left' }}>{ item.nama.length > 20 ? `${item.nama.slice(0, 20)}...` : item.nama}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                            <Text style={{ fontSize: 15, fontWeight: '800' }}>{rupiah(item.hargajual)}</Text>
                            <Text style={{ fontSize: 10 }}>/{_.lowerCase(item.namasatuan)}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderEmptyComponent = () => {
        return (
            this.state.isLoading && this.state.product.length === 0 ?
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: "wrap" }}>
            {_.map([1,2,3,4], (v, k) => (
                <View key={k} style={{ paddingHorizontal: 6, paddingVertical: 6, width: '50%' }}>
                    <View style={{ borderWidth: 0.5, borderColor: BORDER_COLOR, width: '100%', height: 280, borderRadius: 10, backgroundColor: WHITE_COLOR }}>
                        <View style={{ width: '100%', height: 180, borderTopLeftRadius: 10, borderTopRightRadius: 10, backgroundColor: DISABLED_COLOR }} />
                        <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                            <View style={{ height: 18, width: 120, marginBottom: 10, borderRadius: 9, backgroundColor: DISABLED_COLOR }} />
                            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                                <View style={{ height: 15, width: 90, backgroundColor: DISABLED_COLOR, borderRadius: 7.5 }} />
                            </View>
                        </View>
                    </View>
                </View>
            )) }
            </View> : <View />
        )
    }

    renderHeaderComponent = () => {
        return (
            <ScrollView horizontal style={{ borderBottomWidth: .5, borderBottomColor: BORDER_COLOR }} showsHorizontalScrollIndicator={false} contentContainerStyle={{ height: 56, paddingVertical: 10 }}>
                { this.state.kategori && _.map(this.state.kategori, (v, k) => (
                    <TouchableOpacity key={k} style={{ backgroundColor: BADGE_COLOR, height: 40, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10, marginRight: 10, borderRadius: 20 }}>
                        <Text style={{ fontSize: 12 }}>{_.toLower(v.nama)}</Text>
                    </TouchableOpacity>
                ))}
            </ScrollView>
        )
    }

    renderFooterComponent = () => {
        if (!this.state.isInfinityScroll) return null;
        return (
            <View
                style={{
                    position: 'relative',
                    width: width,
                    paddingVertical: 8,
                    borderTopWidth: 0.5,
                    borderColor: BORDER_COLOR,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: -6
                }}>
                <Text style={{ fontSize: 12, color: TEXT_COLOR }}>Sedang memuat produk ...</Text>
            </View>
          );
    }

    onRefresh = () => {
        this.setState({
            product: [],
            limit: 20,
            offset: 0,
            isNoMoreData: false
        }, () => {
            if(this.state.isFilter) {
                this.props.getProductByCategory({
                    namakategori: this.state.namaKategori,
                    limit: this.state.limit,
                    offset: this.state.offset
                })
            } else {
                this.props.getToko({
                    limit: this.state.limit,
                    offset: this.state.offset
                })
            }
        })
    }

    handleLoadMore = () => {
        if(!this.state.isNoMoreData) {
            this.setState(prevState => ({
                offset: prevState.offset + prevState.limit,
                isInfinityScroll: true
            }), () => {
                if(this.state.isSearch) {
                    this.props.getSearchToko({
                        offset: this.state.offset,
                        limit: this.state.limit,
                        nama: this.state.search
                    });
                } else if(this.state.isFilter) {
                    this.props.getProductByCategory({
                        namakategori: this.state.namaKategori,
                        limit: this.state.limit,
                        offset: this.state.offset
                    })
                } else {
                    this.props.getToko({
                        limit: this.state.limit,
                        offset: this.state.offset
                    })
                }
            });
        }
    }

    handleSearch = () => {
        Keyboard.dismiss();
        this.setState({
            isSearch: true,
            isFilter: false,
            product: [],
            limit: 20,
            offset: 0,
            activeCategory: 0
        }, () => {
            this.props.getSearchToko({
                offset: this.state.offset,
                limit: this.state.limit,
                nama: this.state.search
            });
        });
    }

    handleFilterByCategory = (index, namaKategori) => {
        this.setState({
            product: [],
            limit: 20,
            offset: 0,
            activeCategory: namaKategori === "semua" ? 0 : index,
            isSearch: false,
            isFilter: true,
            search: null,
            namaKategori: namaKategori
        }, () => {
            if(namaKategori === "semua") {
                this.setState({ isFilter: false })
                this.props.getToko({
                    offset: this.state.offset,
                    limit: this.state.limit
                })
            } else {
                this.props.getProductByCategory({
                    namakategori: namaKategori,
                    limit: this.state.limit,
                    offset: this.state.offset
                })
            }
        });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.searchContainer}>
                    {/* <View style={{ width: 40, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <TouchableOpacity onPress={() => Actions.pop()}>
                            <Feather name="home" color={ICON_COLOR} size={22} />
                        </TouchableOpacity>
                    </View> */}
                    <View style={{ width: width - 70 }}>
                        <TextInput 
                            style={styles.searchInput}
                            placeholder={"Cari barang di koperasi lemigas ..."}
                            placeholderTextColor={PLACEHOLDER_COLOR}
                            onChangeText={ text => {
                                this.setState({ search: text })
                                if(text.length === 0) {
                                    this.setState({ 
                                        isSearch: false,
                                        product: [],
                                        limit: 20,
                                        offset: 0,
                                    }, () => {
                                        Keyboard.dismiss();
                                        this.props.getToko({
                                            offset: this.state.offset,
                                            limit: this.state.limit
                                        })
                                    })
                                }
                            }}
                            returnKeyType="search"
                            value={this.state.search}
                            onSubmitEditing={() => this.handleSearch()} />
                        <TouchableOpacity style={{ position: 'absolute', right: 20, top: 13 }} onPress={() => {
                                if(this.state.search.length === 0) {
                                    this.setState({ 
                                        isSearch: false,
                                        product: [],
                                        limit: 20,
                                        offset: 0,
                                    }, () => {
                                        Keyboard.dismiss();
                                        this.props.getToko({
                                            offset: this.state.offset,
                                            limit: this.state.limit
                                        })
                                    })
                                } else {
                                    this.handleSearch()
                                }
                            }}>
                            <Feather name="search" size={14} color={ICON_COLOR} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }} onPress={() => Actions.push("keranjang")}>
                            <Feather name="shopping-cart" color={ICON_COLOR} size={22} />
                            { this.props.cart.length > 0 && 
                            <View style={{ position: 'absolute', top: 5, right: 5, width: 20, height: 20, borderRadius: 10, backgroundColor: ERROR_COLOR, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 10, fontWeight: '400', color: WHITE_COLOR }}>{this.props.cart.length}</Text>
                            </View> }
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ width: width, height: 56 }}>
                    { this.props.loadingKategori ? 
                        <View style={{ flexDirection: 'row', width: width, justifyContent: 'space-between', paddingVertical: 10, height: 56 }}>
                            { [1,2,3].map((v, k) => (
                                <View key={k} style={{ backgroundColor: DISABLED_COLOR, height: 36, width: (width / 3) - 10, borderRadius: 18, marginRight: 5, marginLeft: 5, }} />
                            ))}
                        </View> : 
                        <ScrollView horizontal style={{ borderBottomWidth: .5, borderBottomColor: BORDER_COLOR }} showsHorizontalScrollIndicator={false} contentContainerStyle={{ height: 56, paddingVertical: 10 }}>
                            { this.state.kategori && _.map(this.state.kategori, (v, k) => (
                                <TouchableOpacity key={k} style={{ backgroundColor: this.state.activeCategory === k ? BADGE_COLOR : DISABLED_COLOR, height: 36, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10, marginRight: 5, marginLeft: 5, borderRadius: 18 }} onPress={() => this.handleFilterByCategory(k, v.nama)}>
                                    <Text style={{ fontSize: 12, color: this.state.activeCategory === k ? WHITE_COLOR : '#babdbe' }}>{_.toLower(v.nama)}</Text>
                                </TouchableOpacity>
                            ))}
                        </ScrollView>
                    }
                </View>
                <View style={{ flex: 1 }}>
                    <FlatList 
                        contentContainerStyle={{ paddingHorizontal: 10, paddingVertical: 10, }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.product}
                        keyExtractor={(item, index) => index.toString()}
                        // ItemSeparatorComponent={() => {}}
                        ListEmptyComponent={() => this.renderEmptyComponent()}
                        // ListHeaderComponent={() => this.renderHeaderComponent() }
                        ListFooterComponent={() => this.renderFooterComponent()}
                        renderItem={({item, index}) => this.renderItem(item, index) }
                        refreshControl={<RefreshControl
                                refreshing={this.state.isRefresh}
                                onRefresh={this.onRefresh}
                                colors={[DARK_PRIMARY_COLOR]}
                            />
                        }
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        initialNumToRender={this.state.limit}
                        numColumns={2}
                    />
                </View>
                { this.state.showInfoNoMoreData && 
                <View style={{ position: 'absolute', bottom: 0, width: width, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: ERROR_COLOR }}>
                    <Text style={{ fontSize: 10, color: WHITE_COLOR }}>Tidak ada lagi produk untuk ditampilkan</Text>
                </View> }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    searchContainer: { 
        width: width, 
        height: 56, 
        flexDirection: 'row',
        justifyContent: 'space-between', 
        alignItems: 'center', 
        paddingLeft: 16, 
        paddingRight: 10, 
        backgroundColor: DISABLED_COLOR, 
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 1 
    },
    searchInput: {
        width: '100%',
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 25,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        paddingRight: 40
    },
})

const mapStateToProps = ({ tokoReducer }) => {
    const { loading, toko, cart, kategori, loadingKategori } = tokoReducer
    return { loading, toko, cart, kategori, loadingKategori }
}

export default connect(mapStateToProps, {
    getToko,
    getSearchToko,
    getMasterKategoriProduk,
    getProductByCategory
})(Toko)