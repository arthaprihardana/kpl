/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 21:18:10
 */
import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, TouchableHighlight, TouchableOpacity, StyleSheet, Modal } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import _ from 'lodash';
import { connect } from "react-redux";
import { Button, InputCounter } from '../../component';
import { WHITE_COLOR, BORDER_COLOR, DARK_PRIMARY_COLOR, PRIMARY_COLOR, DISABLED_COLOR, LIGHT_PRIMARY_COLOR, ICON_COLOR } from '../../../assets/colors';
import { Actions } from 'react-native-router-flux';
import { getRelatedProduk, addToCart, cleanCart } from '../../../actions';
import { rupiah } from '../../../helpers';

const { width, height } = Dimensions.get("window");

class Produk extends Component {

    state = {
        produk: [],
        modalVisible: false,
        qty: "1"
    }

    componentDidMount() {
        // this.props.cleanCart()
        this.props.getRelatedProduk({
            id: this.props.item.id,
            namakategori: this.props.item.namakategori
        });
    }

    componentWillUnmount() {
        Actions.refresh();
    }
    
    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.relatedProduk !== prevProps.relatedProduk) {
            return { relatedProduk: this.props.relatedProduk }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.relatedProduk) {
                this.setState({
                    produk: snapshot.relatedProduk
                })
            }
        }
    }

    onModalDetail = visible => {
        this.setState({ modalVisible: visible })
    }

    handleAddToCart = () => {
        this.onModalDetail(false);
        this.props.addToCart({
            nama_barang : this.props.item.nama,	
            id_barang : this.props.item.id,
            harga_barang : this.props.item.hargajual,
            qty : this.state.qty,
            image: this.props.item.apps_gambar_barang
        });
        Actions.popTo("toko");
    }

    render() {
        let image = this.props.item.apps_gambar_barang !== null ? { uri: this.props.item.apps_gambar_barang } : require('../../../assets/image/image_default.png');
        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: WHITE_COLOR }} showsVerticalScrollIndicator={false}>
                    <Image source={image} style={{ width: width, height: (height - 220) }} resizeMode="cover" />
                    <TouchableHighlight onPress={() => this.onModalDetail(true)} style={{ position: 'relative', marginLeft: width - 80, marginTop: -30, backgroundColor: PRIMARY_COLOR, width: 60, height: 60, borderRadius: 30, justifyContent: 'center', alignItems: 'center' }} underlayColor={LIGHT_PRIMARY_COLOR}>
                        {/* <Feather name="plus" size={24} color={WHITE_COLOR} /> */}
                        <MaterialIcons name="add-shopping-cart" size={24} color={WHITE_COLOR} />
                    </TouchableHighlight>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 16, paddingHorizontal: 16, marginTop: -30 }}>
                        <View style={{ width: width - 100 }}>
                            <Text style={{ fontSize: 18, fontWeight: '500', textAlign: 'left', marginBottom: 10 }}>{ this.props.item.nama && this.props.item.nama.length > 35 ? `${this.props.item.nama.slice(0, 35)}...` : this.props.item.nama}</Text>
                            <Text style={{ fontSize: 18, fontWeight: '500', color: DARK_PRIMARY_COLOR }}>{rupiah(this.props.item.hargajual)}</Text>
                        </View>
                    </View>
                    <View style={{ width: width, paddingVertical: 16, paddingHorizontal: 16, borderTopWidth: 0.5, borderTopColor: BORDER_COLOR }}>
                        {/* <Text style={{ fontSize: 12, fontWeight: '400' }}>Jumlah Stok saat ini > 50</Text> */}
                        <Text style={{ fontSize: 12, fontWeight: '400' }}>Kategori : {this.props.item.namakategori}</Text>
                    </View>
                    {/* <View style={{ width: width, paddingVertical: 16, paddingHorizontal: 16 }}>
                        <Text style={{ fontSize: 16, fontWeight: '500', marginBottom: 16 }}>Deskripsi Produk</Text>
                        <Text style={{ textAlign: 'justify' }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
                    </View> */}

                    <View style={{ width: width, paddingBottom: 16 }}>
                        <View style={{ paddingVertical: 16, paddingHorizontal: 16 }}>
                            <Text style={{ fontSize: 16, fontWeight: '500', marginBottom: 10 }}>Produk Lainnya</Text>
                        </View>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={{ paddingHorizontal: 10 }}>
                            { this.props.loading ? 
                            _.map([1,2,3], (value, index) => (
                                <View key={index} style={{ paddingHorizontal: 6, paddingVertical: 6, width: 180 }}>
                                    <View style={{ borderWidth: 0.5, borderColor: BORDER_COLOR, width: '100%', height: 250, borderRadius: 10, backgroundColor: WHITE_COLOR }}>
                                        <View style={{ width: '100%', height: 170, borderTopLeftRadius: 10, borderTopRightRadius: 10, backgroundColor: DISABLED_COLOR }} />
                                        <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                                            <View style={{ height: 14, width: 120, backgroundColor: DISABLED_COLOR, borderRadius: 7 }} />
                                            <View style={{ position: 'absolute', width: '100%', left: 16, bottom: 0, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                                <View style={{ height: 12, width: 90, backgroundColor: DISABLED_COLOR, borderRadius: 6 }} />
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            )) :
                            _.map(this.state.produk, (value, index) => {
                                var img = value.apps_gambar_barang !== null ? { uri: value.apps_gambar_barang } : require('../../../assets/image/image_default.png');
                                return (
                                <TouchableOpacity key={index} style={{ paddingHorizontal: 6, paddingVertical: 6, width: 180 }} onPress={() => Actions.push("produk", { item: value })}>
                                    <View style={{ borderWidth: 0.5, borderColor: BORDER_COLOR, width: '100%', height: 250, borderRadius: 10, backgroundColor: WHITE_COLOR }}>
                                        <Image source={img} style={{ width: '100%', height: 170, borderTopLeftRadius: 10, borderTopRightRadius: 10 }} resizeMode="cover" />
                                        <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                                            <Text style={{ fontSize: 14, fontWeight: '400', textAlign: 'justify' }}>{value.nama.length > 35 ? `${value.nama.slice(0, 35)}...` : value.nama}</Text>
                                            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                                                <Text style={{ fontSize: 15, fontWeight: '800' }}>{rupiah(value.hargajual)}</Text>
                                                <Text style={{ fontSize: 10 }}>/{_.lowerCase(value.namasatuan)}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                    </View>
                </ScrollView>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.onModalDetail(false)}>
                    <View style={{flex: 1}}>
                        <View style={{ height: 56, width: width, backgroundColor: WHITE_COLOR, elevation: 2, flexDirection: 'row' }}>
                            <View style={{ width: 56, height: 56 }}>
                                <TouchableOpacity onPress={() => this.onModalDetail(false)} style={{ width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }}>
                                    <Feather name="x" color={ICON_COLOR} size={20} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: width - 56, height: 56, justifyContent: 'center' }}>
                                <Text style={{ fontSize: 18 }}>Detail Produk</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', width: width, paddingHorizontal: 16, paddingVertical: 16 }}>
                            <View style={{ width: 100, height: 100 }}>
                                <Image source={image} style={{ width: 100, height: 100 }} resizeMode="cover" />
                            </View>
                            <View style={{ width: width - 100, height: 100, paddingHorizontal: 16, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                <Text style={{ fontSize: 18, fontWeight: '500', }}>{ this.props.item.nama }</Text>
                                <Text style={{ fontSize: 18, fontWeight: '500', color: DARK_PRIMARY_COLOR }}>{rupiah(this.props.item.hargajual)}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 10, borderTopWidth: 0.5, borderTopColor: BORDER_COLOR }}>
                            <View style={{ height: 80, justifyContent: 'center' }}>
                                <Text>Jumlah</Text>
                            </View>

                            <View style={{ width: '35%', height: 80, justifyContent: 'center' }}>
                                <InputCounter 
                                    nativeID="qty"
                                    refs={ref => this._qty = ref}
                                    placeholder="Quantity"
                                    keyboardType="phone-pad"
                                    maxLength={2}
                                    minValue={1}
                                    value={this.state.qty}
                                    onChangeText={text => this.setState({ qty: text })}
                                    onSub={text => this.setState({ qty: text })}
                                    onAdd={text => this.setState({ qty: text })} 
                                    />
                            </View>
                        </View>

                        <View style={{ width: width, height: 56, position: 'absolute', bottom: 0, left: 0, backgroundColor: WHITE_COLOR, flexDirection: 'row', justifyContent: 'space-between', borderTopColor: BORDER_COLOR, borderTopWidth: .5 }}>
                            <View style={{ height: 56, justifyContent: 'center', paddingHorizontal: 16 }}>
                                <Text>Total Harga</Text>
                                <Text style={{ fontSize: 18, fontWeight: '500', color: DARK_PRIMARY_COLOR }}>{rupiah(this.props.item.hargajual * this.state.qty)}</Text>
                            </View>
                            <View style={{ height: 56, justifyContent: 'center', paddingHorizontal: 16 }}>
                                <Button 
                                    small
                                    primary={true} 
                                    label="Tambah Ke Keranjang"
                                    onPress={() => this.handleAddToCart()}
                                    isLoading={false} />
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const mapStateToProps = ({ tokoReducer }) => {
    const { loading, relatedProduk } = tokoReducer;
    return { loading, relatedProduk }
}

export default connect(mapStateToProps, {
    getRelatedProduk,
    addToCart,
    cleanCart
})(Produk)