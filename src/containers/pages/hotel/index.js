/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:07:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-06 11:51:31
 */
import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Keyboard, Text } from 'react-native';
import { connect } from "react-redux";
import _ from "lodash";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { InputText, Button, Picker, DatePicker, HeaderElipse, ModalResponse } from '../../component';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, BORDER_COLOR } from '../../../assets/colors';
import { getKota, postOrderHotel, clearPrevTiket } from '../../../actions'
import { Actions } from 'react-native-router-flux';

class Hotel extends Component {

    state = {
        forms: {
            kota: "",
            nama_hotel: "",
            check_in: "",
            check_out: "",
            tamu: "",
            rooms: "",
            nama_anggota: this.props.isLogin ? this.props.login.nama : "",
            id_anggota: this.props.isLogin ? this.props.login.id : ""
        },
        errors: {
            err_kota: null,
            err_nama_hotel: null,
            err_check_in: null,
            err_check_out: null,
            err_tamu: null,
            err_rooms: null,
            err_nama_anggota: null,
            err_id_anggota: null
        },
        masterKota: [],
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null
    }

    componentDidMount() {
        this.props.getKota()
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.kota !== prevProps.kota) {
            return { kota: this.props.kota }
        }
        if(this.props.orderHotel !== prevProps.orderHotel) {
            return { orderHotel: this.props.orderHotel }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.kota) {
                this.setState({
                    masterKota: snapshot.kota
                })
            }
            if(snapshot.orderHotel) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.orderHotel.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.orderHotel.status ? "Selamat, pemesanan hotel sedang diproses" : "Terjadi kesalahan dalam pemesanan",
                    responseAction: () => {
                        this.props.clearPrevTiket();
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
        }
    }

    onChange = (key, value) => {
        let forms = this.state.forms;
        let errors = this.state.errors;
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms });
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: val === undefined }))
        });
    }

    onSubmit = () => {
        const { forms } = this.state;
        Keyboard.dismiss();
        this.props.postOrderHotel({
            kota: forms.kota.nama,
            nama_hotel: forms.nama_hotel,
            check_in: forms.check_in,
            check_out: forms.check_out,
            tamu: forms.tamu,
            rooms: forms.rooms,
            nama_anggota: forms.nama_anggota,
            id_anggota: forms.id_anggota
        });
    }

    onValidate = async key => {
        let except = ["id_anggota", "nama_anggota"];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    render() {
        const { forms } = this.state;
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                {/* <HeaderElipse /> */}
                <View style={styles.container}>
                    {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                        <View style={{ width: 110, height: 110, borderRadius: 55, justifyContent: 'center', alignItems: 'center', backgroundColor: DARK_PRIMARY_COLOR }}>
                            <FontAwesome name="bed" size={60} color={WHITE_COLOR} />
                        </View>
                    </View> */}
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pemesan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="nama_anggota"
                                refs={ref => this._nama_anggota = ref}
                                placeholder="Nama Anggota"
                                keyboardType="default"
                                value={forms.nama_anggota}
                                onChangeText={text => this.onChange("nama_anggota", text)} />
                        </View>
                    </View>

                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Menginap</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <Picker 
                                nativeID="kota"
                                refs={ref => this._kota = ref}
                                placeholder="Kota"
                                data={this.state.masterKota}
                                isLoading={this.props.loading}
                                isSearchable={true}
                                value={this.state.forms.kota.nama}
                                callback={value => this.onChange("kota", value)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="nama_hotel"
                                refs={ref => this._nama_hotel = ref}
                                keyboardType="default"
                                placeholder="Nama Hotel"
                                onChangeText={text => this.onChange("nama_hotel", text)} />
                        </View>
                        <View>
                            <DatePicker 
                                nativeID="check_in"
                                refs={ref => this._check_in = ref}
                                placeholder="Tanggal Check In"
                                hasMinDate={true}
                                value={this.state.forms.check_in}
                                callback={date => this.onChange("check_in", date.format("YYYY-MM-DD"))} />
                        </View>
                        <View>
                            <DatePicker 
                                nativeID="check_out"
                                refs={ref => this._check_out = ref}
                                placeholder="Tanggal Check Out"
                                hasMinDate={true}
                                minDate={this.state.forms.check_in !== "" ? new Date(this.state.forms.check_in) : new Date() }
                                value={this.state.forms.check_out}
                                callback={date => this.onChange("check_out", date.format("YYYY-MM-DD"))} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="tamu"
                                refs={ref => this._tamu = ref}
                                placeholder="Jumlah Tamu"
                                keyboardType="phone-pad"
                                maxLength={2}
                                onChangeText={text => this.onChange("tamu", text)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="rooms"
                                refs={ref => this._rooms = ref}
                                placeholder="Jumlah Kamar"
                                keyboardType="phone-pad"
                                maxLength={2}
                                onChangeText={text => this.onChange("rooms", text)} />
                        </View>
                    </View>
                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Pesan"
                            onPress={() => this.onSubmit()}
                            isLoading={this.props.orderLoader} />
                    </View>
                </View>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    }
})

const mapStateToProps = ({ kotaReducer, tiketReducer, loginReducer }) => {
    const { loading, kota } = kotaReducer;
    const { orderHotel } = tiketReducer;
    const { isLogin, login } = loginReducer;
    const orderLoader = tiketReducer.loading;
    return { loading, kota, orderHotel, isLogin, login, orderLoader }
}

export default connect(mapStateToProps, {
    getKota,
    postOrderHotel,
    clearPrevTiket
})(Hotel);