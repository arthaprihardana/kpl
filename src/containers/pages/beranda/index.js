/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-04 07:50:52
 */
import React, { Component } from 'react';
import { View, Text, ScrollView, Image, Dimensions, TouchableOpacity, RefreshControl, Platform, Alert } from 'react-native';
import { connect } from "react-redux";
import { TEXT_COLOR, DISABLED_COLOR, WHITE_COLOR, DARK_PRIMARY_COLOR, PRIMARY_COLOR, BORDER_COLOR } from '../../../assets/colors';
import _ from 'lodash';
import firebase from 'react-native-firebase';
import { getLayanan, getBannerPromo, networkStatus, getToko, getSaldo } from '../../../actions';
import { Banner, HeaderElipse } from '../../component';
import { rupiah } from '../../../helpers';
import { Actions } from 'react-native-router-flux';

const {height, width} = Dimensions.get('window');

class Beranda extends Component {

    state = {
        layanan: [],
        refreshing: false,
        toko: []
    }
    
    componentDidMount() {
        firebase.messaging().subscribeToTopic("broadcast");
        console.log('is Connected', this.props.isConnected);
        
        if(this.props.isConnected) {
            this.props.getLayanan();
            this.props.getToko({
                offset: 0,
                limit: 5
            });
            if(this.props.isLogin) {
                this.props.getSaldo({ id_anggota: this.props.login.id })
            }
        }
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.isConnected !== prevProps.isConnected) {
            return { isConnected: this.props.isConnected }
        }
        if(this.props.layanan !== prevProps.layanan) {
            return { layanan: this.props.layanan }
        }
        if(this.props.toko !== prevProps.toko) {
            return { toko: this.props.toko }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.isConnected) {
                this.props.getLayanan();
                this.props.getToko({
                    offset: 0,
                    limit: 5
                });
                if(this.props.isLogin) {
                    this.props.getSaldo({ id_anggota: this.props.login.id })
                }
            }
            if(snapshot.layanan) {
                this.setState({ layanan: snapshot.layanan });
            }
            if(snapshot.toko) {
                this.setState({ toko: _.slice(snapshot.toko, 0, 5) });
            }
        }
    }

    onRefresh = () => {
        this.props.getBannerPromo();
        this.props.getLayanan();
        if(this.props.isLogin) {
            this.props.getSaldo({ id_anggota: this.props.login.id })
        }
    }

    render() {
        return (
            <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always" refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this.onRefresh}
                  colors={[DARK_PRIMARY_COLOR]}
                />}>
                <HeaderElipse />
                <View style={{ flex: 1, justifyContent: "flex-start" }}>
                    <View style={{ height: 160 }}>
                        <Banner />
                    </View>
                    { this.props.isLogin && 
                    <View style={{ paddingHorizontal: 16 }}>
                        <View style={{ paddingVertical: 8, paddingHorizontal: 16, borderWidth: 0.2, borderColor: BORDER_COLOR, height: 50, borderRadius: 10, backgroundColor: WHITE_COLOR, elevation: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                            { this.props.saldoLoader ? 
                                [1,2,3].map((v, k) => (
                                    <View key={k}>
                                        <View style={{ height: 12, width: 50, marginBottom: 5, borderRadius: 7, backgroundColor: DISABLED_COLOR }} />
                                        <View style={{ height: 12, width: 50, marginBottom: 5, borderRadius: 7, backgroundColor: DISABLED_COLOR }} />
                                    </View>
                                )) : 
                                this.props.saldo && Object.keys(this.props.saldo[0]).map((v, k) => {
                                    let name = "";
                                    switch (k) {
                                        case 1:
                                            name = "Saldo Terpakai"
                                            break;
                                        case 2:
                                            name = "Saldo Sisa"
                                            break;
                                        default:
                                            name = "Total Saldo"
                                            break;
                                    }
                                    return (
                                        <View key={k}>
                                            <Text style={{ fontSize: 12 }}>{name}</Text>
                                            <Text style={{ fontSize: 12, fontWeight: '500' }}>{rupiah(parseInt(this.props.saldo[0][v]))}</Text>
                                        </View>
                                    )
                                })
                            }
                        </View>
                    </View>
                    }

                    <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                        <Text style={{ fontSize: 16, color: TEXT_COLOR, marginBottom: 10, fontWeight: '500' }}>Layanan Kami</Text>
                        <View style={{ flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between'}}>
                            { this.state.layanan.length === 0 || this.props.loading ? 
                                _.map([1,2,3,4], (v, k) => (
                                    <View key={k} style={{ width: (width / 4) - 10, height: (width / 4) - 10, justifyContent: 'center', alignItems: 'center', borderRadius: 10, marginBottom: 10, backgroundColor: DISABLED_COLOR }} />
                                )) :
                                _.map(this.state.layanan, (v, k) => (
                                    <TouchableOpacity key={k} style={{ width: (width / 4) - 10, height: (width / 4) - 10, borderWidth: 1, borderColor: '#ccc', justifyContent: 'center', alignItems: 'center', borderRadius: 10, marginBottom: 10, backgroundColor: '#FFFFFF' }} onPress={() => {
                                        switch (v.id_layanan) {
                                            case 1:
                                                Actions.push("ticketing");
                                                break;
                                            case 2:
                                                if(this.props.isLogin) {
                                                    Actions.push("toko");
                                                } else {
                                                    Alert.alert(
                                                        'Info',
                                                        'Untuk dapat mengakses menu Toko. Anda harus login terlebih dahulu. Apakah Anda akan login sekarang ?',
                                                        [
                                                            {
                                                                text: 'Nanti',
                                                                onPress: () => console.log('Cancel Pressed'),
                                                                style: 'cancel',
                                                            },
                                                            {text: 'Ya', onPress: () => Actions.push('login')},
                                                        ],
                                                        {cancelable: false},
                                                    );
                                                }
                                                break;
                                            case 3:
                                                if(this.props.isLogin) {
                                                    Actions.push("simpanpinjam");
                                                } else {
                                                    Alert.alert(
                                                        'Info',
                                                        'Untuk dapat mengakses menu Simpan Pinjam. Anda harus login terlebih dahulu. Apakah Anda akan login sekarang ?',
                                                        [
                                                            {
                                                                text: 'Nanti',
                                                                onPress: () => console.log('Cancel Pressed'),
                                                                style: 'cancel',
                                                            },
                                                            {text: 'Ya', onPress: () => Actions.push('login')},
                                                        ],
                                                        {cancelable: false},
                                                    );
                                                }
                                                break;
                                            case 4:
                                                if(this.props.isLogin) {
                                                    Actions.push("serbausaha");
                                                } else {
                                                    Alert.alert(
                                                        'Info',
                                                        'Untuk dapat mengakses menu Serba Usaha. Anda harus login terlebih dahulu. Apakah Anda akan login sekarang ?',
                                                        [
                                                            {
                                                                text: 'Nanti',
                                                                onPress: () => console.log('Cancel Pressed'),
                                                                style: 'cancel',
                                                            },
                                                            {text: 'Ya', onPress: () => Actions.push('login')},
                                                        ],
                                                        {cancelable: false},
                                                    );
                                                }
                                                break;
                                            case 5:
                                                Actions.push("topup")
                                                break;
                                            case 6:
                                                Actions.push("gedung", { title: v.nama_layanan });
                                                break;
                                            case 7:
                                                Alert.alert(
                                                    'Informasi',
                                                    'Fitur ini akan segera hadir',
                                                    [
                                                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                                                    ],
                                                    {cancelable: false},
                                                );
                                                break;
                                            case 8:
                                                if(this.props.isLogin) {
                                                    Actions.push("dashboard")
                                                } else {
                                                    Alert.alert(
                                                        'Info',
                                                        'Untuk dapat mengakses menu Toko. Anda harus login terlebih dahulu. Apakah Anda akan login sekarang ?',
                                                        [
                                                            {
                                                                text: 'Nanti',
                                                                onPress: () => console.log('Cancel Pressed'),
                                                                style: 'cancel',
                                                            },
                                                            {text: 'Ya', onPress: () => Actions.push('login')},
                                                        ],
                                                        {cancelable: false},
                                                    );
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }}>
                                        <Image source={{ uri: v.icon_layanan }} style={{  width: (width / 4) - 10, height: (width / 4) - 10, borderRadius: 10 }} resizeMode="cover" />
                                        <View style={{ position: 'absolute', bottom: 0, width: '100%', height: 25, backgroundColor: 'rgba(0,0,0,0.25)', justifyContent: 'center', alignItems: 'center', borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}>
                                            <Text style={{ fontSize: 10 , flexWrap: "wrap", color: WHITE_COLOR, fontWeight: '600', textAlign: 'center'}}>{v.nama_layanan}</Text>
                                        </View>
                                    </TouchableOpacity>
                                ))
                            }
                        </View>
                    </View>

                    <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItem: 'flex-end', height: 30, paddingRight: 6, borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR, marginBottom: 10 }}>
                            <Text style={{ fontSize: 16, color: TEXT_COLOR, fontWeight: '500' }}>Toko</Text>
                            { this.state.toko.length === 0 || this.props.tokoLoader ? <View style={{ height: 14, width: 100, marginBottom: 5, borderRadius: 7, backgroundColor: DISABLED_COLOR }} /> :
                            <TouchableOpacity onPress={() => {
                                if(this.props.isLogin) {
                                    Actions.push("toko")
                                } else {
                                    Alert.alert(
                                        'Info',
                                        'Untuk dapat mengakses menu Toko. Anda harus login terlebih dahulu. Apakah Anda akan login sekarang ?',
                                        [
                                            {
                                                text: 'Nanti',
                                                onPress: () => console.log('Cancel Pressed'),
                                                style: 'cancel',
                                            },
                                            {text: 'Ya', onPress: () => Actions.push('login')},
                                        ],
                                        {cancelable: false},
                                    );
                                }
                            }}><Text style={{ fontSize: 11, color: PRIMARY_COLOR }}>Selengkapnya</Text></TouchableOpacity> }
                        </View>
                        { this.state.toko.length === 0 || this.props.tokoLoader ?
                        _.map([1,2,3], (value, index) => (
                            <View key={index} style={{ flexDirection: 'row', width: width, marginRight: 30, marginBottom: 16 }}>
                                <View style={{ width: (1/4) * width, justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <View style={{ width: (1/4) * width, height: (1/4) * width, borderRadius: 10, backgroundColor: DISABLED_COLOR }} />
                                </View>
                                <View style={{ width: ((3/4) * width) - 20, paddingRight: 20, paddingLeft: 20 }}>
                                    <View style={{ height: 18, width: 200, marginBottom: 10, borderRadius: 9, backgroundColor: DISABLED_COLOR }} />
                                    <View style={{ height: 14, width: 130, marginBottom: 5, borderRadius: 7, backgroundColor: DISABLED_COLOR }} />
                                    <View style={{ height: 14, width: 100, marginBottom: 5, borderRadius: 7, backgroundColor: DISABLED_COLOR }} />
                                </View>
                            </View>
                        )) :
                        _.map(this.state.toko, (value, index) => {
                            let image = value.apps_gambar_barang !== null ? { uri: value.apps_gambar_barang } : require('../../../assets/image/image_default.png')
                            return (
                            <TouchableOpacity key={index} onPress={() => {
                                if(this.props.isLogin) {
                                    Actions.push("produk", { item: value })
                                } else {
                                    Alert.alert(
                                        'Info',
                                        'Untuk dapat mengakses menu Toko. Anda harus login terlebih dahulu. Apakah Anda akan login sekarang ?',
                                        [
                                            {
                                                text: 'Nanti',
                                                onPress: () => console.log('Cancel Pressed'),
                                                style: 'cancel',
                                            },
                                            {text: 'Ya', onPress: () => Actions.push('login')},
                                        ],
                                        {cancelable: false},
                                    );
                                }
                            }} style={{ flexDirection: 'row', width: width, marginRight: 30, marginBottom: 16 }}>
                                <View style={{ width: (1/4) * width, justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Image source={image} style={{ width: (1/4) * width, height: (1/4) * width, borderRadius: 10}} />
                                </View>
                                <View style={{ width: ((3/4) * width) - 20, paddingRight: 20, paddingLeft: 20 }}>
                                    <Text style={{ fontSize: 18, fontWeight: '400' }}>{value.nama}</Text>
                                    <Text style={{ fontSize: 14, fontWeight: '300' }} textBreakStrategy="highQuality">{value.namakategori}</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                                        <Text style={{ fontSize: 15, fontWeight: '800' }}>{rupiah(value.hargajual)}</Text>
                                        <Text style={{ fontSize: 10 }}>/{_.lowerCase(value.namasatuan)}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )})}
                    </View>
                    
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = ({ layananReducer, network, tokoReducer, loginReducer, akunReducer }) => {
    const { loading, layanan } = layananReducer;
    const { isConnected } = network;
    const { toko } = tokoReducer;
    const { isLogin, login } = loginReducer;
    const { saldo } = akunReducer;
    const tokoLoader = tokoReducer.loading;
    const saldoLoader = akunReducer.loading;
    return { loading, layanan, isConnected, toko, tokoLoader, isLogin, saldoLoader, saldo, login };
}

export default connect(mapStateToProps, {
    getLayanan,
    getBannerPromo,
    networkStatus,
    getToko,
    getSaldo
})(Beranda)