/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-12 22:42:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-12 22:48:46
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Calendar } from 'react-native-calendars';
import Feather from 'react-native-vector-icons/Feather';
import _ from 'lodash';
import moment from 'moment';
import 'moment/locale/id';
import { Actions } from 'react-native-router-flux';
import { Button } from '../../component'
import {ERROR_COLOR} from '../../../assets/colors';

class Kalender extends Component {
    
    state = {
        notAvailableDate: {},
        selectedDate: null,
        readyToSubmit: false
    }

    componentDidMount() {
        var selected = {}
        var dates = this.props.bookingDate;
        // var dates = this.sampleNotAvailableDate();
        for(var i = 0; i < dates.length; i++) {
            _.assign(selected, {
                [dates[i]]: {selected: true, selectedColor: ERROR_COLOR, disableTouchEvent: true }
            })
        }
        this.setState({
            notAvailableDate: selected
        })
    }
    
    componentWillUnmount() {
        Actions.refresh({ tanggal_book: this.state.selectedDate })
    }

    // sampleNotAvailableDate = () => {
    //     var startOfWeek = moment().startOf('week');
    //     var endOfWeek = moment().endOf('week');

    //     var days = [];
    //     var day = startOfWeek;

    //     while (day <= endOfWeek) {
    //         days.push(day.format('YYYY-MM-DD'));
    //         day = day.clone().add(1, 'd');
    //     }

    //     return days;
    // }

    _renderArrow = direction => {
        if(direction === 'left') {
            // return <Text>{this.state.previousMonth}</Text>
            return <Feather name="chevron-left" size={16} />
        } else {
            // return <Text>{this.state.nextMonth}</Text>
            return <Feather name="chevron-right" size={16} />
        }
    }

    onSelectedDay = day => {
        this.setState(prevState => {
            return {
                selectedDate: { [day.dateString]: { selected: true, selectedColor: 'orange' } },
                readyToSubmit: true
            }
        })
    }

    render() {
        return (
            <View style={{ flex: 1, paddingHorizontal: 16 }}>
                <Calendar
                    // Initially visible month. Default = Date()
                    // current={moment().format('YYYY-MM-DD')}
                    // current={this.state.selectedDate}
                    // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                    minDate={moment().format('YYYY-MM-DD')}
                    // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                    maxDate={moment().add(1, 'month').format('YYYY-MM-DD')}
                    // Handler which gets executed on day press. Default = undefined
                    onDayPress={this.onSelectedDay}
                    // Handler which gets executed on day long press. Default = undefined
                    // onDayLongPress={this.onSelectedDay}
                    onDayLongPress={undefined}
                    // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                    monthFormat={'MMMM yyyy'}
                    // Handler which gets executed when visible month changes in calendar. Default = undefined
                    // onMonthChange={(month) => {console.log('month changed', month)}}
                    onMonthChange={undefined}
                    // Hide month navigation arrows. Default = false
                    hideArrows={false}
                    // Replace default arrows with custom ones (direction can be 'left' or 'right')
                    renderArrow={this._renderArrow}
                    // Do not show days of other months in month page. Default = false
                    hideExtraDays={false}
                    // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                    // day from another month that is visible in calendar page. Default = false
                    disableMonthChange={false}
                    // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                    firstDay={1}
                    // Hide day names. Default = false
                    hideDayNames={false}
                    // Show week numbers to the left. Default = false
                    showWeekNumbers={false}
                    // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                    onPressArrowLeft={substractMonth => substractMonth()}
                    // Handler which gets executed when press arrow icon left. It receive a callback can go next month
                    onPressArrowRight={addMonth => addMonth()}
                    // Selected date
                    markedDates={{...this.state.selectedDate, ...this.state.notAvailableDate}}
                    />
                <View>
                    <Button 
                        primary={this.state.readyToSubmit} 
                        disabled={!this.state.readyToSubmit} 
                        label="Selesai" 
                        onPress={() => Actions.pop()} 
                        isLoading={false} />
                </View>
                <View>
                    <Text style={styles.note}>Keterangan</Text>
                    <View style={styles.list}>
                        <Text style={styles.note}>1. </Text>
                        <Text style={styles.note}>Tanggal dengan lingkaran merah artinya tanggal tersebut sudah tidak tersedia untuk di pesan.</Text>
                    </View>
                    <View style={styles.list}>
                        <Text style={styles.note}>2. </Text>
                        <Text style={styles.note}>Tanggal pemesanan yang tersedia adalah H+1 Bulan.</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    note: {
        fontSize: 12,
        textAlign: 'justify'
    },
    list: {
        flexDirection: 'row',
        paddingRight: 16
    }
})

export default Kalender;