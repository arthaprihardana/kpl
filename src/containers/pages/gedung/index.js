/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-07 23:31:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-18 22:24:27
 */
import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Dimensions, Text, TouchableHighlight, Image } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import { HeaderElipse } from '../../component';
import { WHITE_COLOR, BORDER_COLOR, PRIMARY_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, TEXT_COLOR } from '../../../assets/colors';
import {getGedungList} from '../../../actions';

const { width, height } = Dimensions.get("window");

class Gedung extends Component {

    state = {
        menuGedung: []
    }

    componentDidMount() {
        this.props.getGedungList();
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.gedung !== prevProps.gedung) {
            return { gedung: this.props.gedung }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.gedung) {
                this.setState({
                    menuGedung: snapshot.gedung
                });
            }
        }
    }
    
    render() {
        return (
            <ScrollView style={ styles.container } contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                {/* <View style={{ flexDirection: "column", justifyContent: 'center', alignItems: 'center', paddingHorizontal: 16 }}> */}
                    { this.state.menuGedung.length === 0 || this.props.loading ? 
                        _.map([1,2,3], (v, k) => ( <View key={k} style={{ 
                            width: '100%', 
                            height: 150,
                            borderRadius: 5, 
                            marginBottom: 10, 
                            backgroundColor: DISABLED_COLOR  }} /> ))
                        :
                        _.map(this.state.menuGedung, (v, k) => (
                        <TouchableOpacity
                            key={k}
                            activeOpacity={0.9}
                            style={{ 
                                width: '100%', 
                                height: 150, 
                                elevation: 1,
                                justifyContent: 'center', 
                                alignItems: 'center', 
                                borderRadius: 5, 
                                marginBottom: 10, 
                                backgroundColor: WHITE_COLOR 
                            }} 
                            onPress={() => Actions.push('formGedung', { title: v.nama_kategori, id_kategori: v.id_kategori, id_layanan: v.id_layanan, nama_kategori: v.nama_kategori }) }
                            underlayColor={DISABLED_COLOR}>
                            <Image source={{ uri: v.gambar_kategori }} resizeMode="cover" defaultSource={require('../../../assets/image/image_default.png')} style={{ width: '100%', height: 150, borderRadius: 5 }} blurRadius={0.3} />
                            <View style={{ position: 'absolute', bottom: 0, left: 0, width: '100%', paddingVertical: 20, paddingHorizontal: 16, justifyConten: 'center', backgroundColor: 'rgba(0,0,0,0.2)', borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }}>
                                <Text style={{ color: WHITE_COLOR, fontSize: 16, fontWeight: '500' }}>{v.nama_kategori}</Text>
                            </View>
                        </TouchableOpacity>
                        )) 
                    }
                {/* </View> */}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16
    },
    contentContainer: {
        // flex: 1,
        // paddingVertical: 20,
        // paddingHorizontal: 16
        paddingBottom: 20
    }
});

const mapStateToProps = ({ gedungReducer }) => {
    const { loading, gedung } = gedungReducer;
    return { loading, gedung };
}

export default connect(mapStateToProps, {
    getGedungList
})(Gedung)