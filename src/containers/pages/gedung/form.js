/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-12 21:17:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-03 19:27:23
 */
import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Text, Image, TouchableOpacity, Alert, Modal, TextInput, Keyboard, Platform } from 'react-native';
import { WebView } from 'react-native-webview';
import { connect } from 'react-redux';
import {DISABLED_COLOR, BORDER_COLOR, WHITE_COLOR, PLACEHOLDER_COLOR, TEXT_COLOR, ICON_COLOR, BACKGROUND_INPUT_FIELD} from '../../../assets/colors';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Contacts from 'react-native-contacts';
import moment from 'moment';
import 'moment/locale/id';
import _ from "lodash";
import { InputText, Button, Picker, ModalResponse } from '../../component';
import { Actions } from 'react-native-router-flux';
import {getGedungDetail, postOrderGedung, getBookingDate, getRekanan} from '../../../actions';
import AndroidPermissions from '../../../helpers/permissions';

class FormGedung extends Component {

    state = {
        forms: {
            nama_kategori: this.props.nama_kategori,
            nama_anggota: this.props.isLogin ? this.props.login.nama : "",
            id_anggota: this.props.isLogin ? this.props.login.id : "",
            id_layanan: this.props.id_layanan,
            id_kategori: this.props.id_kategori,
            tanggal_book: "",
            paket: "",
            telepon: "",
            keterangan: ""
        },
        errors: {
            err_tanggal_book: null,
            err_paket: null,
            err_nama_kategori: null,
            err_nama_anggota: null,
            err_id_anggota: null,
            err_id_layanan: null,
            err_id_kategori: null,
            err_telepon: null,
            err_keterangan: null
        },
        kontak: [],
        searchKontak: [],
        multiKontak: false,
        dataMultiKontak: [],
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null,
        modalContact: false,
        showDescription: false,
        gedungDetail: {},
        modalSyaratDanKetentuan: false,
        isModalSKHasOpen: false,
        masterPaket: [],
        bookingDate: [],
        modalRekanan: false,
        detailRekanan: null
    }

    componentDidMount() {
        this.props.getGedungDetail({ id_kategori: this.props.id_kategori })
        this.props.getBookingDate({ start_date: moment().format("YYYY-MM-DD"), id_kategori: this.props.id_kategori })
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.gedungDetail !== prevProps.gedungDetail) {
            return { gedungDetail: this.props.gedungDetail }
        }
        if(this.props.orderGedung !== prevProps.orderGedung) {
            return { orderGedung: this.props.orderGedung }
        }
        if(this.props.bookingDate !== prevProps.bookingDate) {
            return { bookingDate: this.props.bookingDate }
        }
        if(this.props.rekanan !== prevProps.rekanan) {
            return { rekanan: this.props.rekanan }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.tanggal_book !== prevProps.tanggal_book) {
            this.onChange('tanggal_book', Object.keys(this.props.tanggal_book)[0])
        }
        if(snapshot !== null) {
            if(snapshot.gedungDetail) {
                this.setState({
                    gedungDetail: snapshot.gedungDetail,
                    masterPaket: _.map(snapshot.gedungDetail.paket, (v, k) => {
                        return {id: k, nama: v.nama_paket, desc: v.des_paket}
                    })
                })
            }
            if(snapshot.orderGedung) {
                this.setState({
                    isResponse: true,
                    responseType: "sukses",
                    responseMessage: "Selamat, Anda telah berhasil melakukan pemesanan gedung Koperasi Pegawai Lamigas",
                    responseAction: () => {
                        this.setState({isResponse : false });
                        Actions.pop(); 
                    }
                })
            }
            if(snapshot.bookingDate) {
                this.setState({
                    bookingDate: snapshot.bookingDate
                })
            }
            if(snapshot.rekanan) {
                this.setState({
                    detailRekanan: snapshot.rekanan,
                    modalRekanan: true
                })
            }
        }
    }

    onChange = (key, value) => {
        const { forms, errors } = this.state;
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: val === undefined }))
        });
    }

    onSubmit = () => {
        Keyboard.dismiss();
        this.props.postOrderGedung({
            ..._.omit(this.state.forms, ["paket"]),
            paket: this.state.forms.paket.nama
        });
    }

    onValidate = async key => {
        let except = ["id_anggota", "id_layanan", "id_kategori", "nama_kategori"];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    onOpenContact = () => {
        if(this.state.kontak.length > 0) {
            this.setState({
                searchKontak: this.state.kontak,
                modalContact: !this.state.modalContact
            });
        } else {
            let req = this.requestContactPermission();
            req.then(value => {
                if(value === "granted") {
                    Contacts.getAll((err, contacts) => {
                        // contacts returned in Array
                        let kontak = _.filter(contacts, v => v.phoneNumbers.length > 0);
                        this.setState({
                            kontak: kontak,
                            searchKontak: kontak,
                            modalContact: !this.state.modalContact
                        })
                    })
                } else {
                    Alert.alert(
                        'Info',
                        'Anda tidak diizinkan untuk dapat mengakses kontak',
                        [
                            {text: 'Tanya Nanti', onPress: () => console.log('Ask me later pressed')},
                            {
                                text: 'Batal',
                                onPress: () => this.onOpenContact(),
                                style: 'cancel',
                            },
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        {cancelable: false},
                    );
                }
            });
        }
    }

    onContactSearch = text => {
        let reg = new RegExp(text.toLowerCase(), "g");
        let s = _.filter(this.state.kontak, v => reg.test(v.givenName.toLowerCase()));
        this.setState({ searchKontak: text.length > 0 ? s : this.state.kontak });
    }

    onContactSelected = value => {
        if(value.phoneNumbers.length > 1) {
            this.setState({
                multiKontak: true,
                dataMultiKontak: value
            })
        } else {
            this.onChange("telepon", value.phoneNumbers[0].number.replace(/\s+|-/g, ''))
            this.setState({
                modalContact: !this.state.modalContact
            });
        }
    }

    onContactMultiSelected = value => {
        this.onChange("telepon", value.replace(/\s+|-/g, ''))
        this.setState({
            modalContact: !this.state.modalContact,
            multiKontak: false,
            dataMultiKontak: [],
            searchKontak: this.state.kontak
        });
    }
    
    onRequestContactClose = () => {
        this.setState({
            multiKontak: false,
            modalContact: !this.state.modalContact
        })
    }

    requestContactPermission = async () => {
        try {
            if(Platform.OS === "android") {
                let Permissions = new AndroidPermissions();
                let reqContact = await Permissions.requestReadContactPermission();
                return reqContact;
            }
        } catch (err) {
            console.warn(err);
        }
    }

    render() {
        const { forms, detailRekanan } = this.state;
        const { gedungDetail, loading, isLogin, id_kategori } = this.props;
        return (
            <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <View style={styles.blockTitle}>
                    <Text style={{ fontWeight: '500' }}>Deskripsi</Text>
                </View>
                { (!loading && gedungDetail !== null) ?
                    <View style={styles.blockContent}>
                        <Text style={{ textAlign: "justify" }}>
                            { this.state.showDescription ? gedungDetail.penjelasan : gedungDetail.penjelasan.slice(0, 250) }
                        </Text>
                        <TouchableOpacity style={[this.state.showDescription ? styles.collapse : styles.expand, styles.opacity]} onPress={() => this.setState({ showDescription: !this.state.showDescription })}>
                            <Text style={{ color: TEXT_COLOR, fontSize: 10, marginRight: 16 }}>{ this.state.showDescription ? "Menampilkan lebih sedikit" : "Menampilkan lebih banyak" }</Text>
                            <Feather name={this.state.showDescription ? "chevrons-up" : "chevrons-down"} size={10} />
                        </TouchableOpacity>
                    </View> :
                    <View style={styles.blockContent}>
                        
                    </View>
                }

                <View style={styles.blockTitle}>
                    <Text style={{ fontWeight: '500' }}>Form Pemesanan</Text>
                </View>
                <View style={styles.blockContent}>
                    { !isLogin && 
                    <View>
                        <InputText 
                            nativeID="nama_anggota"
                            refs={ref => this._nama_anggota = ref}
                            keyboardType="phone-pad"
                            placeholder="Nama Pemesan"
                            value={forms.nama_anggota}
                            onChangeText={text => this.onChange("nama_anggota", text) }
                            maxLenght={15}
                            style={{ paddingRight: 60 }}/>
                    </View> }
                    <View>
                        <TouchableOpacity onPress={() => Actions.push('kalender', { bookingDate: this.state.bookingDate }) } style={{ marginBottom: 10, marginTop: 10 }}>
                            <View style={styles.picker}>
                                { _.isEmpty(forms.tanggal_book) ? <Text style={{ color: PLACEHOLDER_COLOR }}>Lihat jadwal yang tersedia</Text> : <Text style={{ color: TEXT_COLOR }}>{moment(forms.tanggal_book).format('LL')}</Text> }
                            </View>
                            <Feather name="calendar" size={20} color={ICON_COLOR} style={{ position: 'absolute', right: 20, top: 15 }} />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <InputText 
                            nativeID="telepon"
                            refs={ref => this._telepon = ref}
                            keyboardType="phone-pad"
                            placeholder="No Handphone"
                            value={forms.telepon}
                            onChangeText={text => this.onChange("telepon", text) }
                            maxLenght={15}
                            style={{ paddingRight: 60 }}/>
                        <TouchableOpacity onPress={() => this.onOpenContact()} style={{ position: 'absolute', right: 20, top: 24 }}>
                            <FontAwesome name="address-book" size={20} color={ICON_COLOR} />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Picker 
                            nativeID="paket"
                            refs={ref => this._paket = ref}
                            placeholder="Paket"
                            value={this.state.forms.paket.nama}
                            data={this.state.masterPaket}
                            errorMessage={this.state.errors.err_paket}
                            callback={value => this.onChange("paket", value)} />
                    </View>
                    { !_.isEmpty(this.state.forms.paket) && 
                        <View style={{ paddingHorizontal: 16 }}>
                            <Text>Deskripsi : </Text>
                            <Text style={{ textAlign: 'justify' }}>{this.state.forms.paket.desc}</Text>
                        </View>
                    }
                    <View>
                        <InputText 
                            nativeID="keterangan"
                            refs={ref => this._keterangan = ref}
                            placeholder="Keterangan"
                            keyboardType="default"
                            numberOfLines={4}
                            multiline={true}
                            onChangeText={text => this.onChange("keterangan", text)}
                            errorMessage={this.state.errors.err_keterangan}
                            returnKeyType="done" />
                    </View>
                </View>

                <View style={styles.blockTitle}>
                    <Text style={{ fontWeight: '500' }}>Galeri</Text>
                </View>
                <View style={styles.blockContent}>
                    { (!loading && gedungDetail !== null) &&
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        { [ ...gedungDetail.photo, ...gedungDetail.denah ].map((v, k) => <Image key={k} source={{ uri: v }} style={{ width: 150, height: 100, marginRight: 16 }} resizeMode="cover" /> ) }
                        <WebView source={{ uri: gedungDetail.video }} style={{ width: 150, height: 100, marginRight: 16 }} javaScriptEnabled={true} domStorageEnabled={true} />
                    </ScrollView> 
                    }
                </View>
                { id_kategori === "21" && 
                <View>
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Rekanan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        { (!loading && gedungDetail !== null) &&
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            { gedungDetail.rekanan.map((v, k) => 
                                <TouchableOpacity key={k} onPress={() => this.props.getRekanan({ kelompok_id: v.id })} style={{ width: 150, height: 100, marginRight: 16 }}>
                                    <Image source={{ uri: v.gambar }} style={{ width: 150, height: 100 }} resizeMode="cover" />
                                    <View style={{ position: 'absolute', bottom: 0, width: 150, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                        <Text style={{ color: WHITE_COLOR, fontSize: 16, fontWeight: '500' }}>{v.nama}</Text>
                                    </View>
                                </TouchableOpacity>
                            )}
                        </ScrollView>
                        }
                    </View>
                </View>
                }

                {/* <View style={styles.blockTitle}>
                    <Text style={{ fontWeight: '500' }}>Riwayat</Text>
                </View>
                <View style={styles.blockContent}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <View style={{ width: 150, height: 100, marginRight: 16 }}>
                            <Image source={{ uri: "https://i.pinimg.com/236x/b6/87/59/b687599d203c2e6a204bd7f022c14f6d--blank-wallpaper-apples.jpg" }} style={{ width: 150, height: 100 }} resizeMode="cover" />
                            <View style={{ position: 'absolute', bottom: 0, width: 150, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Text style={{ color: WHITE_COLOR, fontSize: 16, fontWeight: '500' }}>MUA</Text>
                            </View>
                        </View>
                        <View style={{ width: 150, height: 100, marginRight: 16 }}>
                            <Image source={{ uri: "https://i.pinimg.com/236x/b6/87/59/b687599d203c2e6a204bd7f022c14f6d--blank-wallpaper-apples.jpg" }} style={{ width: 150, height: 100 }} resizeMode="cover" />
                            <View style={{ position: 'absolute', bottom: 0, width: 150, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Text style={{ color: WHITE_COLOR, fontSize: 16, fontWeight: '500' }}>MUA</Text>
                            </View>
                        </View>
                        <View style={{ width: 150, height: 100, marginRight: 16 }}>
                            <Image source={{ uri: "https://i.pinimg.com/236x/b6/87/59/b687599d203c2e6a204bd7f022c14f6d--blank-wallpaper-apples.jpg" }} style={{ width: 150, height: 100 }} resizeMode="cover" />
                            <View style={{ position: 'absolute', bottom: 0, width: 150, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Text style={{ color: WHITE_COLOR, fontSize: 16, fontWeight: '500' }}>MUA</Text>
                            </View>
                        </View>
                        <View style={{ width: 150, height: 100, marginRight: 16 }}>
                            <Image source={{ uri: "https://i.pinimg.com/236x/b6/87/59/b687599d203c2e6a204bd7f022c14f6d--blank-wallpaper-apples.jpg" }} style={{ width: 150, height: 100 }} resizeMode="cover" />
                            <View style={{ position: 'absolute', bottom: 0, width: 150, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Text style={{ color: WHITE_COLOR, fontSize: 16, fontWeight: '500' }}>MUA</Text>
                            </View>
                        </View>
                    </ScrollView>
                </View> */}

                <View>
                    <TouchableOpacity onPress={() => this.setState(prevState => {
                        return {
                            modalSyaratDanKetentuan: !prevState.modalSyaratDanKetentuan,
                            isModalSKHasOpen: prevState.isModalSKHasOpen ? !prevState.isModalSKHasOpen : prevState.isModalSKHasOpen
                        }
                        }) } style={{ marginTop: 10, marginBottom: 10, justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 16, height: 30, borderRadius: 15, borderWidth: 0.5, borderColor: BORDER_COLOR, flexDirection: 'row', backgroundColor: BACKGROUND_INPUT_FIELD }}>
                        <Text>Syarat dan Ketentuan</Text>
                        <Feather name="chevrons-down" size={14} color={ICON_COLOR} />
                    </TouchableOpacity>
                </View>

                <View>
                    <Button 
                        primary={this.state.readyToSubmit} 
                        disabled={!this.state.readyToSubmit} 
                        label="Pesan" 
                        onPress={() => this.onSubmit()} 
                        isLoading={this.props.orderLoading} />
                </View>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalSyaratDanKetentuan}
                    onRequestClose={() => this.setState({ modalSyaratDanKetentuan: !this.state.modalSyaratDanKetentuan })} >
                    <View style={styles.modalContainer}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                            <View style={{ height: 56, width: 300, justifyContent: 'center', paddingHorizontal: 16 }}>
                                <Text>Syarat dan Ketentuan</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.setState({ modalSyaratDanKetentuan: !this.state.modalSyaratDanKetentuan }) } style={{ width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }}>
                                <Feather name="x" size={20} />
                            </TouchableOpacity>
                        </View>
                        { (!loading && gedungDetail !== null) ?
                            <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                                <Text style={{ textAlign: 'justify' }}>{gedungDetail.sk}</Text> 
                            </View>
                            : <View /> 
                        }
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalRekanan}
                    onRequestClose={() => this.setState({ modalRekanan: !this.state.modalRekanan })} >
                    <View style={styles.modalContainer}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                            <View style={{ height: 56, width: 300, justifyContent: 'center', paddingHorizontal: 16 }}>
                                <Text>Info Rekanan</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.setState({ modalRekanan: !this.state.modalRekanan }) } style={{ width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }}>
                                <Feather name="x" size={20} />
                            </TouchableOpacity>
                        </View>
                        { (detailRekanan !== null) ?
                            _.map(detailRekanan, (v, k) => (
                                <View key={k} style={{ paddingHorizontal: 16, paddingVertical: 16, borderBottomColor: BORDER_COLOR, borderBottomWidth: .5 }}>
                                    <Image source={{ uri: v.gambar_rekanan }} resizeMode="cover" defaultSource={require('../../../assets/image/image_default.png')} style={{ width: '100%', height: 150, borderRadius: 5 }} blurRadius={0.3} />
                                    <Text style={{ fontSize: 18, fontWeight: '500' }}>{detailRekanan[0].nama_rekanan}</Text>
                                    <Text style={{ textAlign: 'justify' }}>{detailRekanan[0].deskripsi_rekanan}</Text> 
                                </View>
                            ))
                            : <View /> 
                        }
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalContact}
                    onRequestClose={() => this.onRequestContactClose()}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalHeader}>
                            <TouchableOpacity style={styles.closeButton} onPress={() => this.onRequestContactClose()}>
                                <Feather name="x" size={22} color={ICON_COLOR} />
                            </TouchableOpacity>
                            <View style={styles.modalHeaderTitle}>
                                <Text style={styles.headerTitle}>Kontak</Text>
                            </View>
                        </View>
                        {this.state.multiKontak ? 
                            <View>
                                <TouchableOpacity onPress={() => this.setState({ multiKontak: false })} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 16, flexDirection: 'row' }}>
                                    <Feather name="arrow-left" size={16} />
                                    <Text>Kembali</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }}>
                                    <Text>Pilih No Telepon dari Kontak : {this.state.dataMultiKontak.givenName} {this.state.dataMultiKontak.familyName}</Text>
                                </View>
                                <ScrollView style={{ marginBottom: 220 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                                    {this.state.dataMultiKontak.phoneNumbers && _.map(this.state.dataMultiKontak.phoneNumbers, (v, k) => (
                                        <TouchableOpacity key={k} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }} onPress={() => this.onContactMultiSelected(v.number) }>
                                            <Text>{v.number}</Text>
                                        </TouchableOpacity>
                                    ))}
                                </ScrollView>
                            </View>
                            :
                            <View>
                                <View style={styles.searchContainer}>
                                    <TextInput 
                                        style={styles.searchInput}
                                        placeholder={"Cari Kontak..."}
                                        placeholderTextColor={PLACEHOLDER_COLOR}
                                        onChangeText={ text =>  this.onContactSearch(text) } />
                                    <Feather name="search" size={14} color={ICON_COLOR} style={{ position: 'absolute', right: 30, top: 20 }} />
                                </View>
                                <ScrollView style={{ marginBottom: 220 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                                { this.state.searchKontak && _.map(this.state.searchKontak, (v, k) => (
                                    <TouchableOpacity key={k} style={{ width: '100%', height: 50, borderBottomWidth: .3, borderBottomColor: BORDER_COLOR, justifyContent: 'center', paddingHorizontal: 16 }} onPress={() => this.onContactSelected(v) }>
                                        <Text>{v.givenName} {v.familyName}</Text>
                                    </TouchableOpacity>
                                )) }
                                </ScrollView>
                            </View>
                        }
                    </View>
                </Modal>

                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 16,
        paddingHorizontal: 16
    },
    contentContainer: {
        paddingBottom: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    },
    expand: {
        position: 'absolute',
        bottom: 0,
        left: 10,
        width: '100%',
        height: 30,
        zIndex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#000',
        // opacity: (1 / 15) * (5 + 1)
    },
    collapse: {
        width: '100%',
        height: 30,
        zIndex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    opacity: {
        backgroundColor: WHITE_COLOR,
        opacity: .8,
        flexDirection: 'row'
    },
    picker: {
        height: 50, 
        borderWidth: 0.5, 
        borderColor: BORDER_COLOR, 
        backgroundColor: BACKGROUND_INPUT_FIELD,
        borderRadius: 25, 
        paddingHorizontal: 25,
        justifyContent: 'center'
    },
    modalHeader: { 
        width: '100%', 
        height: 56, 
        backgroundColor: WHITE_COLOR, 
        borderBottomWidth: .5, 
        borderBottomColor: BORDER_COLOR, 
        flexDirection: 'row' 
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    modalHeaderTitle: { 
        width: 150, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'flex-start' 
    },
    headerTitle: { 
        color: TEXT_COLOR, 
        fontWeight: '600' 
    },
    searchContainer: { 
        width: '100%', 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 16, 
        backgroundColor: DISABLED_COLOR, 
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 1 
    },
    searchInput: {
        width: '100%',
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 25,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        paddingRight: 40
    },
    iconPosition: {
        position: 'absolute', right: 20, top: 15
    }
})

const mapStateToProps = ({ gedungReducer, loginReducer }) => {
    const { loading, orderLoading, gedungDetail, orderGedung, bookingDate, rekanan } = gedungReducer;
    const { isLogin, login } = loginReducer;
    return { loading, orderLoading, gedungDetail, orderGedung, isLogin, login, bookingDate, rekanan }
}

export default connect(mapStateToProps, {
    getGedungDetail,
    postOrderGedung,
    getBookingDate,
    getRekanan
})(FormGedung);
