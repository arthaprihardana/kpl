/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:07:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-06 18:14:17
 */
import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Keyboard, Text } from 'react-native';
import { connect } from "react-redux";
import _ from "lodash";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { InputText, Button, Picker, DatePicker, HeaderElipse, ModalResponse } from '../../component';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, BORDER_COLOR, DISABLED_COLOR } from '../../../assets/colors';
import { getKota, postOrderShuttle, clearPrevTiket } from '../../../actions';
import { Actions } from 'react-native-router-flux';

class Shuttle extends Component {

    state = {
        forms: {
            dari: "",
            ke: "",
            nama_shuttle: "",
            penumpang: "",
            waktu_kedatangan: "",
            nama_anggota: this.props.isLogin ? this.props.login.nama : "",
            id_anggota: this.props.isLogin ? this.props.login.id : ""
        },
        errors: {
            err_dari: null,
            err_ke: null,
            err_nama_shuttle: null,
            err_penumpang: null,
            err_waktu_kedatangan: null,
            err_nama_anggota: null,
            err_id_anggota: null
        },
        masterKota: [],
        masterShuttle: [{id: 1, nama: "Cipaganti"}, {id: 2, nama: "XTrans"}],
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null
    }

    componentDidMount() {
        this.props.getKota()
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.kota !== prevProps.kota) {
            return { kota: this.props.kota }
        }
        if(this.props.orderShuttle !== prevProps.orderShuttle) {
            return { orderShuttle: this.props.orderShuttle }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.kota) {
                this.setState({
                    masterKota: snapshot.kota
                })
            }
            if(snapshot.orderShuttle) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.orderShuttle.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.orderShuttle.status ? "Selamat, pemesanan shuttle sedang diproses" : "Terjadi kesalahan dalam pemesanan",
                    responseAction: () => {
                        this.props.clearPrevTiket();
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
        }
    }

    onChange = (key, value) => {
        let forms = this.state.forms;
        let errors = this.state.errors;
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms });
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: val === undefined }))
        });
    }

    onSubmit = () => {
        const { forms } = this.state;
        Keyboard.dismiss();
        this.props.postOrderShuttle({
            dari: forms.dari.nama,
            ke: forms.ke.nama,
            nama_shuttle: forms.nama_shuttle,
            penumpang: forms.penumpang,
            waktu_kedatangan: forms.waktu_kedatangan,
            nama_anggota: forms.nama_anggota,
            id_anggota: forms.id_anggota
        });
    }

    onValidate = async key => {
        let except = ["id_anggota"];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    render() {
        const { forms } = this.state;
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                {/* <HeaderElipse /> */}
                <View style={styles.container}>
                    {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                        <View style={{ width: 110, height: 110, borderRadius: 55, justifyContent: 'center', alignItems: 'center', backgroundColor: DARK_PRIMARY_COLOR }}>
                            <FontAwesome name="taxi" size={60} color={WHITE_COLOR} />
                        </View>
                    </View> */}
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pemesan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="nama_anggota"
                                refs={ref => this._nama_anggota = ref}
                                placeholder="Nama Anggota"
                                keyboardType="default"
                                value={forms.nama_anggota}
                                onChangeText={text => this.onChange("nama_anggota", text)} />
                        </View>
                    </View>
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Perjalanan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <Picker 
                                nativeID="dari"
                                refs={ref => this._dari = ref}
                                placeholder="Dari"
                                data={this.state.masterKota}
                                isLoading={false}
                                isSearchable={true}
                                value={forms.dari.nama}
                                callback={value => this.onChange("dari", value)} />
                        </View>
                        <View>
                            <Picker 
                                nativeID="ke"
                                refs={ref => this._ke = ref}
                                placeholder="Ke"
                                data={this.state.masterKota}
                                isLoading={false}
                                isSearchable={true}
                                value={forms.ke.nama}
                                callback={value => this.onChange("ke", value)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="nama_shuttle"
                                refs={ref => this._nama_shuttle = ref}
                                placeholder="Nama Shuttle"
                                keyboardType="default"
                                onChangeText={text => this.onChange("nama_shuttle", text)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="penumpang"
                                refs={ref => this._penumpang = ref}
                                placeholder="Jumlah Penumpang"
                                keyboardType="phone-pad"
                                maxLength={2}
                                onChangeText={text => this.onChange("penumpang", text)} />
                        </View>
                        <View>
                            <DatePicker 
                                nativeID="waktu_kedatangan"
                                refs={ref => this._waktu_kedatangan = ref}
                                placeholder="Tanggal Keberangkatan"
                                hasMinDate={true}
                                value={forms.waktu_kedatangan}
                                callback={date => this.onChange("waktu_kedatangan", date.format("YYYY-MM-DD"))} />
                        </View>
                    </View>
                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Pesan"
                            onPress={() => this.onSubmit()}
                            isLoading={this.props.orderLoader} />
                    </View>
                </View>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    }
})

const mapStateToProps = ({ kotaReducer, tiketReducer, loginReducer }) => {
    const { loading, kota } = kotaReducer;
    const { orderShuttle } = tiketReducer;
    const { isLogin, login } = loginReducer;
    const orderLoader = tiketReducer.loading;
    return { loading, kota, orderShuttle, orderLoader, isLogin, login }
}

export default connect(mapStateToProps, {
    getKota,
    postOrderShuttle,
    clearPrevTiket
})(Shuttle);