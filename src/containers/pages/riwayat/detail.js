/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-06 11:20:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-07 22:33:07
 */
import React, { Component } from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment/locale/id';
import {BORDER_COLOR, WHITE_COLOR} from '../../../assets/colors';
import {Button} from '../../component';
import { Actions } from 'react-native-router-flux';
import {DISABLED_COLOR} from '../../../assets/colors';
import { rupiah } from '../../../helpers'

class DetailRiwayat extends Component {

    state = {
        detail: []
    }

    componentDidMount() {
        this.setState({
            detail: this.props.detailRiwayat
        });
    }

    renderTiketing = detail => {
        return (
            detail[0].id_kategori !== 2 ? 
                <View>
                    <View style={{paddingHorizontal: 16, paddingVertical: 16, backgroundColor: WHITE_COLOR, marginBottom: 25 }}>
                        { detail[0].id_kategori === 5 && <Text style={{ lineHeight: 30, textAlign: 'center', fontSize: 20, marginBottom: 16 }}>{detail[0].nama_shuttle}</Text> }
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: '50%', height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={{ lineHeight: 30 }}>Dari</Text>
                                <Text style={{ fontWeight: '500', textAlign: 'center' }}>{detail[0].dari}</Text>
                            </View>
                            <View style={{ width: '50%', height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={{ lineHeight: 30 }}>Ke</Text>
                                <Text style={{ fontWeight: '500', textAlign: 'center' }}>{detail[0].ke}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            { (detail[0].id_kategori === 5 || detail[0].id_kategori === 4) && <View style={{ width: '50%', height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={{ lineHeight: 30 }}>Kedatangan</Text>
                                <Text style={{ fontSize: 30, fontWeight: '500' }}>{moment(detail[0].waktu_kedatangan).format('DD')}</Text>
                                <Text style={{ fontWeight: '400' }}>{moment(detail[0].waktu_kedatangan).format('MMM YYYY')}</Text>
                            </View> }
                            <View style={{ width: '50%', height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={{ lineHeight: 30 }}>Penumpang {detail[0].id_kategori === 3 && "Dewasa"}</Text>
                                <Text style={{ fontSize: 30, fontWeight: '500' }}>{ detail[0].id_kategori === 3 ? detail[0].penumpang_dewasa : detail[0].penumpang}</Text>
                                <Text style={{ fontWeight: '400' }}>Orang</Text>
                            </View>
                            { detail[0].id_kategori === 3 && 
                            <View style={{ width: '50%', height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={{ lineHeight: 30 }}>Penumpang Balita</Text>
                                <Text style={{ fontSize: 30, fontWeight: '500' }}>{_.isNull(detail[0].penumpang_balita) ? "0" : detail[0].penumpang_balita}</Text>
                                <Text style={{ fontWeight: '400' }}>Orang</Text>
                            </View> }
                            { detail[0].id_kategori === 1 && 
                            <View style={{ width: '50%', height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Text style={{ lineHeight: 30 }}>Kelas</Text>
                                <Text style={{ fontSize: 30, fontWeight: '500' }}>{detail[0].kursi_kelas}</Text>
                                <Text style={{ fontWeight: '400' }}>Class</Text>
                            </View> }
                        </View>
                    </View>
                    
                    { detail[0].id_kategori === 1 && <View style={{paddingHorizontal: 16, paddingVertical: 16, backgroundColor: WHITE_COLOR }}>
                        { _.map(detail[0].nama_penumpang.split(';'), (v, k) => (
                            <View key={k} style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text>Penumpang {k+1}</Text>
                                <Text>{v}</Text>
                            </View>
                        )) }
                        </View> }
                </View> 
                :
                <View style={{paddingHorizontal: 16, paddingVertical: 16, backgroundColor: WHITE_COLOR }}>
                    <Text style={{ lineHeight: 30, textAlign: 'center', fontSize: 20 }}>{detail[0].nama_hotel}</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Text style={{ lineHeight: 30 }}>Check In</Text>
                            <Text style={{ fontSize: 30, fontWeight: '500' }}>{moment(detail[0].check_in).format('DD')}</Text>
                            <Text style={{ fontWeight: '400' }}>{moment(detail[0].check_in).format('MMM YYYY')}</Text>
                        </View>
                        <View style={{ width: '50%', height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Text style={{ lineHeight: 30 }}>Check Out</Text>
                            <Text style={{ fontSize: 30, fontWeight: '500' }}>{moment(detail[0].check_out).format('DD')}</Text>
                            <Text style={{ fontWeight: '400' }}>{moment(detail[0].check_out).format('MMM YYYY')}</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', height: 100, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ lineHeight: 30 }}>Jumlah Tamu</Text>
                            <Text style={{ fontSize: 30, fontWeight: '500' }}>{detail[0].tamu}</Text>
                        </View>
                        <View style={{ width: '50%', height: 100, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ lineHeight: 30 }}>Jumlah Kamar</Text>
                            <Text style={{ fontSize: 30, fontWeight: '500' }}>{detail[0].rooms}</Text>
                        </View>
                    </View>
                </View> 
        )
    }

    renderTopUp = detail => {
        return (
            (detail[0].id_kategori === 12  || detail[0].id_kategori === 6) ? 
            <View style={{paddingHorizontal: 16, paddingVertical: 16, backgroundColor: WHITE_COLOR }}>
                <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text>Nomor Handphone</Text>
                    <Text>{detail[0].no_hp}</Text>
                </View>
                { detail[0].id_kategori === 6 && 
                <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text>Nominal</Text>
                    <Text>{rupiah(detail[0].nominal)}</Text>
                </View> }
                { detail[0].id_kategori === 12 && 
                <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text>Paket</Text>
                    <Text>{detail[0].paket}</Text>
                </View> }
            </View>
            :
            <View style={{paddingHorizontal: 16, paddingVertical: 16, backgroundColor: WHITE_COLOR }}>
                <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text>Nomor Meter</Text>
                    <Text>{detail[0].no_meter}</Text>
                </View>
                { detail[0].id_kategori === 8 && 
                <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text>Nominal Token</Text>
                    <Text>{rupiah(detail[0].nominal_token)}</Text>
                </View> }
            </View>
        )
    }

    renderToko = detail => {
        let total = _.reduce(detail, (sum, n) => sum + (n.harga_barang * n.qty) , 0);
        return (
           <View style={{paddingHorizontal: 16, paddingVertical: 16, backgroundColor: WHITE_COLOR }}>
                { _.map(detail, (v, k) => (
                    <View key={k} style={{ paddingVertical: 16, borderBottomWidth: 0.5, borderBottomColor: BORDER_COLOR }}>
                        <Text style={{ fontSize: 14, fontWeight: '500' }}>{v.nama_barang}</Text>
                        <Text style={{ fontWeight: '500' }}>{rupiah(v.harga_barang)}</Text>
                        <Text>Qty : {v.qty}</Text>
                    </View>
                )) }

                <Text style={{ fontSize: 16, fontWeight: '500', lineHeight: 24, marginTop: 16 }}>Total : {rupiah(total)}</Text>
            </View>
        )
    }

    renderGedung = detail => {
        return (
             <View style={{paddingHorizontal: 16, paddingVertical: 16, backgroundColor: WHITE_COLOR }}>
                <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text>Paket</Text>
                    <Text>{detail[0].pilihan_paket}</Text>
                </View>
                <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text>Keterangan</Text>
                    <Text>{detail[0].keterangan}</Text>
                </View>
            </View>
        )
    }

    renderDetail = detail => {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ paddingHorizontal: 16, paddingVertical: 16, backgroundColor: WHITE_COLOR, marginTop: 25, marginBottom: 25 }}>
                    <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', justifyContent: 'center' }}>
                            <Text>Nama Layanan</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text>{detail[0].nama_layanan}</Text>
                        </View>
                    </View> 
                    <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', justifyContent: 'center' }}>
                            <Text>Kategori</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text>{detail[0].nama_kategori}</Text>
                        </View>
                    </View> 
                    <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', justifyContent: 'center' }}>
                            <Text>Tanggal order</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text>{moment(detail[0].tanggal_order).format('LL')}</Text>
                        </View>
                    </View>
                    { (detail[0].id_kategori === 3 || detail[0].id_kategori === 1) && 
                    <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', justifyContent: 'center' }}>
                            <Text>Tanggal Keberangkatan</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text>{detail[0].id_kategori === 3 ? moment(detail[0].waktu_kedatangan).format('LL') : moment(detail[0].waktu_keberangkatan).format('LL')}</Text>
                        </View>
                    </View> }
                    { detail[0].id_kategori === 1 && 
                    <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', justifyContent: 'center' }}>
                            <Text>Maskapai</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text>{detail[0].nama_maskapai || "-"}</Text>
                        </View>
                    </View> }
                    { (detail[0].id_layanan === 4 || detail[0].id_layanan === 6) && 
                    <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', justifyContent: 'center' }}>
                            <Text>Tanggal Booking</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text>{ detail[0].id_layanan === 4 ? moment(detail[0].tanggal_book).format('LLL') : moment(detail[0].tanggal_book).format('LL')}</Text>
                        </View>
                    </View> }
                    { detail[0].id_kategori === 14 && 
                    <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', justifyContent: 'center' }}>
                            <Text>Nilai Pinjaman</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text>{rupiah(detail[0].nilai_pinjaman)}</Text>
                        </View>
                    </View> }
                    { detail[0].id_kategori === 13 && 
                    <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', justifyContent: 'center' }}>
                            <Text>Nilai Simpanan</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text>{rupiah(detail[0].jumlah_simpanan)}</Text>
                        </View>
                    </View> }
                    { detail[0].id_kategori === 13 && 
                    <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                        <View style={{ width: '50%', justifyContent: 'center' }}>
                            <Text>Disetor Ke</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text>{detail[0].store_ke}</Text>
                        </View>
                    </View> }
                </View>

                { detail[0].id_layanan === 1 && this.renderTiketing(detail) }
                { detail[0].id_layanan === 2 && this.renderToko(detail) }
                { detail[0].id_layanan === 5 && this.renderTopUp(detail) }
                { detail[0].id_layanan === 6 && this.renderGedung(detail) }

                <View style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{  width: 40, height: 40, borderRadius: 20, backgroundColor: DISABLED_COLOR, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={{ uri: detail[0].icon_layanan }} style={{  width: 30, height: 30, borderRadius: 15 }} resizeMode="cover" />
                    </View>
                </View>

                <View style={{ paddingHorizontal: 16, paddingVertical: 16, width: '100%'}}>
                    <Button 
                        primary={true} 
                        disabled={false} 
                        label="Kembali"
                        onPress={() => Actions.pop()}
                        isLoading={false} />
                </View>
            </View>
        )
    }

    render() {
        const { detail } = this.state;
        return (
            <ScrollView style={{ flex: 1, backgroundColor: DISABLED_COLOR, padding: 10 }}>
                { detail.length > 0 ? this.renderDetail(detail) : <View /> }
            </ScrollView>
        );
    }

}

const mapStateToProps = ({ riwayatReducer }) => {
    const { loading, detailRiwayat } = riwayatReducer;
    return { loading, detailRiwayat }
}

export default connect(mapStateToProps, {

})(DetailRiwayat);