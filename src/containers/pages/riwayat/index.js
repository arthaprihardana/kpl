/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-11 00:07:46
 */
import React, { Component } from 'react';
import { View, Text, FlatList, RefreshControl, StyleSheet, Dimensions, Image, TouchableOpacity, LayoutAnimation, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import moment from 'moment';
import 'moment/locale/id';
import {getRiwayat, getDetailRiwayat} from '../../../actions';
import {BORDER_COLOR, DARK_PRIMARY_COLOR, ERROR_COLOR, TEXT_COLOR, DISABLED_COLOR, PRIMARY_COLOR} from '../../../assets/colors';
import {rupiah} from '../../../helpers';
import { Actions } from 'react-native-router-flux';

const { width, height} = Dimensions.get("window");

class Riwayat extends Component {

    state = {
        riwayat: [],
        limit: 20,
        offset: 0,
        isLoading: true,
        isRefresh: false,
        isInfinityScroll: false,
        isNoMoreData: false,
        showInfoNoMoreData: false,
        title: null,
        index: 0
    }

    componentDidMount() {
        this.props.getRiwayat({
            anggota_id: this.props.login.id,
            limit: this.state.limit,
            offset: this.state.offset
        });
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.riwayat !== prevProps.riwayat) {
            return { riwayat: this.props.riwayat }
        }
        if(this.props.detailRiwayat !== prevProps.detailRiwayat) {
            return { detailRiwayat: this.props.detailRiwayat }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.riwayat) {
                if(snapshot.riwayat.length > 0) {
                    this.setState(prevState => ({
                        riwayat: _.concat(prevState.riwayat, snapshot.riwayat),
                        isInfinityScroll: true
                    }))
                } else {
                    LayoutAnimation.easeInEaseOut();
                    this.setState(prevState => ({
                        isInfinityScroll: false,
                        offset: prevState.riwayat.length,
                        isNoMoreData: true,
                        showInfoNoMoreData: true
                    }));
                }
            }
            if(snapshot.detailRiwayat) {
                Actions.push('detailRiwayat', { title: this.state.title });
            }
        }
        if(this.props.loading !== prevProps.loading) {
            this.setState({ isLoading: this.props.loading })
        }
        if(this.state.showInfoNoMoreData !== prevState.showInfoNoMoreData) {
            setTimeout(() => {
                LayoutAnimation.easeInEaseOut();
                this.setState({ showInfoNoMoreData: false })
            }, 2000);
        }
    }
    

    onClickDetail = (item, index) => {
        this.setState({
            title: item.nama_kategori,
            index: index
        })
        this.props.getDetailRiwayat({
            id_order: item.id_order,
            anggota_id: item.id_anggota,
            id_layanan: item.id_layanan,
            id_kategori: item.id_kategori
        });
        // Actions.push('detailRiwayat', { title: `${item.nama_layanan} (${item.nama_kategori})` });
    }

    renderItem = (item, index) => {
        return (
            <TouchableOpacity style={styles.item} key={index} onPress={() => this.onClickDetail(item, index)}>
                <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={{ uri: item.icon_layanan }} style={{  width: 40, height: 40, borderRadius: 20, marginBottom: 10 }} resizeMode="cover" />
                    <Text>{moment(item.tanggal_order).format('DD MMM')}</Text>
                </View>
                <View style={{ width: '70%', justifyContent: 'center', paddingHorizontal: 10 }}>
                    <Text style={{ fontSize: 16 }}>{item.nama_layanan}</Text>
                    <Text style={{ fontSize: 16 }}>{item.nama_kategori}</Text>
                    <Text style={{ fontSize: 16, fontWeight: '500' }}>{_.isNull(item.total) ? "Rp -" : rupiah(item.total)}</Text>
                    <Text style={{ fontSize: 12, color: _.isEmpty(item.approval) ? ERROR_COLOR : PRIMARY_COLOR }}>{_.isEmpty(item.approval) ? "Belum Disetujui" : (item.approval ? "Sudah Disetujui" : "Tidak Disetujui") }</Text>
                    { (this.props.loading && this.state.index === index) && <ActivityIndicator style={{ position: 'absolute', right: 16 }} size="small" color={PRIMARY_COLOR} /> }
                </View>
            </TouchableOpacity>
        )
    }

    renderEmptyComponent = () => {
        return (
            this.state.isLoading && this.state.riwayat.length === 0 ?
            _.map([1,2,3,4], (v, k) => (
                <View style={styles.item} key={k}>
                    <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: DISABLED_COLOR, marginBottom: 10 }} />
                        <View style={{ height: 12, width: '80%', backgroundColor: DISABLED_COLOR, borderRadius: 6 }} />
                    </View>
                    <View style={{ width: '70%', justifyContent: 'center', paddingHorizontal: 10 }}>
                        <View style={{ height: 16, width: '60%', backgroundColor: DISABLED_COLOR, borderRadius: 6, marginBottom: 5 }} />
                        <View style={{ height: 16, width: '80%', backgroundColor: DISABLED_COLOR, borderRadius: 6, marginBottom: 5 }} />
                        <View style={{ height: 12, width: '80%', backgroundColor: DISABLED_COLOR, borderRadius: 6, marginBottom: 5 }} />
                    </View>
                </View>
            ))
            : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', width: width, height: (height - 56) }}>
                <Image source={require('../../../assets/image/empty.png')} style={{ width: 200, height: 200 }} resizeMode="contain" />
            </View>
        )
    }

    renderFooterComponent = () => {
        if (!this.state.isInfinityScroll) return null;
        return (
            <View
                style={{
                    position: 'relative',
                    width: width,
                    paddingVertical: 8,
                    borderTopWidth: 0.5,
                    borderColor: BORDER_COLOR,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: -6
                }}>
                <Text style={{ fontSize: 12, color: TEXT_COLOR }}>Sedang memuat riwayat ...</Text>
            </View>
          );
    }

    onRefresh = () => {
        this.setState({
            riwayat: [],
            limit: 20,
            offset: 0,
            isNoMoreData: false
        }, () => {
            this.props.getRiwayat({
                anggota_id: this.props.login.id,
                limit: this.state.limit,
                offset: this.state.offset
            });
        })
    }

    handleLoadMore = () => {
        if(!this.state.isNoMoreData) {
            this.setState(prevState => ({
                offset: prevState.offset + prevState.limit,
                isInfinityScroll: true
            }), () => {
                this.props.getRiwayat({
                    anggota_id: this.props.login.id,
                    limit: this.state.limit,
                    offset: this.state.offset
                });
            });
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList 
                    contentContainerStyle={{ paddingHorizontal: 0, paddingVertical: 0, }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.riwayat}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    ListEmptyComponent={() => this.renderEmptyComponent()}
                    // ListHeaderComponent={() => this.renderHeaderComponent() }
                    ListFooterComponent={() => this.renderFooterComponent()}
                    renderItem={({item, index}) => this.renderItem(item, index) }
                    refreshControl={<RefreshControl
                            refreshing={this.state.isRefresh}
                            onRefresh={this.onRefresh}
                            colors={[DARK_PRIMARY_COLOR]}
                        />
                    }
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={this.state.limit}
                    numColumns={1}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        width: width,
        paddingHorizontal: 0,
        paddingVertical: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 100
    },
    separator: {
        width: width,
        borderBottomColor: BORDER_COLOR,
        borderBottomWidth: 0.5,
    }
})

const mapStateToProps = ({ riwayatReducer, loginReducer }) => {
    const { loading, riwayat, detailRiwayat } = riwayatReducer;
    const { login, isLogin } = loginReducer;
    return { loading, riwayat, login, isLogin, detailRiwayat }
}

export default connect(mapStateToProps, {
    getRiwayat,
    getDetailRiwayat
})(Riwayat)