/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-15 07:12:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-07 22:49:51
 */
import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, Dimensions, Text, TouchableHighlight, Image } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';
import { HeaderElipse } from '../../component';
import { WHITE_COLOR, BORDER_COLOR, PRIMARY_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, TEXT_COLOR } from '../../../assets/colors';

const { width, height } = Dimensions.get("window");

class SimpanPinjam extends Component {

    state = {
        menuSimpanPinjam: [{
            nama: "SIMPANAN",
            image: "https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2016/11/Pinjaman-Uang-Ilustrasi.jpg",
            type: "simpan"
        }, {
            nama: "PINJAMAN",
            image: "https://mediakonsumen.com/files/2017/12/ilustrasi-pinjaman-uang-bunga-kalkulator.jpg",
            type: "pinjam"
        }]
    }
    
    render() {
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                {/* <HeaderElipse /> */}
                {/* <View style={{ paddingHorizontal: 16, paddingVertical: 16, marginTop: 0 }}> */}
                    {/* <View style={{ width: '100%', height: 50, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 16, marginBottom: 20 }}>
                        <Text style={{ color: TEXT_COLOR, fontSize: 18, fontWeight: '500', textAlign: 'center' }}>Serba Usaha</Text>
                        <Text style={{ color: TEXT_COLOR, fontSize: 18, fontWeight: '500', textAlign: 'center' }}>di Koperasi Lemigas</Text>
                    </View> */}
                    <View style={{ flexDirection: "column", justifyContent: 'center', alignItems: 'center', paddingHorizontal: 16 }}>
                        { _.map(this.state.menuSimpanPinjam, (v, k) => (
                            <TouchableOpacity
                                key={k}
                                activeOpacity={0.9}
                                style={{ 
                                    width: '100%', 
                                    height: 150, 
                                    elevation: 1,
                                    justifyContent: 'center', 
                                    alignItems: 'center', 
                                    borderRadius: 5, 
                                    marginBottom: 10, 
                                    backgroundColor: WHITE_COLOR 
                                }} 
                                onPress={() => Actions.push('formSimpanPinjam', { type: v.type, title: v.nama })}
                                underlayColor={DISABLED_COLOR}>
                                <Image source={{ uri: v.image }} resizeMode="cover" defaultSource={require('../../../assets/image/image_default.png')} style={{ width: '100%', height: 150, borderRadius: 5 }} blurRadius={0.3} />
                                <View style={{ position: 'absolute', bottom: 0, left: 0, width: '100%', paddingVertical: 20, paddingHorizontal: 16, justifyConten: 'center', backgroundColor: 'rgba(0,0,0,0.2)', borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }}>
                                    <Text style={{ color: WHITE_COLOR, fontSize: 16, fontWeight: '500' }}>{v.nama}</Text>
                                </View>
                            </TouchableOpacity>
                            )) 
                        }
                    </View>
                {/* </View> */}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        flex: 1,
        paddingVertical: 20
    }
});

export default SimpanPinjam;