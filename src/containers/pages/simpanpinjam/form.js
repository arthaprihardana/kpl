/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 21:30:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-04 07:43:19
 */
import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Keyboard, Text, TouchableOpacity, Platform, Alert } from 'react-native';
import { connect } from 'react-redux';
import RNFetchBlob from 'rn-fetch-blob';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';
import { postOrderSimpan, getContentFormSimpan, postOrderPinjam, getContentFormPinjam } from '../../../actions';
import { InputText, Button, Picker, DatePicker, HeaderElipse, ModalResponse, TimePicker } from '../../component';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, DISABLED_COLOR, BORDER_COLOR, ICON_COLOR, PLACEHOLDER_COLOR, TEXT_COLOR } from '../../../assets/colors';
import AndroidPermissions from '../../../helpers/permissions';

class FormSimpanPinjam extends Component {

    state = {
        staticContent: null,
        masterTenor: [{
            id: 1,
            nama: "1 Bulan"
        }, {
            id: 2,
            nama: "2 Bulan"
        }, {
            id: 3,
            nama: "3 Bulan"
        }, {
            id: 4,
            nama: "4 Bulan"
        }, {
            id: 5,
            nama: "5 Bulan"
        }, {
            id: 6,
            nama: "6 Bulan"
        }, {
            id: 7,
            nama: "7 Bulan"
        }, {
            id: 8,
            nama: "8 Bulan"
        }, {
            id: 9,
            nama: "9 Bulan"
        }, {
            id: 10,
            nama: "10 Bulan"
        }, {
            id: 11,
            nama: "11 Bulan"
        }, {
            id: 12,
            nama: "12 Bulan"
        }],
        forms: {
            simpan: {
                id_anggota: this.props.isLogin ? this.props.login.id : "",
                nama_anggota: this.props.isLogin ? this.props.login.nama : "",
                jumlah_simpanan: "",
                store_ke: "langsung",
                keterangan: ""
            },
            pinjam: {
                id_anggota: this.props.isLogin ? this.props.login.id : "",
                nama_anggota: this.props.isLogin ? this.props.login.nama : "",
                nilai_pinjaman: "",
                tenor: "",
                keterangan: ""
            }
        },
        errors: {
            simpan: {
                err_jumlah_simpanan: null,
                err_keterangan: null
            },
            pinjam: {
                err_nilai_pinjaman: null,
                err_tenor: null,
                err_keterangan: null
            }
        },
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null
    }

    componentDidMount() {
        if(this.props.type === "simpan") {
            this.props.getContentFormSimpan()
        } else {
            this.props.getContentFormPinjam()
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.staticFormSimpan !== prevProps.staticFormSimpan) {
            return { staticFormSimpan: this.props.staticFormSimpan }
        }
        if(this.props.staticFormPinjam !== prevProps.staticFormPinjam) {
            return { staticFormPinjam: this.props.staticFormPinjam }
        }
        if(this.props.simpan !== prevProps.simpan) {
            return { simpan: this.props.simpan }
        }
        if(this.props.pinjam !== prevProps.pinjam) {
            return { pinjam: this.props.pinjam }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.staticFormSimpan) {
                this.setState({ staticContent: snapshot.staticFormSimpan })
            }
            if(snapshot.staticFormPinjam) {
                this.setState({ staticContent: snapshot.staticFormPinjam })
            }
            if(snapshot.simpan) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.simpan.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.simpan.status ? `Selamat, proses simpan di koperasi lemigas berhasil` : `Terjadi kesalahan`,
                    responseAction: () => {
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
            if(snapshot.pinjam) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.pinjam.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.pinjam.status ? `Selamat, proses pinjam di koperasi lemigas sedang dalam proses` : `Terjadi kesalahan`,
                    responseAction: () => {
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
        }
    }

    onChange = (key, value) => {
        let { forms, errors } = this.state;
        if(this.props.type === "simpan") {
            forms.simpan[key] = value;
            errors.simpan["err_"+key] = null;
        } else {
            forms.pinjam[key] = value;
            errors.pinjam["err_"+key] = null;
        }
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: (typeof value === "object" ? Object.keys(value).length > 0 : value.length > 0) && val === undefined }))
        });Text
    }

    onSubmit = () => {
        const { simpan, pinjam } = this.state.forms;
        Keyboard.dismiss();
        if(this.props.type === "simpan") {
            this.props.postOrderSimpan(simpan);
        } else {
            this.props.postOrderPinjam({
                id_anggota: pinjam.id_anggota,
                nama_anggota: pinjam.nama_anggota,
                nilai_pinjaman: pinjam.nilai_pinjaman,
                tenor: pinjam.tenor.id,
                keterangan: pinjam.keterangan
            });
        }
    }

    onValidate = async key => {
        const { simpan, pinjam } = this.state.forms;
        let except = [];
        if(this.props.type === "simpan") {
            except = ["id_anggota", "store_ke"];
        } else {
            except = ["id_anggota"];
        }
        except.push(key);
        let o = _.omit(this.props.type === "simpan" ? simpan : pinjam, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    downloadFile = file => {
        let req = this.requestWritePermission();
        req.then(value => {
            if(value === "granted") {
                console.log('granted ==>')
                RNFetchBlob
                    .config({
                        fileCache: true,
                        addAndroidDownloads : {
                            useDownloadManager : true,
                            notification : true,
                            description : 'Form Pengajuan'
                        }
                    })
                    .fetch('GET', file, {
                        //some headers ..
                    })
                    .then(res => {
                        console.log('res ==>', res);
                        alert("Success Downloaded");
                    })
                    .catch(err => {
                        console.warn('error ==>', err);
                    })
            } else {
                Alert.alert(
                    'Info',
                    'Anda tidak diizinkan untuk dapat mengakses media penyimpanan',
                    [
                        {text: 'Tanya Nanti', onPress: () => console.log('Ask me later pressed')},
                        {
                            text: 'Batal',
                            onPress: () => this.downloadFile(),
                            style: 'cancel',
                        },
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    {cancelable: false},
                );
            }
        })
    }

    requestWritePermission = async () => {
        try {
            if(Platform.OS === "android") {
                let Permissions = new AndroidPermissions();
                let reqWrite = await Permissions.requestWriteStoragePermission();
                return reqWrite;
            }
        } catch (err) {
            console.warn(err);
        }
    }

    render() {
        const { forms, errors, staticContent } = this.state;
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <View style={styles.container}>
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Syarat & Peraturan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        { staticContent !== null ? 
                            <View>
                                {_.map(staticContent.content, (v, k) => (
                                    <View key={k} style={{ flexDirection: 'row' }}>
                                        <Text style={{ width: '10%' }}>{k+1}. </Text>
                                        <Text style={{ width: '85%', textAlign: 'justify', textAlignVertical: "top" }}>{v.trim().replace(/[\n\r\t]/g, '')}</Text>
                                    </View>
                                ))}
                                <View style={{ flexDirection: 'row', marginTop: 16 }}>
                                    <Text>Download form pengajuan </Text>
                                    <TouchableOpacity onPress={() => this.downloadFile(staticContent.pdf_form)}>
                                        <Text style={{ color: DARK_PRIMARY_COLOR }}>disini</Text>
                                    </TouchableOpacity>
                                </View>
                            </View> :
                        <View>
                            <View style={{ height: 14, width: '100%', backgroundColor: DISABLED_COLOR, borderRadius: 6, marginBottom: 5 }} />
                            <View style={{ height: 14, width: '100%', backgroundColor: DISABLED_COLOR, borderRadius: 6, marginBottom: 5 }} />
                            <View style={{ height: 14, width: '100%', backgroundColor: DISABLED_COLOR, borderRadius: 6, marginBottom: 5 }} />
                            <View style={{ height: 14, width: '100%', backgroundColor: DISABLED_COLOR, borderRadius: 6, marginBottom: 5 }} />
                            <View style={{ height: 14, width: '100%', backgroundColor: DISABLED_COLOR, borderRadius: 6, marginBottom: 5 }} />
                        </View> }
                    </View>

                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Pengajuan {this.props.type === "simpan" ? "Simpan" : "Pinjam" }</Text>
                    </View>
                    {this.props.type === "simpan" ? 
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="jumlah_simpanan"
                                refs={ref => this._jumlah_simpanan = ref}
                                placeholder="Jumlah Simpanan"
                                keyboardType="phone-pad"
                                value={forms.simpan.jumlah_simpanan}
                                onChangeText={text => this.onChange("jumlah_simpanan", text)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="keterangan"
                                refs={ref => this._keterangan = ref}
                                placeholder="Keterangan"
                                keyboardType="default"
                                value={forms.simpan.keterangan}
                                onChangeText={text => this.onChange("keterangan", text)} />
                        </View>
                    </View> :
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="nilai_pinjaman"
                                refs={ref => this._nilai_pinjaman = ref}
                                placeholder="Nilai Pinjaman"
                                keyboardType="phone-pad"
                                value={forms.pinjam.nilai_pinjaman}
                                onChangeText={text => this.onChange("nilai_pinjaman", text)} />
                        </View>
                        <View>
                            <Picker 
                                nativeID="tenor"
                                refs={ref => this._tenor = ref}
                                placeholder="Tenor"
                                data={this.state.masterTenor}
                                isLoading={false}
                                isSearchable={false}
                                value={forms.pinjam.tenor.nama}
                                callback={value => this.onChange("tenor", value)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="keterangan"
                                refs={ref => this._keterangan = ref}
                                placeholder="Keterangan"
                                keyboardType="default"
                                value={forms.pinjam.keterangan}
                                onChangeText={text => this.onChange("keterangan", text)} />
                        </View>
                    </View> }
                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Proses"
                            onPress={() => this.onSubmit()}
                            isLoading={this.props.loaderStaticSimpan || this.props.loaderStaticPinjam} />
                    </View>
                </View>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    },
    modalHeader: { 
        width: '100%', 
        height: 56, 
        backgroundColor: WHITE_COLOR, 
        borderBottomWidth: .5, 
        borderBottomColor: BORDER_COLOR, 
        flexDirection: 'row' 
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    modalHeaderTitle: { 
        width: 150, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'flex-start' 
    },
    headerTitle: { 
        color: TEXT_COLOR, 
        fontWeight: '600' 
    },
    searchContainer: { 
        width: '100%', 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 16, 
        backgroundColor: DISABLED_COLOR, 
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 1 
    },
    searchInput: {
        width: '100%',
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 25,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        paddingRight: 40
    },
    iconPosition: {
        position: 'absolute', right: 20, top: 15
    }
})

const mapStateToProps = ({ loginReducer, simpanReducer, pinjamReducer }) => {
    const { isLogin, login } = loginReducer;
    const { staticFormSimpan, simpan } = simpanReducer;
    const { staticFormPinjam, pinjam } = pinjamReducer;
    const loaderStaticSimpan = simpanReducer.loading;
    const loaderStaticPinjam = pinjamReducer.loading;
    return { isLogin, login, staticFormSimpan, simpan, staticFormPinjam, pinjam, loaderStaticSimpan, loaderStaticPinjam };
}

export default connect(mapStateToProps, {
    postOrderSimpan,
    postOrderPinjam,
    getContentFormSimpan,
    getContentFormPinjam
})(FormSimpanPinjam)
