/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-29 09:43:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-31 21:42:56
 */
import React, { Component } from 'react';
import { View, ScrollView, Text, Image, ImageBackground, TouchableOpacity, StyleSheet, TouchableHighlight, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import RNFetchBlob from 'rn-fetch-blob'
import _ from 'lodash';
import moment from 'moment';
import 'moment/locale/id';
import { HeaderElipse, Button } from '../../component';
import { BORDER_COLOR, WHITE_COLOR, PRIMARY_COLOR, DISABLED_COLOR } from '../../../assets/colors';
import { greetings } from '../../../helpers';
import { clearPrevLogin, postFotoProfil, refreshNewLogin } from '../../../actions'

class Akun extends Component {

    componentWillUnmount() {
        Actions.refresh();
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.takePicture !== prevProps.takePicture) {
            return {takePicture: this.props.takePicture};
        }
        if(this.props.updateFotoProfil !== prevProps.updateFotoProfil) {
            return {updateFotoProfil: this.props.updateFotoProfil}
        }
        return null;
    }
    

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.takePicture) {
                RNFetchBlob.fs.readFile(snapshot.takePicture, 'base64')
                    .then((data) => {
                        this.props.postFotoProfil({
                            image: data,
                            anggota_id: this.props.login.noanggota
                        });
                    })
            }
            if(snapshot.updateFotoProfil) {
                let login = this.props.login;
                login.photo = snapshot.updateFotoProfil.data[0].photo;
                this.props.refreshNewLogin(login);
            }
        }
    }

    render() {
        const { login, takePicture, loading } = this.props;
        var image = takePicture !== null ? { uri: takePicture } : (login.photo !== null ? { uri: login.photo } : require('../../../assets/image/profil_default.png'));
        return (
            <ScrollView style={{ flex: 1 }}>
                <HeaderElipse />
                <View style={{ paddingHorizontal: 16, paddingVertical: 16 }}>
                    <View style={styles.container}>
                        <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                            <ImageBackground ref={ref => this._photo = ref} source={image} style={{width: 100, height: 100}} imageStyle={{ borderRadius: 50, backgroundColor: DISABLED_COLOR }} resizeMode="cover" defaultSource={require('../../../assets/image/profil_default.png')}>
                                { loading && <View style={{width: 100, height: 100, borderRadius: 50, backgroundColor: 'rgba(0,0,0,0.1)', justifyContent: 'center', alignItems: 'center'}}>
                                    <ActivityIndicator size="small" color={PRIMARY_COLOR} />
                                </View> }
                                <TouchableOpacity style = {{
                                    width: 100,
                                    height: 100,
                                    backgroundColor: 'rgba(0,0,0,0.3)',
                                    borderRadius: 50,
                                    borderTopWidth: 50,
                                    borderTopColor: 'transparent',
                                    justifyContent: 'flex-end',
                                    alignItems: 'center',
                                }}
                                onPress={() => Actions.push("kamera") }>
                                    <View style={{ position: 'absolute', bottom: 7, justifyContent: 'center', alignItems: 'center' }}>
                                        <FontAwesome name="camera" size={12} color={WHITE_COLOR} />
                                        <Text style={{ color: WHITE_COLOR, fontSize: 12 }}>Ganti</Text>
                                    </View>
                                </TouchableOpacity>
                            </ImageBackground>
                            <View style={{ marginLeft: 20 }}>
                                <Text style={{ fontSize: 14 }}>{greetings()}</Text>
                                <Text style={{ fontSize: 18, fontWeight: '600', textAlign: 'right' }}>{login.nama}</Text>
                            </View>
                        </View>
                        <View style={{ position: 'absolute', top: 16, right: 16, minWidth: 70, height: 25, justifyContent: 'center', alignItems: 'center', backgroundColor: PRIMARY_COLOR, borderRadius: 12.5 }}>
                            <Text style={{ color: WHITE_COLOR, fontWeight: '400', fontSize: 12 }}>{login.aktif === "1" ? "Aktif" : "Tidak Aktif"}</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'column', justifyContent: 'space-between', marginTop: 16, paddingHorizontal: 16 }}>
                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <View style={{ width: 120 }}>
                                    <Text style={{ fontSize: 16 }}>NIP</Text>
                                </View>
                                <View style={{ width: 200 }}>
                                    <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{login.noanggota}</Text>
                                </View>
                            </View>

                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <View style={{ width: 120 }}>
                                    <Text style={{ fontSize: 16 }}>Alamat</Text>
                                </View>
                                <View style={{ width: 200 }}>
                                    <Text style={{ fontSize: 16, width: 200 }}>{login.alamat}</Text>
                                </View>
                            </View>

                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <View style={{ width: 120 }}>
                                    <Text style={{ fontSize: 16 }}>TTL</Text>
                                </View>
                                <View style={{ width: 200 }}>
                                    <Text style={{ fontSize: 16 }}>{login.tmplahir}, {moment(login.tgllahir).format('LL')}</Text>
                                </View>
                            </View>

                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <View style={{ width: 120 }}>
                                    <Text style={{ fontSize: 16 }}>Jenis Kelamin</Text>
                                </View>
                                <View style={{ width: 200 }}>
                                    <Text style={{ fontSize: 16 }}>{login.gender === "1" ? "Laki-laki" : "Perempuan"}</Text>
                                </View>
                            </View>

                            <View style={{ borderBottomWidth: 0.6, borderColor: BORDER_COLOR, width: '100%', marginTop: 16, marginBottom: 16 }} />

                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <View style={{ width: 120 }}>
                                    <Text style={{ fontSize: 16 }}>Status Anggota</Text>
                                </View>
                                <View style={{ width: 200 }}>
                                    <Text style={{ fontSize: 16 }}>{login.anggota === "1" ? "Anggota Aktif" : "Angoota Tidak Aktif"}</Text>
                                </View>
                            </View>

                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <View style={{ width: 120 }}>
                                    <Text style={{ fontSize: 16 }}>Keanggotaan</Text>
                                </View>
                                <View style={{ width: 200 }}>
                                    <Text style={{ fontSize: 16 }}>{login.pengurus === "1" ? "Pengurus" : "Anggota"}</Text>
                                </View>
                            </View>

                        </View>
                    </View>

                    <View>
                        <Button 
                            primary={true} 
                            label="Perbaharui Akun"
                            onPress={() => Actions.push('formAkun', {user: this.props.login})}
                            isLoading={false} />
                    </View>

                    <View>
                        <Button 
                            primary={true} 
                            label="Keluar"
                            onPress={() => {
                                firebase.messaging().unsubscribeFromTopic(this.props.login.noanggota);
                                this.props.clearPrevLogin();
                                Actions.reset("main")
                            }}
                            isLoading={false} />
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        borderRadius: 10,
        paddingHorizontal: 16,
        paddingVertical: 16,
        minHeight: 250,
        marginBottom: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    }
})

const mapStateToProps = ({ loginReducer, kameraReducer }) => {
    const { isLogin, login } = loginReducer;
    const { takePicture, loading, updateFotoProfil } = kameraReducer;
    return { isLogin, login, takePicture, loading, updateFotoProfil }
}

export default connect(mapStateToProps, {
    clearPrevLogin,
    postFotoProfil,
    refreshNewLogin
})(Akun);