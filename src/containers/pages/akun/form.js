/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:07:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-27 07:37:12
 */
import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Keyboard, Text, TouchableOpacity, Modal, TextInput, PermissionsAndroid } from 'react-native';
import { connect } from "react-redux";
import _ from "lodash";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { InputText, Button, Picker, DatePicker, HeaderElipse, ModalResponse } from '../../component';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, PRIMARY_COLOR, DISABLED_COLOR, BORDER_COLOR, ICON_COLOR, PLACEHOLDER_COLOR, TEXT_COLOR, ERROR_COLOR } from '../../../assets/colors';
import { updateUser, getCheckUsername, refreshNewLogin } from '../../../actions'
import { Actions } from 'react-native-router-flux';
import {rupiah} from '../../../helpers';

class FormAkun extends Component {

    state = {
        forms: {
            anggota_id: this.props.isLogin ? this.props.login.id : "",
            username: this.props.user.username,
            no_hp: this.props.user.nohp,
            alamat: this.props.user.alamat,
            password: null,
            password_confirm: null,
        },
        errors: {
            err_username: null,
            err_no_hp: null,
            err_alamat: null,
            err_password: null,
            err_password_confirm: null
        },
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null,
        isCheckUsername: false,
        responseCheckUsername: null,
        infoUsernameAvailable: "Tekan tombol ceklis untuk mengetahui apakah username tersedia"
    }

    async componentDidMount() {
        let except = ["anggota_id", "password", "password_confirm"];
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        this.setState({
            readyToSubmit: a === undefined,
        });
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.username !== prevProps.username) {
            return { username: this.props.username }
        }
        if(this.props.akun !== prevProps.akun) {
            return { akun: this.props.akun }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.username) {
                this.setState(prevState => ({
                    responseCheckUsername: snapshot.username.status ? true : false,
                    infoUsernameAvailable: snapshot.username.status ? "Username tersedia" : "Username sudah digunakan"
                }))
            }
            if(snapshot.akun) {
                if(snapshot.akun.status) {
                    this.props.refreshNewLogin(snapshot.akun.data)
                }
                this.setState({
                    isResponse: true,
                    responseType: snapshot.akun.status ? "sukses" : "gagal",
                    responseMessage: snapshot.akun.status ? "Selamat, profil anda berhasil diperbaharui" : "Terjadi kesalahan dalam memperbaharui profil",
                    responseAction: () => {
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
        }
    }

    onChange = (key, value) => {
        let { forms, errors } = this.state;
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: (typeof value === "object" ? Object.keys(value).length > 0 : value.length > 0) && val === undefined }))
        });Text
    }

    onSubmit = () => {
        Keyboard.dismiss();
        this.props.updateUser(this.state.forms);
    }

    onValidate = async key => {
        let except = ["anggota_id", "password", "password_confirm"];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    onCheckUsername = () => {
        this.setState({
            isCheckUsername: true
        }, () => this.props.getCheckUsername({
            anggota_id: this.props.login.id,
            username: this.state.forms.username
        }))
    }

    render() {
        const { forms } = this.state;
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                {/* <HeaderElipse /> */}
                <View style={styles.container}>
                    {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                        <View style={{ width: 110, height: 110, borderRadius: 55, justifyContent: 'center', alignItems: 'center', backgroundColor: DARK_PRIMARY_COLOR }}>
                            <FontAwesome name="bed" size={60} color={WHITE_COLOR} />
                        </View>
                    </View> */}
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pengguna</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="username"
                                refs={ref => this._username = ref}
                                placeholder="Username"
                                keyboardType="default"
                                value={forms.username}
                                onChangeText={text => this.onChange("username", text)}
                                info={this.state.infoUsernameAvailable} />
                            <TouchableOpacity onPress={() => this.onCheckUsername()} style={{ position: 'absolute', right: 20, top: 24 }}>
                                <FontAwesome name="check-circle" size={20} color={ this.state.responseCheckUsername !== null ? (this.state.responseCheckUsername ? PRIMARY_COLOR : ERROR_COLOR) : ICON_COLOR} />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <InputText 
                                nativeID="no_hp"
                                refs={ref => this._no_hp = ref}
                                placeholder="No Handphone"
                                keyboardType="phone-pad"
                                value={forms.no_hp}
                                onChangeText={text => this.onChange("no_hp", text)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="alamat"
                                refs={ref => this._alamat = ref}
                                placeholder="Alamat"
                                keyboardType="default"
                                value={forms.alamat}
                                onChangeText={text => this.onChange("alamat", text)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="password"
                                refs={ref => this._password = ref}
                                placeholder="Password"
                                secureTextEntry
                                onChangeText={text => this.onChange("password", text)}
                                style={{ textAlign: 'center' }} 
                                returnKeyType="next" />
                        </View>
                        <View>
                            <InputText 
                                nativeID="password_confirm"
                                refs={ref => this._password_confirm = ref}
                                placeholder="Password Confirm"
                                secureTextEntry
                                onChangeText={text => this.onChange("password_confirm", text)}
                                style={{ textAlign: 'center' }} 
                                returnKeyType="go" />
                        </View>
                    </View>

                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Perbaharui Data"
                            onPress={() => this.onSubmit()}
                            isLoading={this.props.loading} />
                    </View>
                </View>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    },
    modalHeader: { 
        width: '100%', 
        height: 56, 
        backgroundColor: WHITE_COLOR, 
        borderBottomWidth: .5, 
        borderBottomColor: BORDER_COLOR, 
        flexDirection: 'row' 
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    modalHeaderTitle: { 
        width: 150, 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'flex-start' 
    },
    headerTitle: { 
        color: TEXT_COLOR, 
        fontWeight: '600' 
    },
    searchContainer: { 
        width: '100%', 
        height: 56, 
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 16, 
        backgroundColor: DISABLED_COLOR, 
        borderBottomColor: BORDER_COLOR, 
        borderBottomWidth: 1 
    },
    searchInput: {
        width: '100%',
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 25,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        backgroundColor: WHITE_COLOR,
        paddingRight: 40
    },
    iconPosition: {
        position: 'absolute', right: 20, top: 15
    }
})

const mapStateToProps = ({ loginReducer, akunReducer }) => {
    const { isLogin, login } = loginReducer;
    const { loading, username, akun } = akunReducer;
    return {isLogin, login, loading, username, akun }
}

export default connect(mapStateToProps, {
    updateUser,
    getCheckUsername,
    refreshNewLogin
})(FormAkun);