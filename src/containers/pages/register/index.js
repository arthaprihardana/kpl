/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:07:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 05:20:59
 */
import React, { Component } from 'react';
import { ScrollView, View, Image, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { connect } from "react-redux";
import _ from 'lodash';

import { InputText, Button, Picker, DatePicker, ModalResponse, HeaderElipse } from '../../component';

import { DARK_PRIMARY_COLOR } from '../../../assets/colors';
import { postRegister, getAgama, clearPrevRegister } from '../../../actions';
import { Actions } from 'react-native-router-flux';

class Register extends Component {

    state = {
        forms: {
            nama: "",
            tmplahir: "",
            tgllahir: "",
            gender: "",
            agamaid: ""
        },
        errors: {
            err_nama: null,
            err_tmplahir: null,
            err_tgllahir: null,
            err_gender: null,
            err_agamaid: null
        },
        masterAgama: [],
        masterJenisKelamin: [{ id: 2, nama: "Perempuan" }, { id: 1, nama: "Laki-laki" }],
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null
    }

    componentDidMount() {
        this.props.getAgama();
        this.props.clearPrevRegister();
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.agama !== prevProps.agama) {
            return { agama: this.props.agama }
        }
        if(this.props.register !== prevProps.register) {
            return { register: this.props.register }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.agama) {
                this.setState({
                    masterAgama: snapshot.agama
                })
            }
            if(snapshot.register) {
                if(snapshot.regiter !== null) {
                    this.setState({
                        isResponse: true,
                        responseType: "sukses",
                        responseMessage: "Selamat, Anda telah berhasil melakukan registrasi di sistem Koperasi Pegawai Lamigas",
                        responseAction: () => {
                            this.setState({isResponse : false });
                            Actions.pop(); 
                        }
                    });
                }
            }
        }
    }

    onChange = (key, value) => {
        let forms = this.state.forms;
        let errors = this.state.errors;
        forms[key] = value;
        errors["err_"+key] = null;
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: val === undefined }))
        });
    }

    onSubmit = () => {
        this.props.postRegister({
            nama: this.state.forms.nama,
            tmplahir: this.state.forms.tmplahir,
            tgllahir: this.state.forms.tgllahir,
            gender: this.state.forms.gender.id,
            agamaid: this.state.forms.agamaid.id
        });
    }

    onValidate = async key => {
        let except = [];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <HeaderElipse />
                <View style={styles.container}>
                    <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={ require('../../../assets/image/logo.png') } style={{ width: 100, height: 100}} />
                    </View>
                    <View>
                        <InputText 
                            nativeID="nama"
                            refs={ref => this._nama = ref}
                            placeholder="Nama"
                            keyboardType="default"
                            onChangeText={text => this.onChange("nama", text)}
                            errorMessage={this.state.errors.err_nama}
                            returnKeyType="next"
                            onSubmitEditing={() => this._tmplahir.focus()} />
                    </View>
                    <View>
                        <InputText 
                            nativeID="tmplahir"
                            refs={ref => this._tmplahir = ref}
                            placeholder="Tempat Lahir"
                            keyboardType="default"
                            onChangeText={text => this.onChange("tmplahir", text)}
                            errorMessage={this.state.errors.err_tmplahir}
                            returnKeyType="done" />
                    </View>
                    <View>
                        <DatePicker 
                            nativeID="tgllahir"
                            refs={ref => this._tgllahir = ref}
                            placeholder="Tanggal Lahir"
                            value={this.state.forms.tgllahir}
                            hasMinDate={false}
                            errorMessage={this.state.errors.err_tgllahir}
                            callback={date => this.onChange("tgllahir", date.format("DD-MM-YYYY"))} />
                    </View>
                    <View>
                        <Picker 
                            nativeID="gender"
                            refs={ref => this._gender = ref}
                            placeholder="Jenis Kelamin"
                            value={this.state.forms.gender.nama}
                            data={this.state.masterJenisKelamin}
                            errorMessage={this.state.errors.err_gender}
                            callback={value => this.onChange("gender", value)} />
                    </View>
                    <View>
                        <Picker 
                            nativeID="agamaid"
                            refs={ref => this._agamaid = ref}
                            placeholder="Agama"
                            value={this.state.forms.agamaid.nama}
                            data={this.state.masterAgama}
                            isLoading={this.props.agamaLoader}
                            errorMessage={this.state.errors.err_agamaid}
                            callback={value => this.onChange("agamaid", value)} />
                    </View>
                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Register" 
                            onPress={() => this.onSubmit()} 
                            isLoading={this.props.registerLoader} />
                    </View>
                    <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <Text>Sudah Punya Akun ? </Text>
                        <TouchableOpacity onPress={() => Actions.pop() }>
                            <Text style={{ color: DARK_PRIMARY_COLOR }}>Login Disini</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    }
})

const mapStateToProps = ({ registerReducer, agamaReducer }) => {
    const registerLoader = registerReducer.loading;
    const agamaLoader = agamaReducer.loading;
    const { register } = registerReducer;
    const { agama } = agamaReducer;
    return { agamaLoader, registerLoader, agama, register }
}

export default connect(mapStateToProps, {
    postRegister,
    clearPrevRegister,
    getAgama
})(Register);