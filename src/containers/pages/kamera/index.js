/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-30 23:03:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-01 00:46:02
 */
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Dimensions } from 'react-native';
import { RNCamera } from 'react-native-camera';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import { WHITE_COLOR, BORDER_COLOR } from '../../../assets/colors';
import { onTakePicture } from '../../../actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

const { width } = Dimensions.get('window');

class Kamera extends Component {

    state = {
        flashOption: [
            {name: "auto", value: RNCamera.Constants.FlashMode.auto, icon: "md-flash"},
            {name: "on", value: RNCamera.Constants.FlashMode.on, icon: "md-flash"},
            {name: "off", value: RNCamera.Constants.FlashMode.off, icon: "md-flash-off"},
        ],
        typeOption: [
            {name: "back", value: RNCamera.Constants.Type.back},
            {name: "front", value: RNCamera.Constants.Type.front},
        ],
        flashMode: "auto",
        type: "back",
        orientation: RNCamera.Constants.Orientation.portrait,
        autoFocus: RNCamera.Constants.AutoFocus.on
    }

    componentWillUnmount() {
        Actions.refresh();   
    }

    onChangeFlashMode = () => {
        let idx = _.findIndex(this.state.flashOption, { name: this.state.flashMode });
        let selected = this.state.flashOption[idx + 1];
        this.setState({
            flashMode: selected !== undefined ? _.filter(this.state.flashOption, { name: selected.name })[0].name : this.state.flashOption[0].name
        });
    }

    onChangeCameraType = () => {
        let idx = _.findIndex(this.state.typeOption, { name: this.state.type });
        let selected = this.state.typeOption[idx + 1];
        this.setState({
            type: selected !== undefined ? _.filter(this.state.typeOption, { name: selected.name })[0].name : this.state.typeOption[0].name
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => this.camera = ref}
                    style={styles.preview}
                    type={_.filter(this.state.typeOption, { name: this.state.type })[0].value}
                    flashMode={_.filter(this.state.flashOption, { name: this.state.flashMode })[0].value}
                    autoFocus={this.state.autoFocus}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    captureAudio={false}
                    />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', position: 'absolute', top: 0, width: width, backgroundColor: 'transparent', paddingVertical: 6 }}>
                    <TouchableOpacity onPressIn={() => Actions.pop()} style={{ width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }}>
                        <Ionicons name="md-close" size={28} color={WHITE_COLOR} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onChangeFlashMode()} style={{ width: 56, height: 56, justifyContent: 'center', alignItems: 'center', borderRadius: 28 }}>
                        <Ionicons name={_.filter(this.state.flashOption, { name: this.state.flashMode })[0].icon} size={24} color={WHITE_COLOR} />
                        <Text style={{ fontSize: 10, color: WHITE_COLOR }}>{this.state.flashMode}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', position: 'absolute', bottom: 0, width: width, backgroundColor: 'rgba(0,0,0,0.2)', paddingVertical: 16 }}>
                    <TouchableOpacity onPressIn={() => Actions.push("galeri")} style={{ width: 80, height: 80, justifyContent: 'center', alignItems: 'center' }}>
                        <Ionicons name="md-images" size={24} color={WHITE_COLOR} />
                        <Text style={{ fontSize: 10, color: WHITE_COLOR }}>Galeri</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPressIn={() => this.takePicture()} style={{ borderWidth: 3, borderColor: WHITE_COLOR, borderRadius: 40, width: 80, height: 80, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={styles.capture} />
                    </TouchableOpacity>
                    <TouchableOpacity onPressIn={() => this.onChangeCameraType()} style={{ width: 80, height: 80, justifyContent: 'center', alignItems: 'center' }}>
                        <Ionicons name="md-reverse-camera" size={24} color={WHITE_COLOR} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    
    takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true, orientation: "portrait", skipProcessing: true };
            const data = await this.camera.takePictureAsync(options);
            this.props.onTakePicture(data.uri);
            Actions.pop();
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: WHITE_COLOR,
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 3,
        borderColor: 'transparent',
        zIndex: 1
    },
});

// export default Kamera;

const mapStateToProps = ({  }) => {
    return {  };
}

export default connect(mapStateToProps, {
    onTakePicture
})(Kamera);