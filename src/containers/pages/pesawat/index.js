/**
 * @author: Artha Prihardana 
 * @Date: 2019-04-30 16:07:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-11 00:05:38
 */
import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Keyboard, Text } from 'react-native';
import { connect } from "react-redux";
import _ from "lodash";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { InputText, Button, Picker, DatePicker, HeaderElipse, ModalResponse, InputCounter } from '../../component';
import { WHITE_COLOR, DARK_PRIMARY_COLOR, BORDER_COLOR, BACKGROUND_INPUT_FIELD, DISABLED_COLOR } from '../../../assets/colors';
import { getBandara, postOrderPesawat, clearPrevTiket, getMaskapai } from '../../../actions';
import { Actions } from 'react-native-router-flux';

class Pesawat extends Component {

    state = {
        forms: {
            dari: "",
            ke: "",
            maskapai: "",
            penumpang: "",
            waktu_keberangkatan: "",
            kursi_kelas: "",
            nama_penumpang: [],
            nama_anggota: this.props.isLogin ? this.props.login.nama : "",
            id_anggota: this.props.isLogin ? this.props.login.id : "",
            telepon: "",
	        ekstensi: ""
        },
        errors: {
            err_dari: null,
            err_ke: null,
            err_maskapai: null,
            err_penumpang: null,
            err_waktu_keberangkatan: null,
            err_kursi_kelas: null,
            err_nama_penumpang: null,
            err_nama_anggota: null,
            err_telepon: null,
	        err_ekstensi: null
        },
        masterBandara: [],
        masterKelas: [{id: 1, nama: "Ekonomi"}, {id: 2, nama: "Bisnis"}, {id: 3, nama: "First Class"}],
        readyToSubmit: false,
        isResponse: false,
        responseType: null,
        responseMessage: null,
        responseAction: null
    }

    componentDidMount() {
        this.props.getBandara();
        this.props.getMaskapai();
    }

    getSnapshotBeforeUpdate = (prevProps, prevState) => {
        if(this.props.bandara !== prevProps.bandara) {
            return { bandara: this.props.bandara }
        }
        if(this.props.orderPesawat !== prevProps.orderPesawat) {
            return { orderPesawat: this.props.orderPesawat }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.bandara) {
                this.setState({
                    masterBandara: snapshot.bandara
                })
            }
            if(snapshot.orderPesawat) {
                this.setState({
                    isResponse: true,
                    responseType: typeof snapshot.orderPesawat.status ? "sukses" : "gagal",
                    responseMessage: typeof snapshot.orderPesawat.status ? "Selamat, pemesanan tiket pesawat sedang diproses" : "Terjadi kesalahan dalam pemesanan",
                    responseAction: () => {
                        this.props.clearPrevTiket();
                        this.setState({isResponse : false });
                        Actions.pop();
                    }
                });
            }
        }
    }

    onChange = (key, value, index = 0) => {
        let forms = this.state.forms;
        let errors = this.state.errors;
        if(key === "nama_penumpang") {
            forms[key][index - 1] = value;
        } else {
            forms[key] = value;
        }
        errors["err_"+key] = null;
        this.setState({ forms });
        this.setState({ forms, errors }, () => {
            this.onValidate(key).then(val => this.setState({ readyToSubmit: val === undefined }))
        });
    }

    onSubmit = () => {
        const { forms } = this.state;
        Keyboard.dismiss();
        this.props.postOrderPesawat({
            dari: forms.dari.nama,
            ke: forms.ke.nama,
            maskapai: forms.maskapai.id,
            penumpang: forms.penumpang,
            waktu_keberangkatan: forms.waktu_keberangkatan,
            kursi_kelas: forms.kursi_kelas.nama,
            nama_penumpang: forms.nama_penumpang,
            nama_anggota: forms.nama_anggota,
            id_anggota: forms.id_anggota,
            telepon: forms.telepon,
	        ekstensi: forms.ekstensi
        })
    }

    onValidate = async key => {
        let except = ["id_anggota", "nama_anggota"];
        except.push(key);
        let o = _.omit(this.state.forms, except);
        let a = await _.findKey(o, val => _.isEmpty(val));
        return a;
    }

    renderPenumpang = () => {
        let penumpang = [];
        for(let i = 1; i <= this.state.forms.penumpang; i++) {
            penumpang.push(
            <View key={i}>
                <InputText 
                    nativeID="nama_penumpang"
                    refs={ref => this._nama_penumpang = ref}
                    placeholder={`Nama Penumpang Ke-${i}`}
                    keyboardType="default"
                    // value={forms.nama_penumpang}
                    onChangeText={text => this.onChange("nama_penumpang", text, i)}
                    />
            </View>
            )
        }
        return penumpang;
    }

    render() {
        const { forms } = this.state;
        return (
            <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                {/* <HeaderElipse /> */}
                <View style={styles.container}>
                    {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                        <View style={{ width: 110, height: 110, borderRadius: 55, justifyContent: 'center', alignItems: 'center', backgroundColor: DARK_PRIMARY_COLOR }}>
                            <FontAwesome name="plane" size={60} color={WHITE_COLOR} />
                        </View>
                    </View> */}
                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Pemesan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputText 
                                nativeID="nama_anggota"
                                refs={ref => this._nama_anggota = ref}
                                placeholder="Nama Anggota"
                                keyboardType="default"
                                value={forms.nama_anggota}
                                onChangeText={text => this.onChange("nama_anggota", text)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="telepon"
                                refs={ref => this._telepon = ref}
                                placeholder="Telepon"
                                keyboardType="phone-pad"
                                value={forms.telepon}
                                onChangeText={text => this.onChange("telepon", text)} />
                        </View>
                        <View>
                            <InputText 
                                nativeID="ekstensi"
                                refs={ref => this._ekstensi = ref}
                                placeholder="Ekstensi"
                                keyboardType="phone-pad"
                                value={forms.ekstensi}
                                onChangeText={text => this.onChange("ekstensi", text)} />
                        </View>
                    </View>
                    

                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Perjalanan</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <Picker 
                                nativeID="dari"
                                refs={ref => this._dari = ref}
                                placeholder="Dari"
                                data={this.state.masterBandara}
                                isLoading={this.props.loading}
                                isSearchable={true}
                                value={forms.dari.nama}
                                callback={value => this.onChange("dari", value)} />
                        </View>
                        <View>
                            <Picker 
                                nativeID="ke"
                                refs={ref => this._ke = ref}
                                placeholder="Ke"
                                data={forms.dari !== "" ? _.omit(this.state.masterBandara, forms.dari.nama) : this.state.masterBandara}
                                isLoading={this.props.loading}
                                isSearchable={true}
                                value={forms.ke.nama}
                                callback={value => this.onChange("ke", value)} />
                        </View>
                        <View>
                            <Picker 
                                nativeID="maskapai"
                                refs={ref => this._maskapai = ref}
                                placeholder="Maskapai"
                                data={forms.maskapai !== "" ? _.omit(this.props.maskapai, forms.maskapai) : this.props.maskapai}
                                isLoading={this.props.loading}
                                isSearchable={true}
                                value={forms.maskapai.nama}
                                callback={value => this.onChange("maskapai", value)} />
                        </View>
                        <View>
                            <DatePicker 
                                nativeID="waktu_keberangkatan"
                                refs={ref => this._waktu_keberangkatan = ref}
                                placeholder="Tanggal Keberangkatan"
                                hasMinDate={true}
                                value={forms.waktu_keberangkatan}
                                callback={date => this.onChange("waktu_keberangkatan", date.format("YYYY-MM-DD"))} />
                        </View>
                        <View>
                            <Picker 
                                nativeID="kursi_kelas"
                                refs={ref => this._kursi_kelas = ref}
                                placeholder="Kursi Kelas"
                                data={this.state.masterKelas}
                                value={forms.kursi_kelas.nama}
                                callback={value => this.onChange("kursi_kelas", value)} />
                        </View>
                    </View>

                    <View style={styles.blockTitle}>
                        <Text style={{ fontWeight: '500' }}>Info Penumpang</Text>
                    </View>
                    <View style={styles.blockContent}>
                        <View>
                            <InputCounter 
                                nativeID="penumpang"
                                refs={ref => this._penumpang = ref}
                                placeholder="Jml Penumpang"
                                keyboardType="phone-pad"
                                maxLength={2}
                                minValue={0}
                                value={forms.penumpang}
                                onChangeText={text => this.onChange("penumpang", text)}
                                onSub={text => this.onChange("penumpang", text)}
                                onAdd={text => this.onChange("penumpang", text)} />
                        </View>
                        { this.renderPenumpang() }
                    </View>

                    <View>
                        <Button 
                            primary={this.state.readyToSubmit} 
                            disabled={!this.state.readyToSubmit} 
                            label="Pesan"
                            onPress={() => this.onSubmit()}
                            isLoading={this.props.orderLoader} />
                    </View>
                </View>
                <ModalResponse 
                    responseVisible={this.state.isResponse} 
                    setResponseVisible={value => this.setState({ isResponse: value })}
                    type={this.state.responseType}
                    message={this.state.responseMessage}
                    action={this.state.responseAction} />
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        paddingHorizontal: 16, 
        paddingVertical: 16, 
        justifyContent: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    blockTitle: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        backgroundColor: DISABLED_COLOR,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderLeftWidth: .5,
        borderLeftColor: BORDER_COLOR,
        borderRightWidth: .5,
        borderRightColor: BORDER_COLOR,
        borderTopWidth: .5,
        borderTopColor: BORDER_COLOR
    },
    blockContent: {
        borderWidth: 0.5,
        borderColor: BORDER_COLOR,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: WHITE_COLOR,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 10
    }
})

const mapStateToProps = ({ bandaraReducer, tiketReducer, loginReducer, maskapaiReducer }) => {
    const { loading, bandara } = bandaraReducer;
    const { orderPesawat } = tiketReducer;
    const { isLogin, login } = loginReducer;
    const { maskapai } = maskapaiReducer;
    const orderLoader = tiketReducer.loading;
    return { loading, bandara, orderPesawat, orderLoader, isLogin, login, maskapai }
}

export default connect(mapStateToProps, {
    getBandara,
    postOrderPesawat,
    clearPrevTiket,
    getMaskapai
})(Pesawat);