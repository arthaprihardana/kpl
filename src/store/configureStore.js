/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 05:51:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 07:42:51
 */
import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from "redux-saga";
import { persistStore } from 'redux-persist';
import reducers from '../reducers/index';
import RootSaga from '../middlewares/index';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default configureStore = () => {
    const store = createStore(
        reducers, 
        composeEnhancer(applyMiddleware(...middlewares))
    );
    let persistor = persistStore(store);

    sagaMiddleware.run(RootSaga);

    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = require('../reducers/index').default;
            store.replaceReducer(nextRootReducer);
        })
    }
    
    return { store, persistor };
}