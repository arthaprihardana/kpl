/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-07 23:25:24 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-08-07 20:48:35
 */
import { GET_GEDUNG_LIST, GET_GEDUNG_LIST_SUCCESS, GET_GEDUNG_LIST_FAILURE, GET_GEDUNG_DETAIL, GET_GEDUNG_DETAIL_SUCCESS, GET_GEDUNG_DETAIL_FAILURE, POST_ORDER_GEDUNG, POST_ORDER_GEDUNG_SUCCESS, POST_ORDER_GEDUNG_FAILURE, GET_BOOKING_DATE, GET_BOOKING_DATE_SUCCESS, GET_BOOKING_DATE_FAILURE, GET_REKANAN, GET_REKANAN_SUCCESS, GET_REKANAN_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    orderLoading: false,
    gedung: null,
    gedungDetail: null,
    orderGedung: null,
    bookingDate: [],
    rekanan: null,
    loadingRekanan: false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_GEDUNG_LIST:
            return { ...state, loading: true }
        case GET_GEDUNG_LIST_SUCCESS:
            return { ...state, loading: false, gedung: action.payload }
        case GET_GEDUNG_LIST_FAILURE:
            return { ...state, loading: false, gedung: null }
        case GET_GEDUNG_DETAIL:
            return { ...state, loading: true }
        case GET_GEDUNG_DETAIL_SUCCESS:
            return { ...state, loading: false, gedungDetail: action.payload }
        case GET_GEDUNG_DETAIL_FAILURE:
            return { ...state, loading: false, gedungDetail: null }
        case POST_ORDER_GEDUNG:
            return { ...state, orderLoading: true }
        case POST_ORDER_GEDUNG_SUCCESS:
            return { ...state, orderLoading: false, orderGedung: action.payload }
        case POST_ORDER_GEDUNG_FAILURE:
            return { ...state, orderLoading: false, orderGedung: null }
        case GET_BOOKING_DATE:
            return { ...state, loading: true }
        case GET_BOOKING_DATE_SUCCESS:
            return { ...state, loading: false, bookingDate: action.payload }
        case GET_BOOKING_DATE_FAILURE:
            return { ...state, loading: false, bookingDate: [] }
        case GET_REKANAN:
            return { ...state, loadingRekanan: true }
        case GET_REKANAN_SUCCESS:
            return { ...state, loadingRekanan: false, rekanan: action.payload }
        case GET_REKANAN_FAILURE:
            return { ...state, loadingRekanan: false, rekanan: null }
        default:
            return { ...state }
    }
}
