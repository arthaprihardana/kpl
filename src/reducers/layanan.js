/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:58:49 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-09 22:59:32
 */
import { GET_ALL_LAYANAN, GET_ALL_LAYANAN_SUCCESS, GET_ALL_LAYANAN_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    layanan: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ALL_LAYANAN:
            return { ...state, loading: true }
        case GET_ALL_LAYANAN_SUCCESS:
            return { ...state, loading: false, layanan: action.payload }
        case GET_ALL_LAYANAN_FAILURE:
            return { ...state, loading: false, layanan: null }
        default:
            return { ...state }
    }
}