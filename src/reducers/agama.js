/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:27:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 09:31:09
 */
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import { GET_AGAMA, GET_AGAMA_SUCCESS, GET_AGAMA_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    agama: null
}

const persistConfig = {
    key: 'agamaReducer',
    storage: AsyncStorage,
    whitelist: ['agama']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_AGAMA:
            return { ...state, loading: true }
        case GET_AGAMA_SUCCESS:
            return { ...state, loading: false, agama: action.payload }
        case GET_AGAMA_FAILURE:
            return { ...state, loading: false, agama: null }
        default:
            return { ...state }
    }
})