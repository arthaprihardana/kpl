/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 05:54:23 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-10 23:44:02
 */
import {combineReducers} from 'redux';
import routes from './routes';
import network from './network';
import login from './login';
import register from './register';
import agama from './agama';
import layanan from './layanan';
import bannerPromo from './banner';
import bandara from './bandara';
import stasiun from './stasiun';
import kota from './kota';
import tiket from './tiket';
import berita from './berita';
import toko from './toko';
import topup from './topup';
import kamera from './kamera';
import serbausaha from './serbausaha';
import riwayat from './riwayat';
import simpan from './simpan';
import pinjam from './pinjam';
import akun from './akun';
import gedung from './gedung';
import notifikasi from './notifikasi';
import forceUpdate from './forceUpdate';
import maskapai from './maskapai';

export default combineReducers({
    router: routes,
    network: network,
    // other reducers,
    loginReducer: login,
    registerReducer: register,
    agamaReducer: agama,
    layananReducer: layanan,
    bannerPromoReducer: bannerPromo,
    bandaraReducer: bandara,
    stasiunReducer: stasiun,
    kotaReducer: kota,
    tiketReducer: tiket,
    beritaReducer: berita,
    tokoReducer: toko,
    topupReducer: topup,
    kameraReducer: kamera,
    serbaUsahaReducer: serbausaha,
    riwayatReducer: riwayat,
    simpanReducer: simpan,
    pinjamReducer: pinjam,
    akunReducer: akun,
    gedungReducer: gedung,
    notifikasiReducer: notifikasi,
    forceUpdate: forceUpdate,
    maskapaiReducer: maskapai
});