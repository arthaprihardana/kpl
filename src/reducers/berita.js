/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:27:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-25 01:29:25
 */
import { GET_BERITA, GET_BERITA_SUCCESS, GET_BERITA_FAILURE, GET_DETAIL_BERITA, GET_DETAIL_BERITA_SUCCESS, GET_DETAIL_BERITA_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    berita: null,
    detail: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_BERITA:
            return { ...state, loading: true }
        case GET_BERITA_SUCCESS:
            return { ...state, loading: false, berita: action.payload }
        case GET_BERITA_FAILURE:
            return { ...state, loading: false, berita: null }
        case GET_DETAIL_BERITA:
            return { ...state, loading: true }
        case GET_DETAIL_BERITA_SUCCESS:
            return { ...state, loading: false, detail: action.payload }
        case GET_DETAIL_BERITA_FAILURE:
            return { ...state, loading: false, detail: null }
        default:
            return { ...state }
    }
}