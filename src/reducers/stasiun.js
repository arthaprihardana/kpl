/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:27:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-18 10:42:12
 */
import { GET_MASTER_STASIUN, GET_MASTER_STASIUN_SUCCESS, GET_MASTER_STASIUN_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    stasiun: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_STASIUN:
            return { ...state, loading: true }
        case GET_MASTER_STASIUN_SUCCESS:
            return { ...state, loading: false, stasiun: action.payload }
        case GET_MASTER_STASIUN_FAILURE:
            return { ...state, loading: false, stasiun: null }
        default:
            return { ...state }
    }
}