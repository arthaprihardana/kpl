/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 20:51:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 20:53:24
 */
import {GET_STATIC_FORM_SIMPAN, GET_STATIC_FORM_SIMPAN_SUCCESS, GET_STATIC_FORM_SIMPAN_FAILURE, POST_ORDER_SIMPAN, POST_ORDER_SIMPAN_SUCCESS, POST_ORDER_SIMPAN_FAILURE} from '../actions/types';

const INIT_STATE = {
    loading: false,
    staticFormSimpan: null,
    simpan: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_STATIC_FORM_SIMPAN:
            return { ...state, loading: true }
        case GET_STATIC_FORM_SIMPAN_SUCCESS:
            return { ...state, loading: false, staticFormSimpan: action.payload }
        case GET_STATIC_FORM_SIMPAN_FAILURE:
            return { ...state, loading: false, staticFormSimpan: null }
        case POST_ORDER_SIMPAN:
            return { ...state, loading: true }
        case POST_ORDER_SIMPAN_SUCCESS:
            return { ...state, loading: false, simpan: action.payload }
        case POST_ORDER_SIMPAN_FAILURE:
            return { ...state, loading: false, simpan: null }
        default:
            return { ...state }
    }
}