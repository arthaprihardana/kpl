/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 06:22:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-14 23:18:25
 */
import { POST_REGISTER, POST_REGISTER_SUCCESS, POST_REGISTER_FAILURE, CLEAR_PREV_REGISTER } from "../actions/types";

const INIT_STATE = {
    loading: false,
    register: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case POST_REGISTER:
            return { ...state, loading: true }
        case POST_REGISTER_SUCCESS:
            return { ...state, loading: false, register: action.payload }
        case POST_REGISTER_FAILURE:
            return { ...state, loading: false, register: null }
        case CLEAR_PREV_REGISTER:
            return { ...state, register: null }
        default:
            return { ...state }
    }
}