/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-30 23:02:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-31 23:35:26
 */
import { TAKE_PICTURE, POST_FOTO_PROFIL, POST_FOTO_PROFIL_SUCCESS, POST_FOTO_PROFIL_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    takePicture: null,
    updateFotoProfil: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case TAKE_PICTURE:
            return { ...state, takePicture: action.payload }
        case POST_FOTO_PROFIL:
            return { ...state, loading: true }
        case POST_FOTO_PROFIL_SUCCESS:
            return { ...state, loading: false, updateFotoProfil: action.payload }
        case POST_FOTO_PROFIL_FAILURE:
            return { ...state, loading: false, updateFotoProfil: null }
        default:
            return { ...state }
    }
}