/**
 * @author: Artha Prihardana 
 * @Date: 2019-07-31 21:00:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-07-31 21:01:29
 */
import { GET_NOTIFIKASI, GET_NOTIFIKASI_SUCCESS, GET_NOTIFIKASI_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    notif: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_NOTIFIKASI:
            return { ...state, loading: true }
        case GET_NOTIFIKASI_SUCCESS:
            return { ...state, loading: false, notif: action.payload }
        case GET_NOTIFIKASI_FAILURE:
            return { ...state, loading: false, notif: null }
        default:
            return { ...state }
    }
}