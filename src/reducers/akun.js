/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-26 19:54:24 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-26 20:16:32
 */
import { GET_CHECK_USERNAME, GET_CHECK_USERNAME_SUCCESS, GET_CHECK_USERNAME_FAILURE, PUT_AKUN, PUT_AKUN_SUCCESS, PUT_AKUN_FAILURE, GET_SALDO, GET_SALDO_SUCCESS, GET_SALDO_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    username: null,
    akun: null,
    saldo: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CHECK_USERNAME:
            return { ...state, loading: true }
        case GET_CHECK_USERNAME_SUCCESS:
            return { ...state, loading: false, username: action.payload }
        case GET_CHECK_USERNAME_FAILURE:
            return { ...state, loading: false, username: null }
        case PUT_AKUN:
            return { ...state, loading: true }
        case PUT_AKUN_SUCCESS:
            return { ...state, loading: false, akun: action.payload }
        case PUT_AKUN_FAILURE:
            return { ...state, loading: false, akun: null }
        case GET_SALDO:
            return { ...state, loading: true }
        case GET_SALDO_SUCCESS:
            return { ...state, loading: false, saldo: action.payload }
        case GET_SALDO_FAILURE:
            return { ...state, loading: false, saldo: null }
        default:
            return { ...state }
    }
}