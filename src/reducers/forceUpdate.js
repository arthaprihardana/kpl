/**
 * @Author: Artha Prihardana
 * @Date: 2019-10-03 18:16:43
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-03 18:17:45
 */
import { GET_FORCE_UPDATE, GET_FORCE_UPDATE_SUCCESS, GET_FORCE_UPDATE_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    update: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_FORCE_UPDATE:
            return { ...state, loading: true }
        case GET_FORCE_UPDATE_SUCCESS:
            return { ...state, loading: false, update: action.payload }
        case GET_FORCE_UPDATE_FAILURE:
            return { ...state, loading: false, update: null }
        default:
            return { ...state }
    }
}