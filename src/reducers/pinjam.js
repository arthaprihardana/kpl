/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 20:51:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 20:56:41
 */
import {GET_STATIC_FORM_PINJAM, GET_STATIC_FORM_PINJAM_SUCCESS, GET_STATIC_FORM_PINJAM_FAILURE, POST_ORDER_PINJAM, POST_ORDER_PINJAM_SUCCESS, POST_ORDER_PINJAM_FAILURE} from '../actions/types';

const INIT_STATE = {
    loading: false,
    staticFormPinjam: null,
    pinjam: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_STATIC_FORM_PINJAM:
            return { ...state, loading: true }
        case GET_STATIC_FORM_PINJAM_SUCCESS:
            return { ...state, loading: false, staticFormPinjam: action.payload }
        case GET_STATIC_FORM_PINJAM_FAILURE:
            return { ...state, loading: false, staticFormPinjam: null }
        case POST_ORDER_PINJAM:
            return { ...state, loading: true }
        case POST_ORDER_PINJAM_SUCCESS:
            return { ...state, loading: false, pinjam: action.payload }
        case POST_ORDER_PINJAM_FAILURE:
            return { ...state, loading: false, pinjam: null }
        default:
            return { ...state }
    }
}