/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:27:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 20:24:53
 */
import { GET_MASTER_KOTA, GET_MASTER_KOTA_SUCCESS, GET_MASTER_KOTA_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    kota: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_KOTA:
            return { ...state, loading: true }
        case GET_MASTER_KOTA_SUCCESS:
            return { ...state, loading: false, kota: action.payload }
        case GET_MASTER_KOTA_FAILURE:
            return { ...state, loading: false, kota: null }
        default:
            return { ...state }
    }
}