/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-17 09:47:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-17 10:00:02
 */
import { POST_ORDER_PESAWAT, POST_ORDER_PESAWAT_SUCCESS, POST_ORDER_PESAWAT_FAILURE, POST_ORDER_HOTEL, POST_ORDER_HOTEL_SUCCESS, POST_ORDER_HOTEL_FAILURE, POST_ORDER_KERETA, POST_ORDER_KERETA_SUCCESS, POST_ORDER_KERETA_FAILURE, POST_ORDER_BUS, POST_ORDER_BUS_SUCCESS, POST_ORDER_BUS_FAILURE, POST_ORDER_SHUTTLE, POST_ORDER_SHUTTLE_SUCCESS, POST_ORDER_SHUTTLE_FAILURE, CLEAR_PREV_TIKET } from "../actions/types";

const INIT_STATE = {
    loading: false,
    orderPesawat: null,
    orderHotel: null,
    orderKereta: null,
    orderBus: null,
    orderShuttle: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case POST_ORDER_PESAWAT:
            return { ...state, loading: true }
        case POST_ORDER_PESAWAT_SUCCESS:
            return { ...state, loading: false, orderPesawat: action.payload }
        case POST_ORDER_PESAWAT_FAILURE:
            return { ...state, loading: false, orderPesawat: null }
        case POST_ORDER_HOTEL:
            return { ...state, loading: true }
        case POST_ORDER_HOTEL_SUCCESS:
            return { ...state, loading: false, orderHotel: action.payload }
        case POST_ORDER_HOTEL_FAILURE:
            return { ...state, loading: false, orderHotel: null }
        case POST_ORDER_KERETA:
            return { ...state, loading: true }
        case POST_ORDER_KERETA_SUCCESS:
            return { ...state, loading: false, orderKereta: action.payload }
        case POST_ORDER_KERETA_FAILURE:
            return { ...state, loading: false, orderKereta: null }
        case POST_ORDER_BUS:
            return { ...state, loading: true }
        case POST_ORDER_BUS_SUCCESS:
            return { ...state, loading: false, orderBus: action.payload }
        case POST_ORDER_BUS_FAILURE:
            return { ...state, loading: false, orderBus: null }
        case POST_ORDER_SHUTTLE:
            return { ...state, loading: true }
        case POST_ORDER_SHUTTLE_SUCCESS:
            return { ...state, loading: false, orderShuttle: action.payload }
        case POST_ORDER_SHUTTLE_FAILURE:
            return { ...state, loading: false, orderShuttle: null }
        case CLEAR_PREV_TIKET:
            return { ...state, orderPesawat: null, orderHotel: null, orderKereta: null, orderBus: null, orderShuttle: null }
        default:
            return { ...state }
    }
}