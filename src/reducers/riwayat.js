/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-21 13:17:28 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2019-06-21 13:17:28 
 */
import { GET_RIWAYAT, GET_RIWAYAT_SUCCESS, GET_RIWAYAT_FAILURE, GET_DETAIL_RIWAYAT, GET_DETAIL_RIWAYAT_SUCCESS, GET_DETAIL_RIWAYAT_FAILURE } from '../actions/types';

const INIT_STATE = {
    loading: false,
    riwayat: null,
    detailRiwayat: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_RIWAYAT:
            return { ...state, loading: true }
        case GET_RIWAYAT_SUCCESS:
            return { ...state, loading: false, riwayat: action.payload }
        case GET_RIWAYAT_FAILURE:
            return { ...state, loading: false, riwayat: null }
        case GET_DETAIL_RIWAYAT:
            return { ...state, loading: true }
        case GET_DETAIL_RIWAYAT_SUCCESS:
            return { ...state, loading: false, detailRiwayat: action.payload }
        case GET_DETAIL_RIWAYAT_FAILURE:
            return { ...state, loading: false, detailRiwayat: null }
        default:
            return { ...state }
    }
}