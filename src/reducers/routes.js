/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 05:53:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-09 05:59:07
 */
import { ActionConst } from 'react-native-router-flux';

const INIT_STATE = {
    scene: {}
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case ActionConst.FOCUS:
            console.log('focus ==>', action);
            return {
                ...state,
                scene: action.scene,
            };
        case ActionConst.PUSH:
            console.log('push ==>', action);
            return {
                ...state,
                scene: action.scene
            }
        default:
            return state;
    }
}