/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-20 08:20:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-20 09:34:06
 */
import { NETWORK_STATUS } from "../actions/types";

const INIT_STATE = {
    isConnected: false,
    connectivityType: null,
    // showIsBackOnline: false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case NETWORK_STATUS:
            return { ...state, isConnected: action.payload.isConnected, connectivityType: action.payload.type }
        default:
            return { ...state }
    }
}