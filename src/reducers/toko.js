/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:27:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-28 01:47:00
 */
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import _ from "lodash";
import { GET_TOKO, GET_TOKO_SUCCESS, GET_TOKO_FAILURE, GET_DETAIL_PRODUK, GET_DETAIL_PRODUK_SUCCESS, GET_DETAIL_PRODUK_FAILURE, GET_RELATED_PRODUK, GET_RELATED_PRODUK_SUCCESS, GET_RELATED_PRODUK_FAILURE, ADD_TO_CART, CLEAN_CART, DELETE_FROM_CART, POST_ORDER_TOKO, POST_ORDER_TOKO_SUCCESS, POST_ORDER_TOKO_FAILURE, GET_SEARCH_TOKO, GET_SEARCH_TOKO_SUCCESS, GET_SEARCH_TOKO_FAILURE, GET_MASTER_KATEGORI_PRODUK, GET_MASTER_KATEGORI_PRODUK_SUCCESS, GET_MASTER_KATEGORI_PRODUK_FAILURE, GET_PRODUK_BY_KATEGORI, GET_PRODUK_BY_KATEGORI_SUCCESS, GET_PRODUK_BY_KATEGORI_FAILURE } from "../actions/types";

const INIT_STATE = {
    loadingKategori: false,
    loading: false,
    toko: null,
    detailProduk: null,
    relatedProduk: null,
    cart: [],
    order: null,
    kategori: null
}

const persistConfig = {
    key: 'tokoReducer',
    storage: AsyncStorage,
    whitelist: ['cart']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_TOKO:
            return { ...state, loading: true }
        case GET_TOKO_SUCCESS:
            return { ...state, loading: false, toko: action.payload }
        case GET_TOKO_FAILURE:
            return { ...state, loading: false, toko: null }

        case GET_SEARCH_TOKO:
            return { ...state, loading: true }
        case GET_SEARCH_TOKO_SUCCESS:
            return { ...state, loading: false, toko: action.payload }
        case GET_SEARCH_TOKO_FAILURE:
            return { ...state, loading: false, toko: null }

        case GET_DETAIL_PRODUK:
            return { ...state, loading: true }
        case GET_DETAIL_PRODUK_SUCCESS:
            return { ...state, loading: false, detailProduk: action.payload }
        case GET_DETAIL_PRODUK_FAILURE:
            return { ...state, loading: false, detailProduk: null }

        case GET_RELATED_PRODUK:
            return { ...state, loading: true }
        case GET_RELATED_PRODUK_SUCCESS:
            return { ...state, loading: false, relatedProduk: action.payload }
        case GET_RELATED_PRODUK_FAILURE:
            return { ...state, loading: false, relatedProduk: null }

        case ADD_TO_CART:
            let cart = state.cart;
            let c = action.payload;
            c.index = cart.length + 1;
            cart.push(c);
            return { ...state, cart: cart }
        case CLEAN_CART:
            return { ...state, cart: []}
        case DELETE_FROM_CART:
            let del = _.filter(state.cart, v => v.index !== action.payload)
            return { ...state, cart: del }

        case POST_ORDER_TOKO:
            return { ...state, loading: true }
        case POST_ORDER_TOKO_SUCCESS:
            return { ...state, loading: false, order: action.payload }
        case POST_ORDER_TOKO_FAILURE:
            return { ...state, loading: false, order: null }

        case GET_MASTER_KATEGORI_PRODUK:
            return { ...state, loadingKategori: true }
        case GET_MASTER_KATEGORI_PRODUK_SUCCESS:
            return { ...state, loadingKategori: false, kategori: action.payload }
        case GET_MASTER_KATEGORI_PRODUK_FAILURE:
            return { ...state, loadingKategori: false, kategori: null }

        case GET_PRODUK_BY_KATEGORI:
            return { ...state, loading: true }
        case GET_PRODUK_BY_KATEGORI_SUCCESS:
            return { ...state, loading: false, toko: action.payload }
        case GET_PRODUK_BY_KATEGORI_FAILURE:
            return { ...state, loading: false, toko: null }

        default:
            return { ...state }
    }
})