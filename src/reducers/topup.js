/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-25 22:14:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-19 22:12:58
 */
import { POST_ORDER_PULSA, POST_ORDER_PULSA_SUCCESS, POST_ORDER_PULSA_FAILURE, POST_ORDER_PAKET_DATA, POST_ORDER_PAKET_DATA_SUCCESS, POST_ORDER_PAKET_DATA_FAILURE , POST_ORDER_LISTRIK_TOKEN, POST_ORDER_LISTRIK_TOKEN_SUCCESS, POST_ORDER_LISTRIK_TOKEN_FAILURE, POST_ORDER_LISTRIK_TAGIHAN, POST_ORDER_LISTRIK_TAGIHAN_SUCCESS, POST_ORDER_LISTRIK_TAGIHAN_FAILURE} from "../actions/types";

const INIT_STATE = {
    loading: false,
    orderPulsa: null,
    orderPaketData: null,
    listrikToken: null,
    listrikTagihan: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case POST_ORDER_PULSA:
            return { ...state, loading: true }
        case POST_ORDER_PULSA_SUCCESS:
            return { ...state, loading: false, orderPulsa: action.payload }
        case POST_ORDER_PULSA_FAILURE:
            return { ...state, loading: false, orderPulsa: null }
        case POST_ORDER_PAKET_DATA:
            return { ...state, loading: true }
        case POST_ORDER_PAKET_DATA_SUCCESS:
            return { ...state, loading: false, orderPaketData: action.payload }
        case POST_ORDER_PAKET_DATA_FAILURE:
            return { ...state, loading: false, orderPaketData: null }
        case POST_ORDER_LISTRIK_TOKEN:
            return { ...state, loading: true }
        case POST_ORDER_LISTRIK_TOKEN_SUCCESS:
            return { ...state, loading: false, listrikToken: action.payload }
        case POST_ORDER_LISTRIK_TOKEN_FAILURE:
            return { ...state, loading: false, listrikToken: null }
        case POST_ORDER_LISTRIK_TAGIHAN:
            return { ...state, loading: true }
        case POST_ORDER_LISTRIK_TAGIHAN_SUCCESS:
            return { ...state, loading: false, listrikTagihan: action.payload }
        case POST_ORDER_LISTRIK_TAGIHAN_FAILURE:
            return { ...state, loading: false, listrikTagihan: null }
        default:
            return { ...state }
    }
}