/**
 * @Author: Artha Prihardana
 * @Date: 2019-10-10 23:43:23
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-10-11 00:01:30
 */
import { GET_MASTER_MASKAPAI, GET_MASTER_MASKAPAI_SUCCESS, GET_MASTER_MASKAPAI_FAILURE } from "../actions/types"

const INIT_STATE = {
  loading: false,
  maskapai: null
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
      case GET_MASTER_MASKAPAI:
          return { ...state, loading: true }
      case GET_MASTER_MASKAPAI_SUCCESS:
          return { ...state, loading: false, maskapai: action.payload.map(v => ({id: v.maskapai_id, nama: v.nama_maskapai})) }
      case GET_MASTER_MASKAPAI_FAILURE:
          return { ...state, loading: false, maskapai: null }
      default:
          return { ...state }
  }
}