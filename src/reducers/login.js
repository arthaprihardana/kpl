/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 06:22:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-18 23:07:20
 */
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import { POST_LOGIN, POST_LOGIN_SUCCESS, POST_LOGIN_FAILURE, CLEAR_PREV_LOGIN, REFRESH_NEW_LOGIN } from "../actions/types";

const INIT_STATE = {
    loading: false,
    isLogin: false,
    login: null
}

const persistConfig = {
    key: 'loginReducer',
    storage: AsyncStorage,
    whitelist: ['isLogin', 'login']
};

export default persistReducer(persistConfig, (state = INIT_STATE, action) => {
    switch (action.type) {
        case POST_LOGIN:
            return { ...state, loading: true }
        case POST_LOGIN_SUCCESS:
            return { ...state, loading: false, isLogin: true, login: action.payload }
        case POST_LOGIN_FAILURE:
            return { ...state, loading: false, isLogin: false, login: action.payload }
        case CLEAR_PREV_LOGIN:
            return { ...state, isLogin: false, login: null }
        case REFRESH_NEW_LOGIN:
            return { ...state, login: action.payload }
        default:
            return { ...state }
    }
})