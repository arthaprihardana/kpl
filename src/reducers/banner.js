/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-15 19:33:18 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 19:45:03
 */
import { GET_BANNER_PROMO, GET_BANNER_PROMO_SUCCESS, GET_BANNER_PROMO_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    banner: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_BANNER_PROMO:
            return { ...state, loading: true }
        case GET_BANNER_PROMO_SUCCESS:
            return { ...state, loading: false, banner: action.payload }
        case GET_BANNER_PROMO_FAILURE:
            return { ...state, loading: false, banner: null }
        default:
            return { ...state }
    }
}