/**
 * @author: Artha Prihardana 
 * @Date: 2019-06-19 22:25:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-06-21 20:49:05
 */
import {GET_LIST_SERBA_USAHA, GET_LIST_SERBA_USAHA_SUCCESS, GET_LIST_SERBA_USAHA_FAILURE, GET_DETAIL_SERBA_USAHA, GET_DETAIL_SERBA_USAHA_SUCCESS, GET_DETAIL_SERBA_USAHA_FAILURE, POST_SERBA_USAHA, POST_SERBA_USAHA_SUCCESS, POST_SERBA_USAHA_FAILURE} from '../actions/types';

const INIT_STATE = {
    loading: false,
    listSerbaUsaha: null,
    detailSerbaUsaha: null,
    serbaUsaha: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_LIST_SERBA_USAHA:
            return { ...state, loading: true }
        case GET_LIST_SERBA_USAHA_SUCCESS:
            return { ...state, loading: false, listSerbaUsaha: action.payload }
        case GET_LIST_SERBA_USAHA_FAILURE:
            return { ...state, loading: false, listSerbaUsaha: null }
        case GET_DETAIL_SERBA_USAHA:
            return { ...state, loading: true }
        case GET_DETAIL_SERBA_USAHA_SUCCESS:
            return { ...state, loading: false, detailSerbaUsaha: action.payload }
        case GET_DETAIL_SERBA_USAHA_FAILURE:
            return { ...state, loading: false, detailSerbaUsaha: null }
        case POST_SERBA_USAHA:
            return { ...state, loading: true }
        case POST_SERBA_USAHA_SUCCESS:
            return { ...state, loading: false, serbaUsaha: action.payload }
        case POST_SERBA_USAHA_FAILURE:
            return { ...state, loading: false, serbaUsaha: null }
        default:
            return { ...state }
    }
}