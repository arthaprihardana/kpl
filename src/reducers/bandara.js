/**
 * @author: Artha Prihardana 
 * @Date: 2019-05-09 22:27:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-05-15 20:00:48
 */
import { GET_MASTER_BANDARA, GET_MASTER_BANDARA_SUCCESS, GET_MASTER_BANDARA_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    bandara: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_BANDARA:
            return { ...state, loading: true }
        case GET_MASTER_BANDARA_SUCCESS:
            return { ...state, loading: false, bandara: action.payload }
        case GET_MASTER_BANDARA_FAILURE:
            return { ...state, loading: false, bandara: null }
        default:
            return { ...state }
    }
}