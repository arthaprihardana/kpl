# Koperasi Lembaga Minyak dan Gas (LEMIGAS)

[![Build Status](https://app.bitrise.io/app/6ec83662c2759e47/status.svg?token=niqc9oNrRPXcI7e0nqnv_g&branch=master)](https://app.bitrise.io/app/6ec83662c2759e47)

Projek koperasi LEMIGAS. (*on progress*)

## **TODO**

### Pages
- [x] Splashscreen
- [x] Page Login
- [x] Page Register
- [x] Page Beranda
- [x] Page Riwayat
- [x] Page Berita
- [x] Page Akun
- [x] Page Tiket
- [x] Page Topup
- [x] Page Store
- [x] Page Simpan Pinjam
- [x] Page Serba Usaha
- [x] Page Notifikasi

### API Integration
- [x] API Register
- [x] API Login
- [x] API Banner
- [x] API Layanan
- [x] API Produk
- [x] API Related Produk
- [x] API Kategori Produk
- [x] API Searching Produk
- [x] API Berita
- [x] API Detail Berita
- [x] API Riwayat
- [x] API Akun
- [x] API Master Agama
- [x] API Master Bandara
- [x] API Master Kota
- [x] API Master Stasiun
- [x] API Order Tiket
- [x] API Order TopUp
- [x] API Simpan Pinjam
- [x] API Serba Usaha

## How to run this project

### Instal dependencies

```
npm install
```

### Run project (debug)

```
yarn android
- or -
npm run android
- or - 
react-native run-android
```

### Run project (release)

```
yarn build:android
- or -
npm run build:android
```

## Screenshots

![home](https://bitbucket.org/arthaprihardana/kpl/raw/25057146819c298c4bea84b21f1969a6a17c12a3/screenshots/Screenshot_1559201527.png)